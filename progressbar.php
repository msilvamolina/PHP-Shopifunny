<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

?>
<!doctype html>
<html>
<head>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>

  <style>
  #progressBar {
    margin-left: 100px;
    width: 400px;
    height: 50px;
  }

  #progressBar div {
    height: 100%;
    color: #fff;
    text-align: right;
    font-size: 12px;
    line-height: 50px;
    width: 0;
  }
  .default {
    background: #292929;
    border: 1px solid #111; 
    border-radius: 2px; 
  }
  .default div {
    background-color: #ff9900;
    background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#ff9900), to(#1a82f7)); 
    background: -webkit-linear-gradient(top, #ff9900, #1a82f7); 
    background: -moz-linear-gradient(top, #ff9900, #1a82f7); 
    background: -ms-linear-gradient(top, #ff9900, #1a82f7); 
    background: -o-linear-gradient(top, #ff9900, #1a82f7);
  }
</style>
</head>
<body>

  <div id="progressBar" class="default"><div></div></div>


</div>
<script type="text/javascript" >
  var vendidos = 56;


  progressBar(vendidos, $('#progressBar'), vendidos);

  window.setTimeout( function() {
    vendidos = vendidos + 7;
    progressBar(vendidos, $('#progressBar'), vendidos); 
  }, 5000 );


  function progressBar(percent, $element, vendidos) {
    var progressBarWidth = percent * $element.width() / 100;
    $element.find('div').animate({ width: progressBarWidth }, 500).html(vendidos+" of 100 sold&nbsp;");
  }
</script>

</body>
</html>