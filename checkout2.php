<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

session_start();

$codigo_compra = $_SESSION['codigo_compra'];


$titulo_pagina = 'Shopifunny - Checkout';
$WHERE = null;

conectar2('shopifun', "compras");

//consultar en la base de datos
$query_rs_compras = "SELECT id_producto, cantidad FROM compras_primer_paso WHERE codigo_compra = '$codigo_compra' ORDER BY id_compra DESC";
$rs_compras = mysql_query($query_rs_compras)or die(mysql_error());
$row_rs_compras = mysql_fetch_assoc($rs_compras);
$totalrow_rs_compras = mysql_num_rows($rs_compras);

$lista_producto = NULL;
do {
	$id_producto = $row_rs_compras['id_producto'];
	$cantidad = $row_rs_compras['cantidad'];

	if(!$WHERE) {
		$WHERE = 'WHERE id_producto = '.$id_producto;
	} else {
		$WHERE .= ' OR id_producto = '.$id_producto;
	}
} while ($row_rs_compras = mysql_fetch_assoc($rs_compras));

desconectar();

conectar2('shopifun', "admin");


//consultar en la base de datos
$query_rs_productos = "SELECT * FROM productos $WHERE";
$rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
$row_rs_productos = mysql_fetch_assoc($rs_productos);
$totalrow_rs_productos = mysql_num_rows($rs_productos);

//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, id_publicacion, recorte_foto_miniatura FROM fotos_publicaciones";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));
desconectar();


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php
	//Permisos
	$agregar_masterslider = 1;
	$agregar_slick = 1;

	include('paginas_include/estructura/head.php'); ?>
	
	<style>
	.cd-main-content {
		background: white !important;
		padding: 10px 120px  !important;
		text-align: left;
	}
	.checkout_titulo {
		margin-bottom: 10px;
	}
</style>
</head>
<body >
	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>

	<main class="cd-main-content">
		<h2 class="checkout_titulo">Checkout</h2>
		<div class="cd-form floating-labels" >
			<?php if($totalrow_rs_productos) { ?>
			<table class="table table-striped">
				<tbody>
					<?php 
					$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';
					$precio_total = 0;

					do { 
						$id_producto = $row_rs_productos['id_producto'];
						$promocion_titulo = $row_rs_productos['producto_titulo'];
						$producto_publicado = $row_rs_productos['producto_publicado'];
						$nombre_foto = $row_rs_productos['nombre_foto'];
						$fecha_publicacion = $row_rs_productos['fecha_publicacion'];
						$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];

						$precio_total = $precio_total + $producto_precio_dolar;

						$precio = formato_moneda($producto_precio_dolar, 'dolar');

						$foto_portada = $row_rs_productos['foto_portada'];

						$publicada = '<p class="rojo">No está publicado</p>';
						if($producto_publicado) {
							$publicada = '<p class="verde">'.nombre_fecha($fecha_publicacion).'</p>';
						}

						$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
						$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

						if($foto_portada) {
							$imagen = $array_fotos[$foto_portada];
						}

						$super_class = null;
						if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
							$super_class = 'categorias_con_subgrupos';
						}
						?>
						<tr class="<?php echo $super_class; ?>">
							<td><img src="<?php echo $imagen; ?>"  width="100"></td>
							<td><?php echo $promocion_titulo; ?></td>
							<td width="200">		
								<div class="cart_precio_total"><?php echo $precio; ?></div></td>
							</tr>		
							<?php } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>	        
							<tr class="<?php echo $super_class; ?>">
								<td colspan="2">Total</td>
								<td width="200">		
									<div class="cart_precio_total"><?php echo $precio = formato_moneda($precio_total, 'dolar');
									; ?></div></td>
								</tr>		  	
							</tbody>
						</table>		 
						<a onclick="buynow()" class="vc_btn_largo vc_btn_verde vc_btn_3d boton_cart_separacion" style="max-width:200px; float: right">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-credit-card-alt fa-stack-1x fa-inverse"></i>
							</span>
							<b>Confirm & Pay</b>
						</a>    
						<div class="clear"></div>
						<?php } else { ?>
						<p>No hay productos en tu carrito</p>
						<?php }?>           
					</div>

					<br><br><br><br><br><br><br><br>

				</main>

				<?php include('paginas_include/estructura/pie.php') ; ?>
				<?php include('paginas_include/estructura/javascript-pie.php');?>

				<script type="text/javascript">		
					<?php foreach ($array_cuerpo as $id_cuerpo => $value) { ?>
						cargar_elemento(<?php echo $id_cuerpo; ?>);
						<?php } ?>

						var slider = new MasterSlider();

						slider.control('arrows' ,{insertTo:'#masterslider'});	
						slider.control('bullets', {autohide:false});	

						slider.setup('masterslider' , {
							width:1280,
							height:600,
							autoplay: true,
							speed:20
						});
						function mostrar_producto(producto) {
							$('#producto_nombre_'+producto).show(); 
						}
						function ocultar_producto(producto) {
							$('#producto_nombre_'+producto).hide(); 
						}
						function cargar_elemento(elemento) {
							$.ajax({
								url: "<?php echo $Servidor_url; ?>/ajax/sistema/cargar-elemento.php?elemento="+elemento,
								success: function (resultado) {
									$("#elemento"+elemento).html(resultado);
								}
							});	
						}
					</script>
				</body>
				</html>