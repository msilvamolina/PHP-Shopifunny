<?php include('../paginas_include/variables-generales.php');
include('../paginas_include/variables-permisos.php');

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

require 'src/start.php';

$approved = $_GET['approved'];

if($approved) {

	$codigo = $_GET['codigo'];
	$codigo_compra = $codigo;
	$email_admins = 'msilvamolina@gmail.com, rafawer@gmail.com';

	if($approved=="true") {

		$payerId = $_GET['PayerID'];
		$paymentId = $_GET['paymentId'];

		$payment = Payment::get($paymentId, $api);
		$execution = new PaymentExecution();

		$execution->setPayerId($payerId);
		$payment->execute($execution, $api);

		$paymentId = $_GET['paymentId'];
		$token = $_GET['token'];
		$PayerID = $_GET['PayerID'];

		conectar2('shopifun', "compras");
		mysql_query("UPDATE compras_segundo_paso SET paypal_payment_id='$paymentId', paypal_token='$token', paypal_payer_id='$PayerID', paypal_complete='1' WHERE compra_codigo='$codigo'");

		$_SESSION['codigo_compra'] = "";
		setcookie("codigo_compra","",time() - (86400 *  365), '/');  

		unset($_SESSION['codigo_compra']);
		unset($_COOKIE['codigo_compra']);

		header('Location: '.$Servidor_url.'checkout/paypalapproved/'.$codigo_compra.'/');

		desconectar();

		//la compra se realizó correctamente, entonces avisamos mandando un email
		require '../recursos/vendor/autoload.php';
		include ('../recursos/emails/disenos/01-gracias-por-tu-compra2.php');
		$asunto = $first_name.', Thanks For Your Business!';
		include('../recursos/emails/00-codigo-que-manda-email.php');


		//ahora le avisamos a los administradores
		$email_a_mandar = $email_admins;
		$first_name = '';
		include ('../recursos/emails/disenos/02-nueva-compra.php');
		include('../recursos/emails/00-codigo-que-manda-email.php');

	} else {
		header('Location: '.$Servidor_url.'checkout/paypalcancelled/');

		//Paypal rechazo el pago, asi que le mandamos el mail avisando
		require '../recursos/vendor/autoload.php';
		$email_a_mandar = null;
		include ('../recursos/emails/disenos/03-pago-cancelado.php');
		$asunto = $first_name.', Payment Canceled :(';
		include('../recursos/emails/00-codigo-que-manda-email.php');

		//ahora le avisamos a los administradores
		$email_a_mandar = $email_admins;
		$first_name = '';
		include ('../recursos/emails/disenos/04-admin-pago-cancelado.php');
		include('../recursos/emails/00-codigo-que-manda-email.php');

		exit;
	}
} else {
	header('Location: '.$Servidor_url.'checkout/paypalcancelled/');
	exit;
}

?>