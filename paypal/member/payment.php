<?php include('../../paginas_include/variables-generales.php');
include('../../paginas_include/variables-permisos.php');

use PayPal\Api\Payer;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Item;
use PayPal\Api\ItemList;

use PayPal\Exception\PayPalConnectionException;

require '../src/start.php';

$payer = new Payer();
$details = new Details();
$amount = new Amount();
$transaction = new Transaction();
$payment = new Payment();
$redirectUrls = new RedirectUrls();

$cantidad = 2;
$price = 22.00;
$shipping = 2.00;

$total = $price + $shipping;

$payer->setPaymentMethod('paypal');

$item = new Item();
$item->setName("Reloj muy bueno")
->setCurrency('USD')
->setQuantity($cantidad)
->setPrice($price);

$itemList = new ItemList();
$itemList->setItems([$item]);


$details->setShipping($shipping * $cantidad)
->setSubtotal($price * $cantidad);

$amount = new Amount();
$amount->setCurrency('USD')
->setTotal($total * $cantidad)
->setDetails($details);


//Transaction
$transaction->setAmount($amount)
->setItemList($itemList)
->setDescription('Pay For bla bla')
->setInvoiceNumber(uniqid());

//Redirects URLs
$redirectUrls->setReturnUrl('https://www.shopifunny.com/paypal/pay.php?approved=true')
->setCancelUrl('https://www.shopifunny.com/paypal/pay.php?approved=false');

//Payment
$payment->setIntent('sale')
->setPayer($payer)
->setTransactions([$transaction])
->setRedirectUrls($redirectUrls);

try {
	$payment->create($api);

	$payment_id = $payment->getID();
	$hash = md5($payment_id);
$_SESSION['paypal_hash'] = $hash;

conectar2('shopifun', "compras");

$id_usuario = 22;
$query = "INSERT INTO transactions_paypal (user_id, payment_id, hash, complete) 
VALUES ('$id_usuario','$payment_id', '$hash', '0')";
$result = mysql_query($query);
$id_transaccion = mysql_insert_id();

desconectar();
	// Generate and store hasg
	//prepare and execute transaction storage

} catch (PayPalConnectionException $e) {
	echo $e->getCode(); 
	echo $e->getData();
	die($e);
}

foreach($payment->getLinks() as $link) {
	if($link->getRel() == 'approval_url'){
		$redirectUrl = $link->getHref();
	}
}

?>
<html>
	<head></head>
	<body>
		<a href="<?php echo $redirectUrl; ?>"><?php echo $redirectUrl; ?></a>
	</body>
</html>