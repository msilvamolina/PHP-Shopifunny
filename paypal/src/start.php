<?php

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

session_start();

$_SESSION['user_id'] = 1;

require __DIR__ . '/../vendor/autoload.php';

//API SANDBOX
/*
$api = new ApiContext(
	new OAuthTokenCredential(
		'AZP8iVot28mpjqjn1xw0Ft4k8oOflZ-Bb3c2nD84JSN0Zjiq7prO-DO9BjJpc5IIgk2VLz3ZnAOGxWGD',
		'EPHma3kNK4YTX3O5-TG8H4luG_aehgMr2Lew7AAlYol7EvTophYfOiqxx4myjXgk7llNVVdxZodqvWMM'
	)
);
*/

//API LIVE
$api = new ApiContext(
	new OAuthTokenCredential(
		'AXnlRqWoKE79aZi3NARhz-O6WZIX9QebwOcZNmQDU1MQGu9HPa4qsE70fvav9J5VRDYEUlN7VLPbvtvP',
		'EHX15BfDcPa9Nx2ehyNdaicF9conALhOY-FLbGI5RSQJK0krX-fgUzX3BRp8iZ4QDf8eJXsrZNAHaeTQ'
	)
);

$api->setConfig([
	'mode' => 'live',
	'http.ConnectionTimeOut' => 30,
	'log.LogEnabled' => true,
	'log.FileName' => 'log.txt',
	'log.LogLevel' => 'FINE',
	'validation.level' => 'log'
]);

?>