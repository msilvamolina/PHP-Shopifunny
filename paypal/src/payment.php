<?php include('../../paginas_include/variables-generales.php');
include('../../paginas_include/variables-permisos.php');

session_start();

$get_codigo = trim($_GET['codigo']);
$get_codigo1 = trim($_GET['codigo1']);
$get_codigo2 = trim($_GET['codigo2']);
$get_codigo3 = trim($_GET['codigo3']);

$codigo_compra = trim($_GET['codigo_compra']);

$session_codigo = trim($_SESSION['codigo_seguridad']);
$session_codigo1 = trim($_SESSION['codigo_seguridad_1']);
$session_codigo2 = trim($_SESSION['codigo_seguridad_2']);
$session_codigo3 = trim($_SESSION['codigo_seguridad_3']);

$redireccion_error = $Servidor_url.'error404';

if(!$codigo_compra || ($get_codigo!=$session_codigo) || ($get_codigo1!=$session_codigo1)
	|| ($get_codigo2!=$session_codigo2)
	|| ($get_codigo3!=$session_codigo3)) {
	header('Location: '.$redireccion_error);exit;

}

use PayPal\Api\Payer;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\ShippingAddress;

use PayPal\Exception\PayPalConnectionException;

require 'start.php';

$payer = new Payer();
$details = new Details();
$amount = new Amount();
$transaction = new Transaction();
$payment = new Payment();
$redirectUrls = new RedirectUrls();

conectar2('shopifun', "compras");

//consultar en la base de datos
$query_rs_compra_segundo_paso = "SELECT compras_segundo_paso.compra_email, direcciones.direccion_first_name, direcciones.direccion_last_name, direcciones.direccion_address, direcciones.direccion_address_2, direcciones.direccion_city, direcciones.direccion_country, direcciones.direccion_state, direcciones.direccion_zip, direcciones.direccion_phone  FROM compras_segundo_paso, direcciones WHERE compras_segundo_paso.compra_codigo = '$codigo_compra' AND compras_segundo_paso.compra_direccion = direcciones.id_direccion ORDER BY compras_segundo_paso.id_compra DESC LIMIT 1";
$rs_compra_segundo_paso = mysql_query($query_rs_compra_segundo_paso)or die(mysql_error());
$row_rs_compra_segundo_paso = mysql_fetch_assoc($rs_compra_segundo_paso);
$totalrow_rs_compra_segundo_paso = mysql_num_rows($rs_compra_segundo_paso);

//consultar en la base de datos
$query_rs_compras = "SELECT id_compra, id_producto, cantidad FROM compras_primer_paso WHERE codigo_compra = '$codigo_compra' ORDER BY id_compra DESC";
$rs_compras = mysql_query($query_rs_compras)or die(mysql_error());
$row_rs_compras = mysql_fetch_assoc($rs_compras);
$totalrow_rs_compras = mysql_num_rows($rs_compras);

$lista_producto = NULL;
do {
	$id_compra = $row_rs_compras['id_compra'];
	$id_producto = $row_rs_compras['id_producto'];
	$cantidad = $row_rs_compras['cantidad'];

	if(!$lista_producto) {
		$lista_producto = 'WHERE id_producto = '.$id_producto;
	} else {
		$lista_producto .= ' OR id_producto = '.$id_producto;
	}

	$compra_lista[$id_compra] = $id_producto;
	$compra_lista_cantidad[$id_compra] = $cantidad;
} while ($row_rs_compras = mysql_fetch_assoc($rs_compras));

$fecha_actual = date('Y-m-d H:i:s');
	//consultar en la base de datos
$query_rs_consultar_codigo_descuento = "SELECT id_cupon, cupon_descuento FROM cupones_descuento WHERE cupon_codigo_compra = '$codigo_compra' AND cupon_expiracion > '$fecha_actual' AND cupon_usado = 0";
$rs_consultar_codigo_descuento = mysql_query($query_rs_consultar_codigo_descuento)or die(mysql_error());
$row_rs_consultar_codigo_descuento = mysql_fetch_assoc($rs_consultar_codigo_descuento);
$totalrow_rs_consultar_codigo_descuento = mysql_num_rows($rs_consultar_codigo_descuento);

$precio_descuento_porcentaje = 0;

if($totalrow_rs_consultar_codigo_descuento) {
	$id_cupon_descuento = $row_rs_consultar_codigo_descuento['id_cupon'];
	$precio_descuento_porcentaje = $row_rs_consultar_codigo_descuento['cupon_descuento'];
}
desconectar();

if($lista_producto) {
	conectar2('shopifun', "admin");
//consultar en la base de datos
	$query_rs_productos = "SELECT id_producto, producto_titulo, producto_precio_dolar, producto_precio_shipping_dolar  FROM productos $WHERE";
	$rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
	$row_rs_productos = mysql_fetch_assoc($rs_productos);
	$totalrow_rs_productos = mysql_num_rows($rs_productos);

	desconectar();
}

do {
	$id_producto = $row_rs_productos['id_producto'];
	$producto_titulo = $row_rs_productos['producto_titulo'];
	$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
	$producto_precio_shipping_dolar = $row_rs_productos['producto_precio_shipping_dolar'];

	$array_producto[$id_producto] = $producto_titulo;
	$array_producto_precio[$id_producto] = $producto_precio_dolar;
	$array_producto_shipping[$id_producto] = $producto_precio_shipping_dolar;
} while ($row_rs_productos = mysql_fetch_assoc($rs_productos));

$precio_total = 0;
$shipping = 0;

$descuento = 0;

if($totalrow_rs_productos) { 
	$i=0;
	foreach ($compra_lista as $id_compra => $id_producto) {
		$producto_titulo = $array_producto[$id_producto];
		$producto_precio_dolar = $array_producto_precio[$id_producto];
		$producto_shipping = $array_producto_shipping[$id_producto];

		$cantidad = $compra_lista_cantidad[$id_compra];
		$items[$i] = new Item();
		$items[$i]->setName($producto_titulo)
		->setCurrency('USD')
		->setQuantity($cantidad)
		->setPrice($producto_precio_dolar);

		$precio_total = $precio_total + ($producto_precio_dolar * $cantidad);

		$shipping = $shipping + ($producto_shipping * $cantidad);
		$i++;
	}

	if($precio_descuento_porcentaje) {
		$precio_final = $precio_total + $shipping;
		$descuento = ($precio_descuento_porcentaje * $precio_final) / 100;
	}

	if($descuento != 0) {
		$descuento = $descuento * -1;
		$items[$i] = new Item();
		$items[$i]->setName('Discount')
		->setCurrency('USD')
		->setQuantity(1)
		->setPrice($descuento);
		$precio_total = $precio_total + $descuento;
	}
}

$compra_email = $row_rs_compra_segundo_paso['compra_email'];
$direccion_first_name = $row_rs_compra_segundo_paso['direccion_first_name'];
$direccion_last_name = $row_rs_compra_segundo_paso['direccion_last_name'];
$direccion_address = $row_rs_compra_segundo_paso['direccion_address'];
$direccion_address_2 = $row_rs_compra_segundo_paso['direccion_address_2'];
$direccion_city = $row_rs_compra_segundo_paso['direccion_city'];
$direccion_country = $row_rs_compra_segundo_paso['direccion_country'];
$direccion_state = $row_rs_compra_segundo_paso['direccion_state'];
$direccion_zip = $row_rs_compra_segundo_paso['direccion_zip'];
$direccion_phone = $row_rs_compra_segundo_paso['direccion_phone'];

// Add Shipping Address
$shippingAddress = new ShippingAddress();

$direccion_definitiva = $direccion_address.' '.$direccion_address2;
$direccion_definitiva = trim($direccion_definitiva);

$shippingAddress->setLine1($direccion_definitiva)
->setCity($direccion_city)
->setCountryCode($direccion_country);

if($direccion_state) {
	$shippingAddress->setState($direccion_state);
}

if($direccion_zip) {
	$shippingAddress->setPostalCode($direccion_zip);
}


$payer->setPaymentMethod('paypal');

$itemList = new ItemList();
$itemList->setItems($items)
->setShippingAddress($shippingAddress);

$details->setShipping($shipping)
->setSubtotal($precio_total);

$amount->setCurrency('USD')
->setTotal($precio_total + $shipping)
->setDetails($details);

//Transaction
$transaction->setAmount($amount)
->setItemList($itemList)
->setDescription('Purchase #'.$codigo_compra)
->setInvoiceNumber(uniqid());

//Redirects URLs
$redirectUrls->setReturnUrl('https://www.shopifunny.com/checkout/pay/pal?approved=true&codigo='.$codigo_compra)
->setCancelUrl('https://www.shopifunny.com/checkout/pay/pal?approved=false&codigo='.$codigo_compra);

//Payment
$payment->setIntent('sale')
->setPayer($payer)
->setTransactions([$transaction])
->setRedirectUrls($redirectUrls);

try {
	$payment->create($api);

	$payment_id = $payment->getID();
	$hash = md5($payment_id);
	$_SESSION['paypal_hash'] = $hash;

	conectar2('shopifun', "compras");

	$id_usuario = 22;
	$query = "INSERT INTO transactions_paypal (user_id, payment_id, hash, complete, codigo_compra) 
	VALUES ('$id_usuario','$payment_id' ,'$hash', '0', '$codigo_compra')";
	$result = mysql_query($query);
	$id_transaccion = mysql_insert_id();

	desconectar();
	// Generate and store hasg
	//prepare and execute transaction storage

} catch (PayPalConnectionException $e) {
	echo $e->getCode(); 
	echo $e->getData();
	die($e);
}

$respuesta = "error";
foreach($payment->getLinks() as $link) {
	if($link->getRel() == 'approval_url'){
		$redirectUrl = $link->getHref();
		$array_post['redirectUrl'] = $redirectUrl;
		$respuesta = "OK*****".$redirectUrl;

		if($id_cupon_descuento) {
			conectar2('shopifun', "compras");
			mysql_query("UPDATE cupones_descuento SET 
				cupon_usado='1', 
				cupon_fecha_usado='$fecha_actual', 
				ip_visitante='$ip_visitante'
				WHERE id_cupon='$id_cupon_descuento'");
			desconectar();
		}
	}
}

echo $respuesta;

?>