<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

$nombre = trim($_GET['nombre']);

if(!$nombre) {
	redireccionar_404();
}

conectar2('shopifun', "admin");
//consultar en la base de datos
$query_rs_producto = "SELECT id_producto, producto_url, producto_titulo, foto_portada, producto_modelo, producto_talles, producto_stock, producto_precio_shipping_dolar, producto_colores, producto_precio_dolar  FROM productos WHERE producto_url = '$nombre' OR id_producto = '$nombre' LIMIT 1";
$rs_producto = mysql_query($query_rs_producto)or die(mysql_error());
$row_rs_producto = mysql_fetch_assoc($rs_producto);
$totalrow_rs_producto = mysql_num_rows($rs_producto);

if(!$totalrow_rs_producto) {
	redireccionar_404();
}

$url_actual = $Servidor_url2.$_SERVER['REQUEST_URI'];

$producto = $row_rs_producto['id_producto'];
$producto_url = $row_rs_producto['producto_url'];
$producto_titulo = $row_rs_producto['producto_titulo'];
$foto_portada = $row_rs_producto['foto_portada'];
$producto_modelo = $row_rs_producto['producto_modelo'];
$producto_talles = $row_rs_producto['producto_talles'];
$producto_stock = $row_rs_producto['producto_stock'];
$producto_colores = $row_rs_producto['producto_colores'];

$producto_precio_dolar = $row_rs_producto['producto_precio_dolar'];
$producto_precio_shipping_dolar = $row_rs_producto['producto_precio_shipping_dolar'];


$producto_precio_total = $producto_precio_dolar + $producto_precio_shipping_dolar;
$url_ideal = $Servidor_url.'p/'.$producto_url.'/';

if($url_actual!=$url_ideal) {
	header('location: '.$url_ideal);
	exit; 
}

if($producto_colores) {
	$lista_colores = explode(',', $producto_colores);
	$lista_colores = array_unique($lista_colores);
}

if($producto_modelo) {
	$lista_modelos = explode(',', $producto_modelo);

	//consultar en la base de datos
	$query_rs_modelos = "SELECT id_modelo, modelo_nombre FROM modelos ";
	$rs_modelos = mysql_query($query_rs_modelos)or die(mysql_error());
	$row_rs_modelos = mysql_fetch_assoc($rs_modelos);
	$totalrow_rs_modelos = mysql_num_rows($rs_modelos);

	do {
		$id_modelo = $row_rs_modelos['id_modelo'];
		$modelo_nombre = $row_rs_modelos['modelo_nombre'];

		$modelos_nombres[$id_modelo] = $modelo_nombre;
	} while($row_rs_modelos = mysql_fetch_assoc($rs_modelos));
}

if($producto_talles) {
	$lista_talles = explode(',', $producto_talles);

//consultar en la base de datos
	$query_rs_talles = "SELECT id_talle, talle FROM talles";
	$rs_talles = mysql_query($query_rs_talles)or die(mysql_error());
	$row_rs_talles = mysql_fetch_assoc($rs_talles);
	$totalrow_rs_talles = mysql_num_rows($rs_talles);

	do {
		$id_talle = $row_rs_talles['id_talle'];
		$talle = $row_rs_talles['talle'];

		$talles_nombres[$id_talle] = $talle;
	} while($row_rs_talles = mysql_fetch_assoc($rs_talles));
}



//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, cuerpo, color, modelo, recorte_foto_nombre, recorte_foto_miniatura, nombre_foto, fecha_carga FROM fotos_publicaciones WHERE id_publicacion = $producto ORDER BY orden ASC";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];
	$recorte_foto_miniatura = $row_rs_imagenes['recorte_foto_miniatura'];
	$cuerpo =  $row_rs_imagenes['cuerpo'];

	$nombre_foto = $row_rs_imagenes['nombre_foto'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
	$array_imagenes_miniatura[$id_foto] = $recorte_foto_miniatura;

	$array_foto[$id_foto] =  $row_rs_imagenes['nombre_foto'];
	$array_foto_cuerpo[$id_foto] =  $row_rs_imagenes['cuerpo'];

	$color = $row_rs_imagenes['color'];
	$modelo = $row_rs_imagenes['modelo'];

	if($color>0) {
		$array_foto_color[$id_foto] = $color;
		$array_fotos_mostrar_color[$id_foto] = $recorte_foto_nombre;
		$array_foto_tiene_color[$color] = $id_foto;
	} else {
		if($cuerpo==0) {
			if($modelo==0) {
				$array_fotos_mostrar[$id_foto] = $recorte_foto_nombre;
			}
		}
	}

	$array_fecha_carga[$id_foto] = $row_rs_imagenes['fecha_carga'];
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));

if(!$array_fotos_mostrar) {
	if($array_fotos_mostrar_color) {
		$array_fotos_mostrar = $array_fotos_mostrar_color;
	}
}
$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/';

$ruta_imagenes_recortes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';
$imagen_mostrar = $ruta_imagenes.'recortes/'.$array_recorte_foto_miniatura[$foto_portada];


//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, cuerpo_tipo, contenido FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

do {
	$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
	$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
	$contenido = $row_rs_cuerpo['contenido'];
	$orden = $row_rs_cuerpo['orden'];

	$array_cuerpo[$id_cuerpo] = $contenido;
	$array_cuerpo_tipo[$id_cuerpo] = $cuerpo_tipo;
	$array_cuerpo_orden[$id_cuerpo] = $orden;

	if($cuerpo_tipo=="imagen") {
		$imagen_cuerpo[$contenido] = 1;
	}
} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));

//si llego hasta aca, es porque está todo bien
//consultar en la base de datos
$query_rs_colores = "SELECT id_color, color, color_codigo  FROM colores ";
$rs_colores = mysql_query($query_rs_colores)or die(mysql_error());
$row_rs_colores = mysql_fetch_assoc($rs_colores);
$totalrow_rs_colores = mysql_num_rows($rs_colores);

do {
	$id_color = $row_rs_colores['id_color'];
	$color = $row_rs_colores['color'];
	$color_codigo = $row_rs_colores['color_codigo'];

	$colores_nombres[$id_color] = $color;
	$colores_codigos[$id_color] = $color_codigo;

} while ($row_rs_colores = mysql_fetch_assoc($rs_colores));

desconectar();

$cuenta_regresiva_fecha = '2018-01-01 00:00:00';
$titulo_pagina = "Shopifunny   &raquo; ".$producto_titulo;

$producto_stock = 9;
?>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="<?php echo $Servidor_url;?>js/sticky-kit.js"></script>
<script src="<?php echo $Servidor_url;?>js/imagenSticky.js"></script>
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/imagenSticky.css"> <!-- Resource style -->

<style>

</style>
</head>
<body >

	<div class="content" data-sticky_parent>
		<div class="sidebar" data-sticky_column>
			<?php include('paginas_include/productos/productos-sticky-column.php'); ?>
		</div>

		<div class="main" data-sticky_column>
			<?php include('paginas_include/productos/productos-main-column.php'); ?>
		</div>
	</div>
	<div class="footer">
					<?php include('paginas_include/productos/productos-footer.php'); ?>
	</div>
</body>
</html>