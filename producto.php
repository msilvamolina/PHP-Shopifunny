<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

$nombre = trim($_GET['nombre']);

if(!$nombre) {
	redireccionar_404();
}


//Comenzamos a trabajar en producto
conectar2('shopifun', "admin");
//consultar en la base de datos
$query_rs_producto = "SELECT id_producto, producto_descripcion, producto_precio_dolar_comparado, producto_url, producto_titulo, foto_portada, producto_modelo, producto_talles, producto_stock, producto_precio_shipping_dolar, producto_colores, producto_precio_dolar, producto_tags  FROM productos WHERE producto_url = '$nombre' OR id_producto = '$nombre' LIMIT 1";
$rs_producto = mysql_query($query_rs_producto)or die(mysql_error());
$row_rs_producto = mysql_fetch_assoc($rs_producto);
$totalrow_rs_producto = mysql_num_rows($rs_producto);

if(!$totalrow_rs_producto) {
	redireccionar_404();
}

$url_actual = $Servidor_url2.$_SERVER['REQUEST_URI'];

$producto = $row_rs_producto['id_producto'];
$producto_descripcion = $row_rs_producto['producto_descripcion'];

$producto_url = $row_rs_producto['producto_url'];
$producto_titulo = $row_rs_producto['producto_titulo'];
$foto_portada = $row_rs_producto['foto_portada'];
$producto_modelo = $row_rs_producto['producto_modelo'];
$producto_talles = $row_rs_producto['producto_talles'];
$producto_stock = $row_rs_producto['producto_stock'];
$producto_colores = $row_rs_producto['producto_colores'];
$producto_tags = $row_rs_producto['producto_tags'];

$producto_precio_dolar = $row_rs_producto['producto_precio_dolar'];
$producto_precio_shipping_dolar = $row_rs_producto['producto_precio_shipping_dolar'];

$producto_precio_dolar_comparado = $row_rs_producto['producto_precio_dolar_comparado'];

$shipping_shipping = $producto_precio_shipping_dolar;
$operacion = $producto_precio_dolar / $producto_precio_dolar_comparado;
$operacion2 = $operacion * 100;
$operacion3 = $operacion2 - 100;

$porcentaje_diferencia = calcular_porcentaje_direcia($producto_precio_dolar,$producto_precio_dolar_comparado);


$producto_precio_total = $producto_precio_dolar + $producto_precio_shipping_dolar;
$url_ideal = $Servidor_url.'p/'.$producto_url.'/';

if($url_actual!=$url_ideal) {
	header('location: '.$url_ideal);
	exit; 
}

if($producto_colores) {
	$lista_colores = explode(',', $producto_colores);
	$lista_colores = array_unique($lista_colores);
}

if($producto_modelo) {
	$lista_modelos = explode(',', $producto_modelo);

	//consultar en la base de datos
	$query_rs_modelos = "SELECT id_modelo, modelo_nombre FROM modelos ";
	$rs_modelos = mysql_query($query_rs_modelos)or die(mysql_error());
	$row_rs_modelos = mysql_fetch_assoc($rs_modelos);
	$totalrow_rs_modelos = mysql_num_rows($rs_modelos);

	do {
		$id_modelo = $row_rs_modelos['id_modelo'];
		$modelo_nombre = $row_rs_modelos['modelo_nombre'];

		$modelos_nombres[$id_modelo] = $modelo_nombre;
	} while($row_rs_modelos = mysql_fetch_assoc($rs_modelos));
}

if($producto_talles) {
	$lista_talles = explode(',', $producto_talles);

//consultar en la base de datos
	$query_rs_talles = "SELECT id_talle, talle FROM talles";
	$rs_talles = mysql_query($query_rs_talles)or die(mysql_error());
	$row_rs_talles = mysql_fetch_assoc($rs_talles);
	$totalrow_rs_talles = mysql_num_rows($rs_talles);

	do {
		$id_talle = $row_rs_talles['id_talle'];
		$talle = $row_rs_talles['talle'];

		$talles_nombres[$id_talle] = $talle;
	} while($row_rs_talles = mysql_fetch_assoc($rs_talles));
}



//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, cuerpo, color, modelo, recorte_foto_nombre, recorte_foto_miniatura, nombre_foto, fecha_carga FROM fotos_publicaciones WHERE id_publicacion = $producto ORDER BY orden ASC";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];
	$recorte_foto_miniatura = $row_rs_imagenes['recorte_foto_miniatura'];
	$cuerpo =  $row_rs_imagenes['cuerpo'];

	$nombre_foto = $row_rs_imagenes['nombre_foto'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
	$array_imagenes_miniatura[$id_foto] = $recorte_foto_miniatura;

	$array_foto[$id_foto] =  $row_rs_imagenes['nombre_foto'];
	$array_foto_cuerpo[$id_foto] =  $row_rs_imagenes['cuerpo'];

	$color = $row_rs_imagenes['color'];
	$modelo = $row_rs_imagenes['modelo'];

	if($color>0) {
		$array_foto_color[$id_foto] = $color;
		$array_fotos_mostrar_color[$id_foto] = $recorte_foto_nombre;
		$array_foto_tiene_color[$color] = $id_foto;
	} else {
		if($cuerpo==0) {
			if($modelo==0) {
				$array_fotos_mostrar[$id_foto] = $recorte_foto_nombre;
			}
		}
	}

	$array_fecha_carga[$id_foto] = $row_rs_imagenes['fecha_carga'];
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));

if(!$array_fotos_mostrar) {
	if($array_fotos_mostrar_color) {
		$array_fotos_mostrar = $array_fotos_mostrar_color;
	}
}
$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/';

$ruta_imagenes_recortes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';
$imagen_mostrar = $ruta_imagenes.'recortes/'.$array_recorte_foto_miniatura[$foto_portada];


//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, cuerpo_tipo, contenido FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

do {
	$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
	$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
	$contenido = $row_rs_cuerpo['contenido'];
	$orden = $row_rs_cuerpo['orden'];

	$array_cuerpo[$id_cuerpo] = $contenido;
	$array_cuerpo_tipo[$id_cuerpo] = $cuerpo_tipo;
	$array_cuerpo_orden[$id_cuerpo] = $orden;

	if($cuerpo_tipo=="imagen") {
		$imagen_cuerpo[$contenido] = 1;
	}
} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));

//si llego hasta aca, es porque está todo bien
//consultar en la base de datos
$query_rs_colores = "SELECT id_color, color, color_codigo  FROM colores ";
$rs_colores = mysql_query($query_rs_colores)or die(mysql_error());
$row_rs_colores = mysql_fetch_assoc($rs_colores);
$totalrow_rs_colores = mysql_num_rows($rs_colores);

do {
	$id_color = $row_rs_colores['id_color'];
	$color = $row_rs_colores['color'];
	$color_codigo = $row_rs_colores['color_codigo'];

	$colores_nombres[$id_color] = $color;
	$colores_codigos[$id_color] = $color_codigo;

} while ($row_rs_colores = mysql_fetch_assoc($rs_colores));


$titulo_pagina = "Shopifunny   &raquo; ".$producto_titulo;

$producto_stock = 1000;

$watching = rand ( 15, 167);
$sold_last_hour = rand (123, 500);

$minimo = 234;

if($sold_last_hour > $minimo) {
	$minimo = $sold_last_hour;
}
$sold = rand($minimo, 1000);

$votes = rand(15, 1000);

$wish_list_adds = rand(123, 1292);

$fecha_actual = date('Y-m-d H:i:s');

$una_hora = 60 * 60;
$un_dia = 24 * 60 * 60;

$nuevafecha = strtotime ( '+37 hour' , strtotime ( $fecha_actual ) ) ;
$nuevafecha = date ( 'Y-m-d H:i:s' , $nuevafecha );
$cuenta_regresiva_fecha = $nuevafecha;

$procentaje_sold = $sold / 10;



$panel = 12;
//consultar en la base de datos
$query_rs_panel = "SELECT *  FROM paneles WHERE id_panel = $panel ";
$rs_panel = mysql_query($query_rs_panel)or die(mysql_error());
$row_rs_panel = mysql_fetch_assoc($rs_panel);
$totalrow_rs_panel = mysql_num_rows($rs_panel);

$panel_titulo = $row_rs_panel['panel_titulo'];

$panel_nombre = $row_rs_panel['panel_nombre'];
$panel_seccion = $row_rs_panel['panel_seccion'];
$panel_tipo = $row_rs_panel['panel_tipo'];
$panel_diseno = $row_rs_panel['panel_diseno'];
$panel_imagen_lateral = $row_rs_panel['panel_imagen_lateral'];
$panel_imagen_lateral_link = $row_rs_panel['panel_imagen_lateral_link'];
$panel_imagen_lateral_target = $row_rs_panel['panel_imagen_lateral_target'];

$panel_imagen_lateral_2 = $row_rs_panel['panel_imagen_lateral_2'];
$panel_imagen_lateral_2_link = $row_rs_panel['panel_imagen_lateral_2_link'];
$panel_imagen_lateral_2_target = $row_rs_panel['panel_imagen_lateral_2_target'];

$panel_imagen_destacada = $row_rs_panel['panel_imagen_destacada'];
$panel_imagen_destacada_2 = $row_rs_panel['panel_imagen_destacada_2'];

$panel_facebook_animacion = $row_rs_panel['panel_imagen_destacada_facebook_animacion'];
$panel_facebook_animacion_2 = $row_rs_panel['panel_imagen_destacada_2_facebook_animacion'];

$panel_imagen_destacada_link = $row_rs_panel['panel_imagen_destacada_link'];
$panel_imagen_destacada_target = $row_rs_panel['panel_imagen_destacada_target'];

$panel_imagen_destacada_2_link = $row_rs_panel['panel_imagen_destacada_2_link'];
$panel_imagen_destacada_2_target = $row_rs_panel['panel_imagen_destacada_2_target'];


$producto_precio_shipping_dolar = $row_rs_panel['producto_precio_shipping_dolar'];


$productos_aleatorios = $row_rs_panel['productos_aleatorios'];
$productos = $row_rs_panel['productos'];


$panel_diseno = 6;

//consultar en la base de datos
$query_rs_diseno = "SELECT diseno_fondo, diseno_producto_fondo FROM paneles_diseno WHERE id_diseno = $panel_diseno ";
$rs_diseno = mysql_query($query_rs_diseno)or die(mysql_error());
$row_rs_diseno = mysql_fetch_assoc($rs_diseno);
$totalrow_rs_diseno = mysql_num_rows($rs_diseno);

$panel_fondo = $row_rs_diseno['diseno_fondo'];
$diseno_producto_fondo = $row_rs_diseno['diseno_producto_fondo'];

$LIMITE = 10;
$query_rs_productos = "SELECT * from productos WHERE productos.producto_publicado = 1 ";

$productos_aleatorios = 1;
if($productos_aleatorios) {
	//consultar en la base de datos

	$query_rs_productos .= " ORDER BY RAND() LIMIT $LIMITE";
} else {
	$separar_productos = explode(',', $productos);

	$WHERE = null;
	foreach ($separar_productos as $id_producto) {
		if(!$WHERE) {
			$WHERE = '(productos.id_producto = '.$id_producto;
		} else {
			$WHERE .= ' OR productos.id_producto = '.$id_producto.')';
		}
	}

	$WHERE = 'AND '.$WHERE;
	$query_rs_productos .= " $WHERE LIMIT $LIMITE";
}

$rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
$row_rs_productos = mysql_fetch_assoc($rs_productos);
$totalrow_rs_productos = mysql_num_rows($rs_productos);


//consultar en la base de datos
$query_rs_imagenes_productos = "SELECT id_foto, nombre_foto, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion > 0";
$rs_imagenes_productos = mysql_query($query_rs_imagenes_productos)or die(mysql_error());
$row_rs_imagenes_productos = mysql_fetch_assoc($rs_imagenes_productos);
$totalrow_rs_imagenes_productos = mysql_num_rows($rs_imagenes_productos);

do {
	$id_foto = $row_rs_imagenes_productos['id_foto'];
	$nombre_foto = $row_rs_imagenes_productos['nombre_foto'];
	$recorte_foto_miniatura = $row_rs_imagenes_productos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $recorte_foto_miniatura;
} while ($row_rs_imagenes_productos = mysql_fetch_assoc($rs_imagenes_productos));
desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta property="og:url" content="<?php echo $url_ideal;?>">
	<meta name="description" content="<?php echo $producto_descripcion;?>">
	<meta name="og:title" content="Shopifunny   &raquo; <?php echo $producto_titulo; ?>"/>
	<meta name="twitter:title" content="Shopifunny   &raquo; <?php echo $producto_titulo; ?>" />
	<meta itemprop="name" content="Shopifunny   &raquo; <?php echo $producto_titulo; ?>" />
	<meta name="og:site_name" content="Shopifunny.com"/>
	<meta name="og:description" content="<?php echo $producto_descripcion;?>"/>
	<meta itemprop="description" content="<?php echo $producto_descripcion;?>"/>
	<meta name="Keywords" content="<?php echo $producto_tags; ?>" />
	<meta name="author" content="Shopifunny Team :D" />
	<meta name="rating" content="General" />
	<meta name="distribution" content="global" />
	<meta name="robots" content="index,follow,noodp" />
	<meta name="twitter:card" content="photo" />
	<meta name="twitter:site" content="@shopifunny" />
	<meta name="twitter:creator" content="@shopifunny" />
	<meta name="twitter:domain" content="shopifunny.com" />    
	<link rel="image_src" type="image/jpeg" href="<?php echo $ruta_imagenes.'recortes/'.$array_imagenes[$foto_portada];?>">
	<meta property="og:image" content="<?php echo $ruta_imagenes.'recortes/'.$array_imagenes[$foto_portada];?>" />
	<meta property="og:type" content="image/jpg" />

	<meta property="og:image:type" content="image/jpg" />
	<meta property="og:image:width" content="500" />
	<meta property="og:image:height" content="500" />

	<?php
	//Permisos
	$agregar_slick = 1;
	$agregar_xZoom = 1;
	$agregar_fancybox = 1;
	$bootstrap_viejo = 0;
	include('paginas_include/estructura/head.php'); ?>
	<?php include('paginas_include/estructura/google-tag-manager.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/imagenSticky2.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/producto2.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->



	<style>
	
	.content {
		background: #ffffff !important;
		padding: 0px;
		-webkit-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		margin-bottom: 15px;
	}
	.sidebar {
		-webkit-box-shadow: 5px 6px 25px -7px rgba(0,0,0,0.75);
		-moz-box-shadow: 5px 6px 25px -7px rgba(0,0,0,0.75);
		box-shadow: 5px 6px 25px -7px rgba(0,0,0,0.75);
	}

	.contenido_texto {
		line-height: 20px;
	}

	.main {
		min-height: 550px;
	}

	#barra_sold {
		height: 50px;
		font-size: 20px;
		font-weight: bold;
	}

	#barra_sold .bg-danger {
		background-color: #F44336 !important;
	}

	.div_watching {
		width: 100%;
		float: none;
		font-size: 18px;
		font-weight: bold;
		text-align: center;
	}

	.div_sold {
		width: 100%;
		float: none;
		text-align: center;
		font-size: 18px;
		font-weight: bold;
	}

	@media only screen and (min-width: 1279px) {
		.div_watching {
			width: 50%;
			float: left;
			font-size: 18px;
			font-weight: bold;
			text-align: left;
		}

		.div_sold {
			width: 50%;
			float: left;
			text-align: right;
			font-size: 18px;
			font-weight: bold;
		}
	}

	.contenedor_dias, .contenedor_dias2 {
		width: 100%;
		padding: 0 !important;
	}

	.contenedor_dias {
		margin-top: 10px;
	}

	.contenedor_dias span{
		float: left;
		width: 25%;
		text-align: center;
		font-size: 24px;
		padding: 0 !important;
		margin:0 !important;
		font-weight: bold;
		color: #616161;
	}

	.contenedor_dias2 span{
		float: left;
		width: 25%;
		text-align: center;
		font-size: 20px;
		padding: 0 !important;
		margin:0 !important;
		color: #616161;
	}

	.cotenedor_general_barra_sold {
		margin-bottom: 15px;
	}

	#progressBar {
		width: 100%;
		height: 15px;
		font-weight: bold;
		background-color: #E0E0E0 !important;
		border-radius: 2px; 
	}

	#progressBar div {
		height: 100%;
		color: #fff;
		text-align: right;
		font-size: 20px;
		line-height: 50px;
		width: 0;
	}
	.default {
		background: #E0E0E0;
		border-radius: 2px; 
	}

	#progressBar .bg-danger {
		background-color: #F44336 !important;
	}
	/*
	.default div {
		background-color: #ff9900;
		background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#ff9900), to(#1a82f7)); 
		background: -webkit-linear-gradient(top, #ff9900, #1a82f7); 
		background: -moz-linear-gradient(top, #ff9900, #1a82f7); 
		background: -ms-linear-gradient(top, #ff9900, #1a82f7); 
		background: -o-linear-gradient(top, #ff9900, #1a82f7);
		}*/

		.contenedor_harry {
			width: 100%;
			text-align: center;
			font-weight: bold;
		}
		.contenedor_harry span{
			color: #F44336;
		}

		#botones_sticky {
			margin-top: 10px;
			margin-bottom: 10px;
		}

		.people_watching_now {
			font-weight: bold;
			color: #F44336;
			margin-bottom: 15px;
			margin-top: -10px;
		}
		.boostrap_no_width_left {
			width: 55% !important;
			float: left;
			text-align: left ;
		}
		.boostrap_no_width_right {
			width: 45% !important;
			float: left;
			text-align: right;
		}


		@media only screen and (min-width: 768px) {
			.boostrap_no_width_right {
				text-align: left !important;
			}
		}
		
		.stuck {
			z-index: 998;
		}
	</style>
</head>
<body >
	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
	<div id="share_products" class="white-popup mfp-hide">
		<h3>Share Products</h3>
		<div class="alert alert-success" role="alert" style="display:none" id="compartir_nota_ok">
			<strong>Success!</strong>
		</div>
		<div id="compartir_nota_contenedor">
			<form action="javascript:compartir_nota()">
				<label>Your name</label>
				<input type="text" placeholder="Your name" required id="compartir_nota_nombre">
				<br>
				<label>Your friend's e-mail</label><br>
				<input type="email" placeholder="E-mail" required id="compartir_nota_email"> 
				<br><br>    
				<center>
					<img src="<?php echo $Servidor_url; ?>img/loadermaterial.gif" width="80" style="display:none" id="compartir_nota_cargando">
					<button type="submit" class="btn btn-lg btn-success" id="compartir_nota_boton"><i class="fa fa-envelope fa-fw"></i> Share!</button>
				</center>
				<br>
			</form>
		</div>  
	</div>   
	<main class="cd-main-content">
		<!-- Vertical sticky share icons-->
		<div class="ssk-sticky ssk-right ssk-center" style="z-index: 900!important;">
			<a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $url_ideal; ?>','Share', 'toolbar=0, status=0, width=650, height=450');" title="Share in Facebook!" class="ssk ssk-facebook"></a>
			<a href="#"  rel="nofollow" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo $url_ideal; ?>&via=Shopifunny&text=<?php echo $producto_titulo;?>','Share', 'toolbar=0, status=0, width=650, height=450');"  title="Share in Twitter!" class="ssk ssk-twitter"></a>
			<a href="#" onclick="window.open('https://plus.google.com/share?url=<?php echo $url_ideal; ?>','Sahre', 'toolbar=0, status=0, width=650, height=450');" class="ssk ssk-google-plus"></a>
			<a  href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-custom="true" style="color:#fff" class="ssk ssk-pinterest"></a>
			<a  href="whatsapp://send?text=Shopifunny &raquo; <?php echo $producto_titulo; ?>  <?php echo $url_ideal; ?>" data-action="share/whatsapp/share" class="ssk ssk-whatsapp"></a>
			<a href="#share_products" class="ssk ssk-email open-popup-link"></a>
		</div>
		<?php include('paginas_include/estructura/main.php') ;?>
		<div class="content" data-sticky_parent >
			<div class="sidebar" data-sticky_column>
				<?php include('paginas_include/productos/productos-sticky-column.php'); ?>
			</div>

			<div class="main" data-sticky_column>
				<?php include('paginas_include/productos/productos-main-column.php'); ?>
			</div>
		</div>
		<div class="footer">
			<?php include('paginas_include/productos/productos-footer.php'); ?>
		</div>

		<div id="producto_relacionados">
			
			<div class="encabezado_productos fondo<?php echo $panel_fondo; ?> panel_diseno<?php echo $panel_diseno; ?>" style="padding-right: 0px !important; margin-right: 0px !important; margin-bottom: 20px">

				<div class="panel_titulo">More Products</div>
				
				<div class="clear"></div>

				<div class="nuevo_contenedor_productos">
					<div class="panelproductos">
						<section class="regularRelacionados slider">
							<?php 

							$producto_i = 1;
							do { 
								$id_producto2 = $row_rs_productos['id_producto'];
								$producto_titulo2 = $row_rs_productos['producto_titulo'];
								$producto_esquinero2 = $row_rs_productos['producto_esquinero'];
								$producto_categoria2 = $row_rs_productos['producto_categoria'];
								$producto_seccion2 = $row_rs_productos['producto_seccion'];
								$producto_precio_dolar2 = $row_rs_productos['producto_precio_dolar'];
								$producto_precio_dolar_comparado2 = $row_rs_productos['producto_precio_dolar_comparado'];

								$producto_url2 = $row_rs_productos['producto_url'];

								$foto_portada2 = $row_rs_productos['foto_portada'];
								$producto_precio_shipping_dolar2 = $row_rs_productos['producto_precio_shipping_dolar'];


								$precio_shipping2 = 'Free';

								if($producto_precio_shipping_dolar2) {
									$precio_shipping2 = formato_moneda($producto_precio_shipping_dolar2, '');
								}


								$precio2 = formato_moneda($producto_precio_dolar2, 'dolar');
								$precio_comparado2 = formato_moneda($producto_precio_dolar_comparado2, '');
								$ruta_imagenes2 = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

								$recorte_foto_miniatura2 = $array_fotos[$foto_portada2];

								$i=0;


								$link_producto2 = $Servidor_url.'p/'.$producto_url2.'/';
								?>

								<div class="contenedor_producto_slider fondo<?php echo $diseno_producto_fondo; ?>">
									<div class="contenedor_imagen">
										<div class="contenedor_botones">
											<a onclick="add_to_cart(<?php echo $id_producto2; ?>, <?php echo $producto_i; ?>)">
												<div class="boton_add_to_cart" id="add_to_cart_producto_<?php echo $producto_i; ?>">
													<i class="fa fa-cart-plus"></i>
												</div>
											</a>
											<a onclick="add_to_wish_list(<?php echo $id_producto2; ?>, <?php echo $producto_i; ?>)">
												<div class="boton_add_to_wish_list" id="add_to_wish_list_producto_<?php echo $producto_i; ?>">
													<i class="fa fa-heart-o"></i>
												</div>
											</a>
										</div>
										<a href="<?php echo $link_producto2; ?>">
											<img class="producto_imagen" src="<?php echo $ruta_imagenes2.$recorte_foto_miniatura2; ?>" alt="<?php echo $producto_titulo2; ?>">
										</a>
									</div>
									<a href="<?php echo $link_producto2; ?>">

										<div class="span_precio"><?php echo $precio2; ?><span><?php echo $precio_comparado2; ?></span></div>
										<h3 class="producto_titulo"><?php echo $producto_titulo2; ?></h3>

										<div class="clear"></div>
										<div class="div_contenedora_boton">
											<span class="panel_producto_shipping"><i class="fa fa-truck"></i> <?php echo $precio_shipping2; ?> Shipping!</span>
										</div>
									</a>

								</div> <!-- .cd-single-item -->

								<?php $producto_i++; } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>
							</section>
						</div>
					</div>
				</div>
			</main>
			<?php include('paginas_include/estructura/pie.php') ; ?>

			<script src="<?php echo $ruta_js2; ?>js/foundation.min.js"></script>
			<script src="<?php echo $ruta_js2; ?>js/setup.js"></script>
			<script src="<?php echo $Servidor_url;?>js/jquery-1.11.2.min.js"></script> 
			<script src="<?php echo $Servidor_url;?>js/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="<?php echo $Servidor_url;?>js/waypoints/lib/shortcuts/sticky.min.js"></script>

			<script>

				var sticky = new Waypoint.Sticky({
					element: $('#botones_sticky')[0]
				});


			</script>
			<?php include('paginas_include/estructura/javascript-pie.php');?>	

			<script type="text/javascript">		

				var vendidos = 56;


				progressBar(vendidos, $('#progressBar'), vendidos);

				window.setTimeout( function() {
					vendidos = vendidos + 7;
					progressBar(vendidos, $('#progressBar'), vendidos); 
				}, 5000 );


				function progressBar(percent, $element, vendidos) {
					var progressBarWidth = percent * $element.width() / 100;
					$element.find('div').animate({ width: progressBarWidth }, 500);
				}
				
				$('#countdown_producto2').countdown('<?php echo $cuenta_regresiva_fecha; ?>', function(event) {
					$('#days_producto').html(event.strftime('%D'));
					$('#hours_producto').html(event.strftime('%H'));
					$('#minutes_producto').html(event.strftime('%M'));
					$('#seconds_producto').html(event.strftime('%S'));
				});

				cargar_reviews();

				function cargar_reviews() {
					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/productos/productos-reviews.php?producto=<?php echo $producto; ?>&palabras=<?php echo $producto_tags; ?>",
						success: function (resultado) {
							$("#producto_descripcion_columna1").html(resultado);
						}
					});	
				}



				function activar_foto(color, original, imagen) {
					$("#xzoom-fancy").attr("src","<?php echo $ruta_imagenes_recortes; ?>"+imagen);
					$("#xzoom-fancy").attr("xoriginal","<?php echo $ruta_imagenes; ?>"+original);

					$('.color_imagen').removeClass('color_imagen_activada');
					$('#color_imagen_id_'+color).addClass('color_imagen_activada');
					$('#producto_color_elegido').val(color);
				}

				function activar_foto2(color) {
					$('.color_imagen').removeClass('color_imagen_activada');
					$('#color_imagen_id_'+color).addClass('color_imagen_activada');
					$('#producto_color_elegido').val(color);
				}

				function activar_modelo(modelo) {
					$('.mostrar_modelos').removeClass('modelo_activado');
					$('#producto_modelo'+modelo).addClass('modelo_activado');
					$('#producto_modelo_elegido').val(modelo);
				}

				function activar_talle(talle) {
					$('.mostrar_talles').removeClass('modelo_activado');
					$('#producto_talle'+talle).addClass('modelo_activado');
					$('#producto_talle_elegido').val(talle);
				}

				function add_to_cart_sticky(producto) {
					$('#cart_loader').show();
					//alert(producto);
					$('#add_to_cart_producto_sticky').html('<i class="fa fa-check fa-stack-1x fa-inverse"></i>');

					redireccion = "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-cart.php?producto="+producto;
					var color = $('#producto_color_elegido').val();
					var talle = $('#producto_talle_elegido').val();
					var modelo = $('#producto_modelo_elegido').val();
					var cantidad = $('#producto_cantidad').val();

					redireccion += '&cantidad=' + cantidad + '&color=' + color + '&talle=' + talle + '&modelo=' + modelo;

					$.ajax({
						url: redireccion,
						success: function (resultado) {
							$('#add_to_cart_producto_sticky').html('<i class="fa fa-cart-plus fa-stack-1x fa-inverse"></i>');

							var analizar_resultado = resultado.split("*****");

							$('#cart_count').html(analizar_resultado[0]);
							$('#contenedor_cart').html(analizar_resultado[1]);
							$('#cart_loader').hide();
						}
					});	
				}
				function currency(value, decimals, separators) {
					decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
					separators = separators || ['.', "'", ','];
					var number = (parseFloat(value) || 0).toFixed(decimals);
					if (number.length <= (4 + decimals))
						return number.replace('.', separators[separators.length - 1]);
					var parts = number.split(/[-.]/);
					value = parts[parts.length > 1 ? parts.length - 2 : 0];
					var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
						separators[separators.length - 1] + parts[parts.length - 1] : '');
					var start = value.length - 6;
					var idx = 0;
					while (start > -3) {
						result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
						+ separators[idx] + result;
						idx = (++idx) % 2;
						start -= 3;
					}

					var retorno =  (parts.length == 3 ? '-' : '') + result;

					return retorno;
				}

				function aumentar_cantidad(operacion) {
					var cantidad = $("#producto_cantidad").val();
					var stock = <?php echo $producto_stock; ?>;
					var precio_total = <?php echo $producto_precio_total; ?>;
					if(operacion == 0) {
						if(cantidad > 1) {
							cantidad = parseInt(cantidad) - 1;
						}
					}
					if(operacion == 1) {
						if(cantidad < stock) {
							cantidad = parseInt(cantidad) + 1;
						}
					}

					var precio_con_cantidad = parseInt(cantidad) * precio_total;

					var precio_mostrar = currency(precio_con_cantidad, 2, ',');

					$("#total_price_contenedor").html('US $'+precio_mostrar);
					$("#producto_cantidad").val(cantidad);
					$("#cantidad_txt").html(cantidad);
				}

				function add_to_wish_list_sticky(producto) {
					//alert(producto);
					$('#btn_add_to_wish_list_icon').html('<i class="fa fa-heart"></i> Add to Wish List!');
					var redireccion = "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-wish-list.php?producto="+producto;

					$.ajax({
						url: redireccion,
						success: function (resultado) {
							$('#btn_add_to_wish_list_icon').html('<i class="fa fa-heart"></i> into Wish List!');
						}
					});	
				}

				function buynow() {
					var redireccion = '<?php echo $Servidor_url; ?>buynow/';

					var color = $('#producto_color_elegido').val();
					var talle = $('#producto_talle_elegido').val();
					var modelo = $('#producto_modelo_elegido').val();
					var cantidad = $('#producto_cantidad').val();

					redireccion += cantidad + '-' + color + '-' + talle + '-' + modelo + '/';
					redireccion += '<?php echo $producto_url; ?>/';

					window.location.href = redireccion;

				}

				
				/*
				$('#countdown_producto2').downCount({
					date: '<?php echo $cuenta_regresiva_fecha; ?>'
				}, function () {
					alert('Offer is expired');
				});
				*/
				$(".regularFoto").slick({
					dots: false,
					infinite: true,
					slidesToShow: 2,
					slidesToScroll: 2,
					responsive: [
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
});

				$(".regularRelacionados").slick({
					dots: false,
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 5,
					responsive: [
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 4
						}
					},
					{
						breakpoint: 920,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 680,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					}
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
});


				function compartir_nota() {
					var nombre = document.getElementById("compartir_nota_nombre").value;
					var email = document.getElementById("compartir_nota_email").value;
					var producto = <?php echo $producto; ?>;
					if((nombre)&&(email)) {
						$('#compartir_nota_boton').hide('fast');
						$('#compartir_nota_cargando').show('fast');
						var sendInfo = {
							nombre: nombre,
							email: email,
							producto: producto
						};
						$.ajax({
							type: "POST",
							url: "<?php echo $Servidor_url;?>share/products/",
							success: function (resultado) {
								var partir = resultado.split('OK*****');    
								if(partir[1]) {
									$("#compartir_nota_contenedor").hide('slow');
									$('#compartir_nota_ok').show('slow');           
								}
							},
							data: sendInfo
						});
					} 
				} 
			</script>

			<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
			<script src="<?php echo $Servidor_url;?>js/sticky-kit.js?v=2"></script>
			<script src="<?php echo $Servidor_url;?>js/imagenSticky.js?v=4"></script>

			<?php include('paginas_include/estructura/javascript-pie2.php');?>	

			<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
		</body>
		</html>