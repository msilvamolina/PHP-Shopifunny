<?php include('paginas_include/variables-generales.php');



$panelpromo = 0;
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $Servidor_url;?>apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Servidor_url;?>apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Servidor_url;?>apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Servidor_url;?>apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $Servidor_url;?>apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $Servidor_url;?>apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $Servidor_url;?>apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $Servidor_url;?>apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style2.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style.css?v=5"> <!-- Resource style -->

	<script src="<?php echo $Servidor_url;?>js/modernizr.js"></script> <!-- Modernizr -->

	<title>Shopifunny | Shopping is funny with us!</title>

	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>


	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>

	<link href="<?php echo $Servidor_url; ?>css/estilohome.css?v=4" rel='stylesheet' type='text/css'>

	<link href="<?php echo $Servidor_url; ?>css/productos-grilla.css?v=1" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />


	<style type="text/css">
	
	.cd-main-header {
		background: #ee3840;
	}
	
	body {
		background-color: #eaeaea;


	} 
	.cd-main-content {
		background: transparent !important ;

	} 

	.ecabezado_principal {
	}

	.oferta_main {
		height: 423px;
		background: #e31b1e url('https://www.shopifunny.com/img/fondoweb.jpg') no-repeat center center;
	}

	.oferta_main img {
		height: 423px;
		width: 365px;
	}

	.cd-logo {
		position: absolute;
		top: 0px;
		left: 0px;
	}
	.cd-logo img {
		display: block;
		width: 216px;
	}
	.cd-main-content {
		padding-top: 50px;
	}
	@media only screen and (min-width: 1169px) {
		.cd-logo img {
			display: block;
			width: 344px;
		}
		.cd-main-content {
			padding-top: 80px;

		}
	}

	.cd-main-header {
		position: fixed;
		width: 100%;
	}
	
	

</style>
</head>
<body  onload="countDown();">
	<?php include('paginas_include/home/header.php'); ?>
	<main class="cd-main-content">
		
		<div class="ecabezado_principal">
			<?php include('paginas_include/home/masterslider.php'); ?>
			
			<div class="oferta_main">
				<img src="<?php echo $Servidor_url; ?>img/oferta.jpg" alt="">
			</div>
		</div>

		<img src="<?php echo $Servidor_url; ?>img/banner.jpg" class="banner" alt="">

		<div id="contador"></div>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

	</main>

	<?php include('paginas_include/home/navbar.php'); ?>

	<script src="<?php echo $Servidor_url;?>js/jquery-2.1.1.js"></script>
	<script src="<?php echo $Servidor_url;?>js/jquery.mobile.custom.min.js"></script>
	<script src="<?php echo $Servidor_url;?>js/main.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url;?>js/main2.js"></script> <!-- Resource jQuery -->

	<script type="text/javascript">		

		var slider = new MasterSlider();

		slider.control('arrows' ,{insertTo:'#masterslider'});	
		slider.control('bullets');	

		slider.setup('masterslider' , {
			width:1280,
			height:600,
			autoplay: true,
			speed:20
		});


		function mostrar_producto(producto) {
			$('#producto_nombre_'+producto).show(); 
		}
		function ocultar_producto(producto) {
			$('#producto_nombre_'+producto).hide(); 
		}
	</script>
</body>
</html>