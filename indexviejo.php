<?php include('paginas_include/variables-generales.php');


$panelpromo = 0;

$producto_i = 0; 

$titulo_pagina = 'Shopifunny - Shopping is funny with us!';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('paginas_include/estructura/head.php'); ?>
	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>

	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>
	<link href="<?php echo $Servidor_url; ?>css/productos-grilla.css?v=2" rel='stylesheet' type='text/css'>


</head>
<body >
	<?php include('paginas_include/estructura/barra-top.php') ; ?>
	<main class="cd-main-content">
		<div class="ecabezado_principal">
			<?php include('paginas_include/home/masterslider.php'); ?>
			
			<div class="oferta_main">
				<img src="<?php echo $Servidor_url; ?>img/dadbag2.png" alt="">
			</div>
		</div>

		<img src="<?php echo $Servidor_url; ?>img/banner-medios2.png" class="banner_shopping" alt="">

		<div class="encabezado_productos">
			<h1>Flash Deals</h1>
			<h2>Ends in: 02 : 29 : 28</h2>
			<h4>New Deals</h4>
			<h3>View More</h3>

			<div class="clear"></div>
			<div class="nuevo_contenedor_productos">
				<?php include('paginas_include/home/panelproductos.php'); ?>
			</div>
		</div>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<div class="encabezado_productos fondo_negro">
			<h1>Black Friday Specials Offers</h1>
			<h2>Ends in: 02 : 29 : 28</h2>
			<h3>View More</h3>

			<div class="clear"></div>
			<div class="nuevo_contenedor_productos">
				<?php $producto_i = 4; include('paginas_include/home/panelproductos.php'); ?>
			</div>
		</div>


		<?php include('paginas_include/home/panelpromochristmas.php'); ?>

		<div class="encabezado_productos fondo_negro">
			<h1>Funny Christmas Specials Offers</h1>
			<h3>View More</h3>

			<div class="clear"></div>
			<div class="nuevo_contenedor_productos">
				<?php $producto_i = 4; include('paginas_include/home/panelproductos.php'); ?>
			</div>
		</div>
		<br><br><br><br><br><br><br><br>

	</main>


	<?php include('paginas_include/estructura/javascript-pie.php');?>
</script>
</body>
</html>

<script type="text/javascript">		

	var slider = new MasterSlider();

	slider.control('arrows' ,{insertTo:'#masterslider'});	
	slider.control('bullets', {autohide:false});	

	slider.setup('masterslider' , {
		width:1280,
		height:600,
		autoplay: true,
		speed:20
	});


	function mostrar_producto(producto) {
		$('#producto_nombre_'+producto).show(); 
	}
	function ocultar_producto(producto) {
		$('#producto_nombre_'+producto).hide(); 
	}
</script>
</body>
</html>