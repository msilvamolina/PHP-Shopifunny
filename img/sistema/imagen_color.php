<?php

function hexToRgb($hex, $alpha = false) {
 $hex      = str_replace('#', '', $hex);
 $length   = strlen($hex);
 $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
 $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
 $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
 if ( $alpha ) {
  $rgb['a'] = $alpha;
}
return $rgb;
}

Header("Content-type: image/png");
$im = imagecreate(200,200);
$color = trim($_GET['color']);

if(!$color) {
	$color = 'ff9900';
}

$nuevo_color = hexToRgb($color);

$fondo=imagecolorallocate ($im, $nuevo_color['r'], $nuevo_color['g'], $nuevo_color['b']);

Imagefill ($im, 0, 0, $fondo);
Imagepng($im);
Imagedestroy($im);
?>