<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');


$titulo_pagina = "Shopifunny   &raquo; Help Center";
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php
	//Permisos
	$agregar_slick = 1;

	include('paginas_include/estructura/head.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/help.css?v=142"> <!-- Resource style -->

	<style>
	.contenedor_noticia {
		margin-bottom: 20px;
		padding: 30px;
	}
	.titulo_pagina {
		font-weight: bold;
		font-size: 32px;
	}

	.contenedor_elementos {
		width: 100%;
	}

	.contenedor_producto_busqueda {
		width: 99%;
		margin: 0.5%;
		float: left;
		background: #fff;
		border: 2px solid #000;
	}
	@media only screen and (min-width: 480px) {
		.contenedor_producto_busqueda {
			width: 49%;
		}
	}

	@media only screen and (min-width: 680px) {
		.contenedor_producto_busqueda {
			width: 32%;
		}
	}

	@media only screen and (min-width: 920px) {
		.contenedor_producto_busqueda {
			width: 24%;
		}
	}

	@media only screen and (min-width: 1200px) {
		.contenedor_producto_busqueda {
			width: 19%;
		}
	}

	.contenedor_producto_busqueda:hover {
		-webkit-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);	
	}

	.nuevo_contenedor_botones {
		position: absolute;
	}

	.contenedor_elementos2 {
		padding: 20px;
	}

	p {
		text-align: left;
	}
</style>
</head>
<body >

	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
	<main class="cd-main-content">
		<section class="cd-faq">
			<ul class="cd-faq-categories">
				<li><a class="selected" href="#SHIPPING">SHIPPING</a></li>
				<li><a href="#ORDERING">ORDERING</a></li>
				<li><a href="#PROMOTIONS">PROMOTIONS</a></li>
				<li><a href="#RETURN">RETURN AND REFUND POLICY</a></li>
				<li><a href="#CANCELLING">CANCELLING ORDERS</a></li>
				<li><a href="#RESELLING">RESELLING</a></li>
			</ul> <!-- cd-faq-categories -->

			<div class="cd-faq-items">
				<ul id="SHIPPING" class="cd-faq-group">
					<li class="cd-faq-title"><h2>SHIPPING</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">HOW MUCH DOES YOUR SHIPPING COST?</a>
						<div class="cd-faq-content">
							<p>Every product from Shopifunny comes with Free Worldwide Shipping, no matter where you live.</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">HOW LONG DOES SHIPPING TAKE?</a>
						<div class="cd-faq-content">
							<p>All of our products come with Free Worldwide Shipping!<br><br>
								Shipping time depends on your country and also the collection you purchase from.<br>Most products take on average 2-3 weeks to be delivered.
							</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">WHERE IS MY ORDER?</a>
						<div class="cd-faq-content">
							<p>If you have received an order confirmation email from us, don't panic! It is in safe hands, it will be delivered. You should also check your email for your tracking code.</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">HOW CAN I TRACK MY ORDER?</a>
						<div class="cd-faq-content">
							<p>Your order can easily be tracked at Shopifunny tracking. To find your tracking number check your email.</p>
						</div> <!-- cd-faq-content -->
					</li>
					<li>
						<a class="cd-faq-trigger" href="#0">WHY IS MY TRACKING PAGE EMPTY?</a>
						<div class="cd-faq-content">
							<p>It might be due to the fact that your order was shipped out recently. Just hold on tight. It should appear in a few days.</p>
						</div> <!-- cd-faq-content -->
					</li>
					<li>
						<a class="cd-faq-trigger" href="#0">IT HAS BEEN OVER THE AVERAGE DELIVERY TIME NOW, WHAT SHOULD I DO?</a>
						<div class="cd-faq-content">
							<p>First of all, we are sorry it has taken longer than expected. While it does not happen often, some deliveries can take longer than the estimated time displayed on our website. Just to be sure, please check with your post office in case they failed to notify you. Also, send us an email with all the details you can provide by using the contact form.
							Your order can easily be tracked at Shopifunny tracking. To find your tracking number check your email.</p>
						</div> <!-- cd-faq-content -->
					</li>
				</ul> <!-- cd-faq-group -->

				<ul id="ORDERING" class="cd-faq-group">
					<li class="cd-faq-title"><h2>ORDERING</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">HOW CAN I PAY ON SHOPIFUNNY?</a>
						<div class="cd-faq-content">
							<p>It's quite easy. Just proceed to checkout and choose to pay via either PayPal/All major credit and debit cards.</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">WHAT CREDIT/DEBIT CARDS ARE SUPPORTED?</a>
						<div class="cd-faq-content">
							<p>We support Visa, Mastercard and American Express.</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">WHEN IS MONEY CHARGED AFTER I ORDER?</a>
						<div class="cd-faq-content">
							<p>Money is charged from your credit/debit card or Paypal account immediately after your order has been processed.</p>
						</div> <!-- cd-faq-content -->
					</li>
				</ul> <!-- cd-faq-group -->

				<ul id="RETURN" class="cd-faq-group">
					<li class="cd-faq-title"><h2>RETURN AND REFUND POLICY</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">WHAT IS YOUR RETURN POLICY?</a>
						<div class="cd-faq-content">
							<p>
								All products from Shopifunny come with a 14-day return policy. You are eligible to return your product(s) for an exchange, gift card or a refund within 14 days of receiving it.<br><br>

								We can only accept your return if the profuct is unused, its original protective stickers have not been removed and it is returned in its original packaging, which is in new condition.<br><br>

								Please note that you may be subject to shipping expenses when returning your product(s), we are not responsible for lost packages during the returns process.
							</p>
						</div> <!-- cd-faq-content -->
					</li>

					<li>
						<a class="cd-faq-trigger" href="#0">I AM UNHAPPY, HOW CAN I GET A REFUND?</a>
						<div class="cd-faq-content">
							<p>We are very sorry for your inconvenience, please Contact Us and we will help you out with whatever problem you are experiencing!?</p>
						</div> <!-- cd-faq-content -->
					</li>

				</ul> <!-- cd-faq-group -->

				<ul id="PROMOTIONS" class="cd-faq-group">
					<li class="cd-faq-title"><h2>PROMOTIONS</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">WHERE CAN I FIND A DISCOUNT CODE?</a>
						<div class="cd-faq-content">
							<p>We send out discount codes regularly to those who are subscribed to our email list. If you wish to subscribe, enter your email directly below.</p>
						</div> <!-- cd-faq-content -->
					</li>
				</ul> <!-- cd-faq-group -->

				<ul id="CANCELLING" class="cd-faq-group">
					<li class="cd-faq-title"><h2>CANCELLING ORDERS</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">I WANT TO CANCEL MY ORDER, HOW CAN I DO THAT?</a>
						<div class="cd-faq-content">
							<p>If you'd like to cancel your order, send us an email <a href="mailto:info@shopifunny.com">info@shopifunny.com</a> or contact us here. We can cancel your order as long as your items have not been shipped yet. Please note that your items might have been shipped already, but you just haven't got the notification email yet. The emails are sent out after the tracking codes are updated.</p>
						</div> <!-- cd-faq-content -->
					</li>

				</ul> <!-- cd-faq-group -->

				<ul id="RESELLING" class="cd-faq-group">
					<li class="cd-faq-title"><h2>RESELLING</h2></li>
					<li>
						<a class="cd-faq-trigger" href="#0">I WOULD LIKE TO RESELL YOUR PRODUCTS, HOW CAN I DO THAT?</a>
						<div class="cd-faq-content">
							<p>If you would like to be a reseller, we can give you a special discount code, so you can refer people to our website and earn money per every customer referred. 
							If you would like to resell products in a physical store, send us an email by using our contact form. We can work out a special price depending on the size of your order.</p>
						</div> <!-- cd-faq-content -->
					</li>

				</ul> <!-- cd-faq-group -->
			</div> <!-- cd-faq-items -->
			<a href="#0" class="cd-close-panel">Close</a>
		</section> <!-- cd-faq -->
	</main>
	<?php include('paginas_include/estructura/pie.php') ; ?>
	<?php include('paginas_include/estructura/javascript-pie.php');?>
	<?php include('paginas_include/estructura/javascript-pie2.php');?>	
	<script src="<?php echo $Servidor_url; ?>js/help.js?v=2"></script>

</body>
</html>