<?php include('paginas_include/variables-generales.php');



$panelpromo = 0;

$producto_i = 0; 
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $Servidor_url;?>apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Servidor_url;?>apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Servidor_url;?>apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Servidor_url;?>apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $Servidor_url;?>apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $Servidor_url;?>apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $Servidor_url;?>apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $Servidor_url;?>apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style2.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style.css?v=5"> <!-- Resource style -->

	<script src="<?php echo $Servidor_url;?>js/modernizr.js"></script> <!-- Modernizr -->

	<title>Shopifunny - Shopping is funny with us!</title>

	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>


	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>

	<link href="<?php echo $Servidor_url; ?>css/estilohome.css?v=5" rel='stylesheet' type='text/css'>

	<link href="<?php echo $Servidor_url; ?>css/productos-grilla.css?v=1" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />


	<style type="text/css">
	
	.cd-main-header {
		background: #ee3840;
	}
	
	body {
		background-color: #eaeaea;


	} 
	.cd-main-content {
		background: transparent !important ;

	} 

	.ecabezado_principal {
	}

	.oferta_main {
		height: 404px;
		background: #e31b1e url('https://www.shopifunny.com/img/fondoweb.jpg') no-repeat center center;
	}

	.oferta_main img {
		height: 404px;
		width: 365px;
	}

	.cd-logo {
		position: absolute;
		top: 0px;
		left: 0px;
	}
	.cd-logo img {
		display: block;
		width: 216px;
	}
	.cd-main-content {
		margin-top: 0px;
	}
	

	.banner_principal {
		width: 100%;
		/* fallback */
		background-color: ;
		background: #1264b4 url('https://www.shopifunny.com/img/fondo-navidad.jpg') repeat-x;
		text-align: center !important;
		padding: 0;
		margin: 0;
	}

	.nuevo_header {
		background: #fff;
		height: 100px;
		width: 100%;

	}

	.logo_nuevo {
		width: 220px;
		float: left;
	}

	.barra_header {
		background: #f6f6f6;
		height: 35px;
		border-bottom: 1px solid #e4e4e4;
		width: 100%;
	}

	.form_busqueda {

	}

	.search_input {
		float: left;
		padding: 10px;
		width: 563px;
		margin-top: 25px;
		margin-left: 25px;
	}

	.btnbuscar {
		float: left;
		margin-top: 25px;
		margin-left: 10px;
	}

	.contenedor_botones {
		float: left;
		width: 365px;
		height: 80px;
		margin-left: 15px;
	}

	.boton_arriba {
		float: left;
		width: 33.333%;
		height: 80px;
		text-align: center !important;
	}

	.boton_arriba img{
		width: 80%;
		margin: 0 auto;
	}

	.boton_verde {
		background-color: #4CAF50;
		background-repeat: repeat-y;
		/* Safari 4-5, Chrome 1-9 */
		background: -webkit-gradient(linear, left top, right top, from(#4CAF50), to(#388E3C));
		/* Safari 5.1, Chrome 10+ */
		background: -webkit-linear-gradient(top, #4CAF50, #388E3C);
		/* Firefox 3.6+ 4CAF50		background: -moz-linear-gradient(top, #2E7D32, #2E7D32);
		/* IE 10 */
		background: -ms-linear-gradient(top, #4CAF50, #388E3C);
		/* Opera 11.10+ */
		background: -o-linear-gradient(top, #4CAF50, #388E3C);
	}

	.boton_amarillo {
		background-color: #FFEB3B;
		background-repeat: repeat-y;
		/* Safari 4-5, Chrome 1-9 */
		background: -webkit-gradient(linear, left top, right top, from(#FFEB3B), to(#FBC02D));
		/* Safari 5.1, Chrome 10+ */
		background: -webkit-linear-gradient(top, #FFEB3B, #FBC02D);
		/* Firefox 3.6+ 4CAF50		background: -moz-linear-gradient(top, #2E7D32, #2E7D32);
		/* IE 10 */
		background: -ms-linear-gradient(top, #FFEB3B, #FBC02D);
		/* Opera 11.10+ */
		background: -o-linear-gradient(top, #FFEB3B, #FBC02D);
	}

	.boton_azul {
		background-color: #2196F3;
		background-repeat: repeat-y;
		/* Safari 4-5, Chrome 1-9 */
		background: -webkit-gradient(linear, left top, right top, from(#2196F3), to(#1976D2));
		/* Safari 5.1, Chrome 10+ */
		background: -webkit-linear-gradient(top, #2196F3, #1976D2);
		/* Firefox 3.6+ 4CAF50		background: -moz-linear-gradient(top, #2E7D32, #2E7D32);
		/* IE 10 */
		background: -ms-linear-gradient(top, #2196F3, #1976D2);
		/* Opera 11.10+ */
		background: -o-linear-gradient(top, #2196F3, #1976D2);
	}

	.banner_top {
		padding: 0;
		margin-top: 5px;
	}

	.banner_shopping {
		width: 100%;
		margin-top: 1%;
		background-color: #CDDC39;
		background-repeat: repeat-y;
		/* Safari 4-5, Chrome 1-9 */
		background: -webkit-gradient(linear, left top, right top, from(#FFEB3B), to(#CDDC39));
		/* Safari 5.1, Chrome 10+ */
		background: -webkit-linear-gradient(top, #FFEB3B, #CDDC39);
		/* Firefox 3.6+ 4CAF50		background: -moz-linear-gradient(top, #2E7D32, #2E7D32);
		/* IE 10 */
		background: -ms-linear-gradient(top, #FFEB3B, #CDDC39);
		/* Opera 11.10+ */
		background: -o-linear-gradient(top, #FFEB3B, #CDDC39);
	}
</style>
</head>
<body  onload="countDown();">
	
	<div class="banner_principal">
		<img src="<?php echo $Servidor_url; ?>img/banner-top2.png" class="banner_top" alt="">
	</div>
	<div class="nuevo_header">

		<div class="nuevo_header_contenedor">
			<img src="<?php echo $Servidor_url; ?>img/logo_nuevo.png" class="logo_nuevo" alt="">

			<form class="form_busqueda">
				<input class="search_input" type="text" placeholder="I'm shopping for.." aria-label="Search">

				<input type="image" class="btnbuscar" src="<?php echo $Servidor_url; ?>img/btnbuscar.png" />

				<div class="contenedor_botones">
					<div class="boton_arriba boton_verde">
						<img src="<?php echo $Servidor_url; ?>img/buyer3.png" alt="">
					</div>
					<div class="boton_arriba boton_amarillo">
						<img src="<?php echo $Servidor_url; ?>img/wish2.png" alt="">
					</div>
					<div class="boton_arriba boton_azul">
						<img src="<?php echo $Servidor_url; ?>img/shipto.png" alt="">
					</div>
				</div>
			</form>
			
			
		</div>
	</div>




	<main class="cd-main-content">
		<div class="ecabezado_principal">
			<?php include('paginas_include/home/masterslider.php'); ?>
			
			<div class="oferta_main">
				<img src="<?php echo $Servidor_url; ?>img/dadbag2.png" alt="">
			</div>
		</div>

		<img src="<?php echo $Servidor_url; ?>img/shopping.png" class="banner_shopping" alt="">

		<div id="contador"></div>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

	</main>

	<script src="<?php echo $Servidor_url;?>js/jquery-2.1.1.js"></script>
	<script src="<?php echo $Servidor_url;?>js/jquery.mobile.custom.min.js"></script>
	<script src="<?php echo $Servidor_url;?>js/main.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url;?>js/main2.js"></script> <!-- Resource jQuery -->

	<script type="text/javascript">		

		var slider = new MasterSlider();

		slider.control('arrows' ,{insertTo:'#masterslider'});	
		slider.control('bullets');	

		slider.setup('masterslider' , {
			width:1280,
			height:600,
			autoplay: true,
			speed:20
		});


		function mostrar_producto(producto) {
			$('#producto_nombre_'+producto).show(); 
		}
		function ocultar_producto(producto) {
			$('#producto_nombre_'+producto).hide(); 
		}
	</script>
</body>
</html>