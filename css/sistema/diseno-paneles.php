<?php include('../../paginas_include/variables-generales.php'); 

$fondo = trim($_GET['fondo']);

conectar2('shopifun', "admin");

//consultar en la base de datos
$query_rs_paneles_diseno = "SELECT * FROM paneles_diseno";
$rs_paneles_diseno = mysql_query($query_rs_paneles_diseno)or die(mysql_error());
$row_rs_paneles_diseno = mysql_fetch_assoc($rs_paneles_diseno);
$totalrow_rs_paneles_diseno = mysql_num_rows($rs_paneles_diseno);


header("Content-type: text/css", true);

do{
	$id_diseno = $row_rs_paneles_diseno['id_diseno'];
	$diseno_fondo = $row_rs_paneles_diseno['diseno_fondo'];
	$diseno_color_txt = $row_rs_paneles_diseno['diseno_color_txt'];
	$diseno_color_link = $row_rs_paneles_diseno['diseno_color_link'];
	$diseno_color_cuenta_regresiva_txt = $row_rs_paneles_diseno['diseno_color_cuenta_regresiva_txt'];
	$diseno_color_cuenta_regresiva_bg = $row_rs_paneles_diseno['diseno_color_cuenta_regresiva_bg'];
	$diseno_producto_color_borde = $row_rs_paneles_diseno['diseno_producto_color_borde'];
	$diseno_producto_fondo = $row_rs_paneles_diseno['diseno_producto_fondo'];
	$diseno_producto_btn_add_to_cart_color_txt = $row_rs_paneles_diseno['diseno_producto_btn_add_to_cart_color_txt'];
	$diseno_producto_btn_add_to_cart_color_bg = $row_rs_paneles_diseno['diseno_producto_btn_add_to_cart_color_bg'];
	$diseno_producto_cartucho_precio_color_txt = $row_rs_paneles_diseno['diseno_producto_cartucho_precio_color_txt'];
	$diseno_producto_cartucho_precio_color_bg = $row_rs_paneles_diseno['diseno_producto_cartucho_precio_color_bg'];
	$diseno_producto_color_titulo = $row_rs_paneles_diseno['diseno_producto_color_titulo'];
	$diseno_producto_color_txt = $row_rs_paneles_diseno['diseno_producto_color_txt'];

	$diseno_producto_btn_add_to_wish_list_color_bg = $row_rs_paneles_diseno['diseno_producto_btn_add_to_wish_list_color_bg'];
	$diseno_producto_btn_add_to_wish_list_color_txt = $row_rs_paneles_diseno['diseno_producto_btn_add_to_wish_list_color_txt'];

	$diseno_color_slider_bg = $row_rs_paneles_diseno['diseno_color_slider_bg'];
	$diseno_color_slider_txt = $row_rs_paneles_diseno['diseno_color_slider_txt'];

	$agregar_w = '.png';

	if($diseno_color_slider_txt) {
		$agregar_w = '-w.png';
	}
	?>
	.panel_diseno<?php echo $id_diseno; ?> .panel_titulo{
	color: #<?php echo $diseno_color_txt; ?>; 
}

.panel_diseno<?php echo $id_diseno; ?> .ul_cuenta_regresiva {
background: #<?php echo $diseno_color_cuenta_regresiva_bg; ?>;
color: #<?php echo $diseno_color_cuenta_regresiva_txt; ?>;
}
.panel_diseno<?php echo $id_diseno; ?> a, .panel_diseno<?php echo $id_diseno; ?> ul li {
color: #<?php echo $diseno_color_link; ?>;
}

.panel_diseno<?php echo $id_diseno; ?> .contenedor_producto_slider {
border:3px solid #<?php echo $diseno_producto_color_borde; ?>;
}

.panel_diseno<?php echo $id_diseno; ?> .boton_add_to_cart {
background: #<?php echo $diseno_producto_btn_add_to_cart_color_bg; ?>;
}
.panel_diseno<?php echo $id_diseno; ?> .boton_add_to_wish_list {
background: #<?php echo $diseno_producto_btn_add_to_wish_list_color_bg; ?>;
}
.panel_diseno<?php echo $id_diseno; ?> .boton_add_to_cart i{
color: #<?php echo $diseno_producto_btn_add_to_cart_color_txt; ?>;
opacity:1;
}
.panel_diseno<?php echo $id_diseno; ?> .boton_add_to_wish_list i{
color: #<?php echo $diseno_producto_btn_add_to_wish_list_color_txt; ?>;
opacity:1;
}
.panel_diseno<?php echo $id_diseno; ?> .boton_add_to_cart:hover i, .panel_diseno<?php echo $id_diseno; ?> .boton_add_to_wish_list:hover i{
opacity:0.8;
}
.panel_diseno<?php echo $id_diseno; ?> .span_precio {
background:#<?php echo $diseno_producto_cartucho_precio_color_bg; ?>;
color:#<?php echo $diseno_producto_cartucho_precio_color_txt; ?>;
}

.panel_diseno<?php echo $id_diseno; ?> .producto_titulo {
color:#<?php echo $diseno_producto_color_titulo; ?>;
}

.panel_diseno<?php echo $id_diseno; ?>  .panel_producto_shipping {
color:#<?php echo $diseno_producto_color_txt; ?>;
}


.panel_diseno<?php echo $id_diseno; ?> .slick-prev {
    background: #<?php echo $diseno_color_slider_bg; ?> url('<?php echo $Servidor_url; ?>img/slider-left<?php echo $agregar_w; ?>') center center no-repeat;
}


.panel_diseno<?php echo $id_diseno; ?> .slick-next {
    background: #<?php echo $diseno_color_slider_bg; ?> url('<?php echo $Servidor_url; ?>img/slider-right<?php echo $agregar_w; ?>') center center no-repeat;
}


<?php } while($row_rs_paneles_diseno = mysql_fetch_assoc($rs_paneles_diseno));?>