<?php include('paginas_include/variables-generales.php') ; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shopifunny</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <link rel="stylesheet" id="ms-fonts" href="https://fonts.googleapis.com/css?family=Playfair+Display:regular|Lato:regular" type="text/css" media="all" />
  <!-- Base MasterSlider style sheet -->
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/assets/css/masterslider.main.css" />
  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/css/style.css" />
  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/assets/js/jquery.easing.min.js"></script>
  <!-- Master Slider -->
  <script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/assets/js/masterslider.min.js"></script>


  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />

  <!-- Bootstrap core CSS -->
  <link href="<?php echo $Servidor_url; ?>recursos/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="<?php echo $Servidor_url; ?>css/navbar-top-fixed.css" rel="stylesheet" />

  <style>
  body {
    background: #80807f !important;
  }
</style>
</head>

<body>

  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Shopifunny</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </nav>

  <div class="container">
    <br><br> <br><br> <br><br>
   <!-- MasterSlider -->
   <div id="P_masterslider" class="master-slider-parent ms-parent-id-20" >
    <!-- MasterSlider Main -->
    <div id="masterslider" class="master-slider ms-skin-default" >
      <div class="ms-overlay-layers">
        <img class="ms-layer"
        src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/blank.gif"
        data-src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/bride-controller-arrow-nxt.png"
        alt=""
        style=""
        data-ease="easeOutQuint"
        data-type="image"
        data-action="next"
        data-offset-x="50"
        data-offset-y="0"
        data-origin="mr"
        data-position="normal" />
        <img class="ms-layer"
        src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/blank.gif"
        data-src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/bride-controller-arrow-prv.png"
        alt=""
        style=""
        data-ease="easeOutQuint"
        data-type="image"
        data-action="previous"
        data-offset-x="104"
        data-offset-y="0"
        data-origin="mr"
        data-position="normal" />
      </div>
      <div class="ms-slide" data-delay="5.2" data-fill-mode="fill" >
        <img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/blank.gif" alt="" title="bride-bg-slide1" data-src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/bride-bg-slide1.jpg" />
        <div class="ms-layer msp-cn-152-10"
        style=""
        data-effect="t(true,n,85,n,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="3200"
        data-ease="easeOutQuart"
        data-offset-x="50"
        data-offset-y="0"
        data-origin="ml"
        data-position="normal"
        data-masked="true">Spring Bridal Accessoacries</div>
        <a href="#"
        target="_self"
        class="ms-layer msp-cn-152-11 ms-btn ms-btn-box ms-btn-n msp-preset-btn-186"
        data-effect="t(true,n,n,666,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="2400"
        data-delay="600"
        data-ease="easeOutQuart"
        data-type="button"
        data-offset-x="0"
        data-offset-y="50"
        data-origin="bc"
        data-position="normal" >New Arrivals</a>
      </div>
      <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
        <img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/blank.gif" alt="" title="bride-bg-slide1" data-src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/bride-bg-slide2.jpg" />
        <div class="ms-layer msp-cn-152-12"
        style=""
        data-effect="t(true,n,85,n,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="3200"
        data-ease="easeOutQuart"
        data-offset-x="50"
        data-offset-y="0"
        data-origin="ml"
        data-position="normal"
        data-masked="true">Vintage Wedding Dresses</div>
        <a href="#"
        target="_self"
        class="ms-layer msp-cn-152-13 ms-btn ms-btn-box ms-btn-n msp-preset-btn-186"
        data-effect="t(true,n,n,666,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="2400"
        data-delay="600"
        data-ease="easeOutQuart"
        data-type="button"
        data-offset-x="0"
        data-offset-y="50"
        data-origin="bc"
        data-position="normal" >View Collection</a>
      </div>
      <div class="ms-slide" data-delay="5" data-fill-mode="fill" >
        <img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/blank.gif" alt="" title="bride-bg-slide1" data-src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/modern-sliders/Bride/images/bride-bg-slide3.jpg" />
        <div class="ms-layer msp-cn-152-14"
        style=""
        data-effect="t(true,n,85,n,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="3200"
        data-ease="easeOutQuart"
        data-offset-x="50"
        data-offset-y="0"
        data-origin="ml"
        data-position="normal"
        data-masked="true">Find the Perfect Wedding Dress</div>
        <a href="#"
        target="_self"
        class="ms-layer msp-cn-152-15 ms-btn ms-btn-box ms-btn-n msp-preset-btn-186"
        data-effect="t(true,n,n,666,n,n,n,n,n,n,n,n,n,n,n)"
        data-duration="2400"
        data-delay="600"
        data-ease="easeOutQuart"
        data-type="button"
        data-offset-x="0"
        data-offset-y="50"
        data-origin="bc"
        data-position="normal" >Make an Appointment</a>
      </div>
    </div>
    <!-- END MasterSlider Main -->
  </div>
  <!-- END MasterSlider -->
</div>


<script type="text/javascript">   
  var masterslider = new MasterSlider();

      // slider controls
      masterslider.control('timebar'    ,{ autohide:false, overVideo:true, align:'top', color:'#FFFFFF'  , width:4 });
      // slider setup
      masterslider.setup("masterslider", {
        width           : 1366,
        height          : 768,
        minHeight       : 0,
        space           : 0,
        start           : 1,
        grabCursor      : true,
        swipe           : true,
        mouse           : true,
        keyboard        : false,
        layout          : "fullscreen",
        wheel           : false,
        autoplay        : true,
        instantStartLayers:true,
        loop            : true,
        shuffle         : false,
        preload         : 0,
        heightLimit     : true,
        autoHeight      : false,
        smoothHeight    : true,
        endPause        : false,
        overPause       : true,
        fillMode        : "fill",
        centerControls  : true,
        startOnAppear   : false,
        layersMode      : "center",
        autofillTarget  : "",
        hideLayers      : false,
        fullscreenMargin: 80,
        speed           : 10,
        dir             : "h",
        parallaxMode    : 'swipe',
        view            : "parallaxMask"
      });
    </script>


    <script>window.jQuery || document.write('<script src="<?php echo $Servidor_url; ?>recursos/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src=".<?php echo $Servidor_url; ?>recursos/bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo $Servidor_url; ?>recursos/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo $Servidor_url; ?>recursos/bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>

  </body>
  </html>
