<?php
conectar2('shopifun', "compras");

//consultar en la base de datos
$query_rs_compra_segundo_paso = "SELECT compras_segundo_paso.compra_email, compras_segundo_paso.fecha_creacion, direcciones.direccion_first_name, direcciones.direccion_last_name, direcciones.direccion_address, direcciones.direccion_address_2, direcciones.direccion_city, direcciones.direccion_country, direcciones.direccion_state, direcciones.direccion_zip, direcciones.direccion_phone  FROM compras_segundo_paso, direcciones WHERE compras_segundo_paso.compra_codigo = '$codigo_compra' AND compras_segundo_paso.compra_direccion = direcciones.id_direccion ORDER BY compras_segundo_paso.id_compra DESC LIMIT 1";
$rs_compra_segundo_paso = mysql_query($query_rs_compra_segundo_paso)or die(mysql_error());
$row_rs_compra_segundo_paso = mysql_fetch_assoc($rs_compra_segundo_paso);
$totalrow_rs_compra_segundo_paso = mysql_num_rows($rs_compra_segundo_paso);

//consultar en la base de datos
$query_rs_compras = "SELECT id_compra, id_producto, cantidad FROM compras_primer_paso WHERE codigo_compra = '$codigo_compra' ORDER BY id_compra DESC";
$rs_compras = mysql_query($query_rs_compras)or die(mysql_error());
$row_rs_compras = mysql_fetch_assoc($rs_compras);
$totalrow_rs_compras = mysql_num_rows($rs_compras);

$lista_producto = NULL;
do {
	$id_compra = $row_rs_compras['id_compra'];
	$id_producto = $row_rs_compras['id_producto'];
	$cantidad = $row_rs_compras['cantidad'];

	if(!$lista_producto) {
		$lista_producto = 'WHERE id_producto = '.$id_producto;
	} else {
		$lista_producto .= ' OR id_producto = '.$id_producto;
	}

	$compra_lista[$id_compra] = $id_producto;
	$compra_lista_cantidad[$id_compra] = $cantidad;
} while ($row_rs_compras = mysql_fetch_assoc($rs_compras));

$fecha_actual = date('Y-m-d H:i:s');
	//consultar en la base de datos
$query_rs_consultar_codigo_descuento = "SELECT id_cupon, cupon_descuento FROM cupones_descuento WHERE cupon_codigo_compra = '$codigo_compra'";
$rs_consultar_codigo_descuento = mysql_query($query_rs_consultar_codigo_descuento)or die(mysql_error());
$row_rs_consultar_codigo_descuento = mysql_fetch_assoc($rs_consultar_codigo_descuento);
$totalrow_rs_consultar_codigo_descuento = mysql_num_rows($rs_consultar_codigo_descuento);

$precio_descuento_porcentaje = 0;

if($totalrow_rs_consultar_codigo_descuento) {
	$id_cupon_descuento = $row_rs_consultar_codigo_descuento['id_cupon'];
	$precio_descuento_porcentaje = $row_rs_consultar_codigo_descuento['cupon_descuento'];
}
desconectar();

if($lista_producto) {
	conectar2('shopifun', "admin");
//consultar en la base de datos
	$query_rs_productos = "SELECT id_producto, producto_titulo, producto_precio_dolar, producto_precio_shipping_dolar  FROM productos $WHERE";
	$rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
	$row_rs_productos = mysql_fetch_assoc($rs_productos);
	$totalrow_rs_productos = mysql_num_rows($rs_productos);

	desconectar();
}

do {
	$id_producto = $row_rs_productos['id_producto'];
	$producto_titulo = $row_rs_productos['producto_titulo'];
	$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
	$producto_precio_shipping_dolar = $row_rs_productos['producto_precio_shipping_dolar'];

	$array_producto[$id_producto] = $producto_titulo;
	$array_producto_precio[$id_producto] = $producto_precio_dolar;
	$array_producto_shipping[$id_producto] = $producto_precio_shipping_dolar;
} while ($row_rs_productos = mysql_fetch_assoc($rs_productos));

$precio_total = 0;
$shipping = 0;

$descuento = 0;

$compra_email = $row_rs_compra_segundo_paso['compra_email'];
$fecha_carga = $row_rs_compra_segundo_paso['fecha_creacion'];

$fecha_carga = nombre_fecha_ingles($fecha_carga);
$direccion_first_name = $row_rs_compra_segundo_paso['direccion_first_name'];
$direccion_last_name = $row_rs_compra_segundo_paso['direccion_last_name'];
$direccion_address = $row_rs_compra_segundo_paso['direccion_address'];
$direccion_address_2 = $row_rs_compra_segundo_paso['direccion_address_2'];
$direccion_city = $row_rs_compra_segundo_paso['direccion_city'];
$direccion_country = $row_rs_compra_segundo_paso['direccion_country'];
$direccion_state = $row_rs_compra_segundo_paso['direccion_state'];
$direccion_zip = $row_rs_compra_segundo_paso['direccion_zip'];
$direccion_phone = $row_rs_compra_segundo_paso['direccion_phone'];

if(!$email_a_mandar) {
	$email_a_mandar = $compra_email;
}
$first_name = $direccion_first_name;

$cuerpo = '
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thanks For Your Business!</title>
<style media="all" type="text/css">
@media only screen and (max-width: 620px) {
	.span-2,
	.span-3 {
		max-width: none !important;
		width: 100% !important;
	}
	.span-2 > table,
	.span-3 > table {
		max-width: 100% !important;
		width: 100% !important;
	}
}

@media all {
	.btn-primary table td:hover {
		background-color: #34495e !important;
	}
	.btn-primary a:hover {
		background-color: #34495e !important;
		border-color: #34495e !important;
	}
}

@media all {
	.btn-secondary a:hover {
		border-color: #34495e !important;
		color: #34495e !important;
	}
}

@media only screen and (max-width: 620px) {
	h1 {
		font-size: 28px !important;
		margin-bottom: 10px !important;
	}
	h2 {
		font-size: 22px !important;
		margin-bottom: 10px !important;
	}
	h3 {
		font-size: 16px !important;
		margin-bottom: 10px !important;
	}
	.main p,
	.main ul,
	.main ol,
	.main td,
	.main span {
		font-size: 16px !important;
	}
	.wrapper {
		padding: 10px !important;
	}
	.article {
		padding-left: 0 !important;
		padding-right: 0 !important;
	}
	.content {
		padding: 0 !important;
	}
	.container {
		padding: 0 !important;
		width: 100% !important;
	}
	.header {
		margin-bottom: 10px !important;
	}
	.main {
		border-left-width: 0 !important;
		border-radius: 0 !important;
		border-right-width: 0 !important;
	}
	.btn table {
		max-width: 100% !important;
		width: 100% !important;
	}
	.btn a {
		max-width: 100% !important;
		padding: 12px 5px !important;
		width: 100% !important;
	}
	.img-responsive {
		height: auto !important;
		max-width: 100% !important;
		width: auto !important;
	}
	.alert td {
		border-radius: 0 !important;
		padding: 10px !important;
	}
	.receipt {
		width: 100% !important;
	}
	hr {
		Margin-bottom: 10px !important;
		Margin-top: 10px !important;
	}
	.hr tr:first-of-type td,
	.hr tr:last-of-type td {
		height: 10px !important;
		line-height: 10px !important;
	}
}

@media all {
	.ExternalClass {
		width: 100%;
	}
	.ExternalClass,
	.ExternalClass p,
	.ExternalClass span,
	.ExternalClass font,
	.ExternalClass td,
	.ExternalClass div {
		line-height: 100%;
	}
	.apple-link a {
		color: inherit !important;
		font-family: inherit !important;
		font-size: inherit !important;
		font-weight: inherit !important;
		line-height: inherit !important;
		text-decoration: none !important;
	}
}
</style>

<!--[if gte mso 9]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->

</head>
<body style="font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
<td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; Margin: 0 auto !important; max-width: 580px; padding: 10px; width: 580px;" width="580" valign="top">
<div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

<!-- START CENTERED WHITE CONTAINER -->
<span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Thanks For Your Business!</span>

<!-- START HEADER -->
<div class="header" style="margin-bottom: 20px; Margin-top: 10px; width: 100%;">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
<tr>
<td class="align-center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center;" valign="top" align="center">
<a href="https://www.shopifunny.com" target="_blank" style="color: #3498db; text-decoration: underline;"><img src="https://www.shopifunny.com/img/Logo-Cuadrado.png" width="90" alt="Shopifunny" align="center" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;"></a>
</td>
</tr>
</table>
</div>

<!-- END HEADER -->
<table border="0" cellpadding="0" cellspacing="0" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;" width="100%">

<!-- START MAIN CONTENT AREA -->
<tr>
<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;" valign="top">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
<h1 style="color: #222222; font-family: sans-serif; font-weight: 300; line-height: 1.4; margin: 0; Margin-bottom: 30px; font-size: 35px; text-align: center; text-transform: capitalize;">Thanks for your business!</h1>
<h2 class="align-center" style="color: #222222; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 15px; font-size: 28px; text-align: center;">Your order</h2>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
<td class="receipt-container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 80%;" width="80%" valign="top">
<table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; Margin-bottom: 20px;" width="100%">
<tr class="receipt-subtle" style="color: #aaa;">
<td colspan="2" class="align-center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top" align="center">'.$fecha_carga.'<br>Purchase #'.$codigo_compra.'</td></tr>';



if($totalrow_rs_productos) { 
	$i=0;
	foreach ($compra_lista as $id_compra => $id_producto) {
		$producto_titulo = $array_producto[$id_producto];
		$producto_precio_dolar = $array_producto_precio[$id_producto];
		$producto_shipping = $array_producto_shipping[$id_producto];

		$cantidad = $compra_lista_cantidad[$id_compra]; 

		$producto_titulo = '<b>'.$cantidad.'x</b> '.$producto_titulo;

		$cuerpo .= '<tr><td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">'.$producto_titulo.'</td>
		<td class="receipt-figure" style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px; text-align: right;" valign="top" align="right">'.formato_moneda($producto_precio_dolar, '0').'</td>
		</tr>';

		$precio_total = $precio_total + ($producto_precio_dolar * $cantidad);
		$shipping = $shipping + ($producto_shipping * $cantidad);
	}



	$cuerpo .='<tr class="receipt-subtle" style="color: #aaa;">
	<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">Shipping</td>
	<td class="receipt-figure" style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px; text-align: right;" valign="top" align="right">'.formato_moneda($shipping, '0').'</td>
	</tr>';

	$cuerpo .='<tr class="receipt-subtle" style="color: #aaa;">
	<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">Subtotal</td>
	<td class="receipt-figure" style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px; text-align: right;" valign="top" align="right">'.formato_moneda($precio_total, '0').'</td>
	</tr>';


	if($precio_descuento_porcentaje) {
		$precio_final = $precio_total + $shipping;
		$descuento = ($precio_descuento_porcentaje * $precio_final) / 100;
	}
	if($descuento != 0) {
		$precio_total = $precio_total - $descuento;

		$cuerpo .='<tr class="receipt-subtle" style="color: #aaa;">
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">Discount</td>
		<td class="receipt-figure" style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px; text-align: right;" valign="top" align="right">-'.formato_moneda($descuento, '0').'</td>
		</tr>';
	}
}

$cuerpo .= '<tr class="receipt-bold">
<td style="font-family: sans-serif; vertical-align: top; margin: 0; padding: 5px; font-size: 18px; border-bottom: 2px solid #333; border-top: 2px solid #333; font-weight: 600;" valign="top">Total</td>
<td class="receipt-figure" style="font-family: sans-serif; vertical-align: top; margin: 0; padding: 5px; font-size: 18px; border-bottom: 2px solid #333; text-align: right; border-top: 2px solid #333; font-weight: 600;" valign="top" align="right">'.formato_moneda($precio_total + $shipping, '').'</td>';


$cuerpo .='</tr>
</table>
<h2 class="align-center" style="color: #222222; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 15px; font-size: 28px; text-align: center;">Your details</h2>
<table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; Margin-bottom: 20px;" width="100%">
<tr>';

$nombre_completo = $direccion_first_name.' '.$direccion_last_name;
$direccion_first_name = $row_rs_compra_segundo_paso['direccion_first_name'];
$direccion_last_name = $row_rs_compra_segundo_paso['direccion_last_name'];
$direccion_address = $row_rs_compra_segundo_paso['direccion_address'];
$direccion_address_2 = $row_rs_compra_segundo_paso['direccion_address_2'];
$direccion_city = $row_rs_compra_segundo_paso['direccion_city'];
$direccion_country = $row_rs_compra_segundo_paso['direccion_country'];
$direccion_state = $row_rs_compra_segundo_paso['direccion_state'];
$direccion_zip = $row_rs_compra_segundo_paso['direccion_zip'];
$direccion_phone = $row_rs_compra_segundo_paso['direccion_phone'];

$direccion_completa = trim($direccion_address.' '.$direccion_address_2);

if($direccion_state) {
	$estado_postal = $direccion_state;
}
$estado_postal = trim($estado_postal.' '.$direccion_zip);

$cuerpo .= '<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">Shipping to</td>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 5px;" valign="top">'.$nombre_completo.'
<br>'.$direccion_completa.
'<br>'.$direccion_city.
'<br>'.$estado_postal.'</td>
</tr>

</table>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; Margin: 0; Margin-bottom: 15px;">Notice something wrong? <a href="https://www.shopifunny.com/support/" target="_blank" style="color: #3498db; text-decoration: underline;">Contact our support team</a> and well be happy to help.</p>
</td>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>

<!-- START CALL OUT -->
<tr>
<td class="wrapper section-callout" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px; background-color: #ff9800; color: #ffffff;" valign="top" bgcolor="#ff9800">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; color: #ffffff;" valign="top">
<h2 class="align-center" style="font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 15px; font-size: 28px; text-align: center; color: #ffffff;">Tell your friends</h2>

<table class="social-sharing" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; Margin: 0 auto; text-align: center;" align="center">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; color: #ffffff;" valign="top">
<a href="https://twitter.com/home?status&#x3D;Check%20this%20out%20https%3A//www.shopifunny.com" style="color: #3498db; text-decoration: underline;"><img src="https://www.shopifunny.com/recursos/emails/disenos/img/twitter.png" alt="Share on Twitter" width="44" class="social-sharing-icon" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; height: 44px; Margin: 0 2px;"></a>
</td>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; color: #ffffff;" valign="top">
<a href="https://www.facebook.com/sharer/sharer.php?u&#x3D;https%3A//shopifunny.com" style="color: #3498db; text-decoration: underline;"><img src="https://www.shopifunny.com/recursos/emails/disenos/img/facebook.png" alt="Share on Facebook" width="44" class="social-sharing-icon" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; height: 44px; Margin: 0 2px;"></a>
</td>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; color: #ffffff;" valign="top">
<a href="https://www.instagram.com/shopifunny" style="color: #3498db; text-decoration: underline;"><img src="https://www.shopifunny.com/recursos/emails/disenos/img/instagram.png" alt="Share on Facebook" width="44" class="social-sharing-icon" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; height: 44px; Margin: 0 2px;"></a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>

<!-- END CALL OUT -->
<tr>
<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;" valign="top">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
<tr>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;" width="100%">
<tbody>
<tr>
<td align="center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">

</td>
</tr>
</tbody>
</table>
<p class="align-center" style="font-family: sans-serif; font-size: 14px; font-weight: normal; Margin: 0; Margin-bottom: 15px; text-align: center;">Thanks for being a great customer.</p>
</td>
</tr>
</table>
</td>
</tr>

<!-- END MAIN CONTENT AREA -->
</table>

<!-- START FOOTER -->
<div class="footer" style="clear: both; padding-top: 0px; text-align: center; width: 100%;">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
<tr>
<td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-top: 10px; padding-bottom: 10px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
<br> Dont like these emails? <a href="https://www.shopifunny.com/unsubscribe/'.$email_a_mandar.'/" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>.
</td>
</tr>
<tr>
<td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-top: 10px; padding-bottom: 10px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
Powered by <a href="https://www.shopifunny.com/about/" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">Shopifunny Team</a> :D
</td>
</tr>
</table>
</div>

<!-- END FOOTER -->

<!-- END CENTERED WHITE CONTAINER --></div>
</td>
<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
</tr>
</table>
</body>
</html>
';