<?php include('../../paginas_include/variables-generales.php'); 

$id_producto = $_GET['producto'];
$palabras = trim($_GET['palabras']);
if(!$palabras) {
  exit;
}
$limite = 6;
conectar2('shopifun', "admin");
//consultar en la base de datos
//consultar en la base de datos
$query_rs_reviews = "SELECT id_review, review_usuario, review_pais, review_ranking, review_mensaje, review_fecha  FROM productos_reviews WHERE id_producto = $id_producto ORDER BY id_review ASC ";
$rs_reviews = mysql_query($query_rs_reviews)or die(mysql_error());
$row_rs_reviews = mysql_fetch_assoc($rs_reviews);
$totalrow_rs_reviews = mysql_num_rows($rs_reviews);

//consultar en la base de datos
$query_rs_reviews_imagenes = "SELECT id_review_imagen, id_review, imagen_url FROM productos_reviews_imagenes WHERE id_producto = $id_producto ";
$rs_reviews_imagenes = mysql_query($query_rs_reviews_imagenes)or die(mysql_error());
$row_rs_reviews_imagenes = mysql_fetch_assoc($rs_reviews_imagenes);
$totalrow_rs_reviews_imagenes = mysql_num_rows($rs_reviews_imagenes);

do {
  $id_review_imagen = $row_rs_reviews_imagenes['id_review_imagen'];
  $id_review = $row_rs_reviews_imagenes['id_review'];
  $imagen_url = $row_rs_reviews_imagenes['imagen_url'];

  $imagenes_reviews[$id_review][$id_review_imagen] = $imagen_url;
} while ($row_rs_reviews_imagenes = mysql_fetch_assoc($rs_reviews_imagenes));
desconectar();
$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

$cantidad = 6;
?>
<style>
.review i{
  color: #FFC107;
}

.review_fecha {
  font-weight: bold;
  font-size: 14px;
  color: #757575;
}

.review_imagenes {
  margin-top: 10px;
  margin-bottom: 10px;
}
/* padding-bottom and top for image */
.mfp-no-margins img.mfp-img {
  padding: 0;
}
/* position of shadow behind the image */
.mfp-no-margins .mfp-figure:after {
  top: 0;
  bottom: 0;
}
/* padding for main container */
.mfp-no-margins .mfp-container {
  padding: 0;
}


    /* 

    for zoom animation 
    uncomment this part if you haven't added this code anywhere else

    */
    /*
    
    .mfp-with-zoom .mfp-container,
    .mfp-with-zoom.mfp-bg {
      opacity: 0;
      -webkit-backface-visibility: hidden;
      -webkit-transition: all 0.3s ease-out; 
      -moz-transition: all 0.3s ease-out; 
      -o-transition: all 0.3s ease-out; 
      transition: all 0.3s ease-out;
    }
    
    .mfp-with-zoom.mfp-ready .mfp-container {
        opacity: 1;
    }
    .mfp-with-zoom.mfp-ready.mfp-bg {
        opacity: 0.8;
    }
    
    .mfp-with-zoom.mfp-removing .mfp-container, 
    .mfp-with-zoom.mfp-removing.mfp-bg {
      opacity: 0;
    }
    */
  </style>
  <?php if($totalrow_rs_reviews) { ?>
  <?php do { 
    $id_review = $row_rs_reviews['id_review'];
    $review_usuario = $row_rs_reviews['review_usuario'];
    $review_pais = $row_rs_reviews['review_pais'];
    $review_ranking = $row_rs_reviews['review_ranking'];
    $review_mensaje = $row_rs_reviews['review_mensaje'];
    $review_fecha = $row_rs_reviews['review_fecha'];

    if(!$review_usuario) {
      $review_usuario = 'Shopifunny User';
    } else {
      $review_usuario = arreglar_datos_db_al_reves($review_usuario);
    }

    $review_mensaje = arreglar_datos_db_al_reves($review_mensaje);


    ?>
    <div class="review">
      <div class="review_nombre_usuario"><?php echo $review_usuario; ?> <b>(<?php echo $review_pais; ?>)</b></div>
      <div class="review_estrellas">
        <i class="fa fa-star"></i>
        <i class="fa fa-star"></i>
        <i class="fa fa-star"></i>
        <i class="fa fa-star"></i>
        <i class="fa fa-star"></i> 
      </div>
      <div class="review_mensaje"><?php echo $review_mensaje; ?></div>
      <?php  if($imagenes_reviews[$id_review]) { ?>
      <div class="review_imagenes">
        <div class="popup-gallery<?php echo $id_review; ?>">
          <?php foreach ($imagenes_reviews[$id_review] as $id_review_imagen => $imagen_url) { ?>
          <a href="<?php echo $imagen_url; ?>" title="<?php echo $review_usuario; ?>">
            <img width="50" src="<?php echo $imagen_url; ?>" alt="">
          </a>
          <?php } ?>
        </div>
      </div>
      <script type="text/javascript">
        $(document).ready(function() {
          $('.popup-gallery<?php echo $id_review; ?>').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
              enabled: true,
              navigateByImgClick: true          },
          image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
              return item.el.attr('title');
            }
          }
        });
        });
      </script>
      <?php } ?>
      <div class="review_fecha"><?php echo $review_fecha; ?></div>
    </div>
    <?php } while($row_rs_reviews = mysql_fetch_assoc($rs_reviews)); ?>
    <?php } ?>