<?php 
session_start();

$producto_i = $_SESSION['producto_i'];
$panelpromo = $_SESSION['panelpromo'];

?>
<div class="encabezado_productos fondo<?php echo $panel_fondo; ?> panel_diseno<?php echo $panel_diseno; ?>">
	<h1><?php echo $panel_nombre; ?></h1>
	<?php if ($cuenta_regresiva) { ?>
	<h2 class="ul_cuenta_regresiva">
		<ul class="countdown<?php echo $panelpromo; ?>">
			<li>Ends in:</li>
			<li> <span class="days">00</span> days,
			</li>
			<li class="seperator"></li>
			<li> <span class="hours">00</span>h
			</li>
			<li class="seperator">:</li>
			<li> <span class="minutes">00</span>m
			</li>
			<li class="seperator">:</li>
			<li> <span class="seconds">00</span>s
			</li>
		</ul>
	</h2>
	<script class="source" type="text/javascript">
		$('.countdown<?php echo $panelpromo; ?>').downCount({
			date: '<?php echo $cuenta_regresiva_fecha; ?>',
			offset: +10
		}, function () {
			//alert('WOOT WOOT, done!');
		});
	</script> 
	<?php } ?>
	<a href="<?php echo $Servidor_url; ?>sec1231<?php echo $panel_seccion; ?>122312/">
		<h3>View More</h3>
	</a>
	<div class="clear"></div>
	<div class="nuevo_contenedor_productos">
		<div class="panelproductos">
			<?php 
			do { 
				$id_producto = $row_rs_productos['id_producto'];
				$producto_titulo = $row_rs_productos['producto_titulo'];
				$producto_esquinero = $row_rs_productos['producto_esquinero'];
				$producto_categoria = $row_rs_productos['producto_categoria'];
				$producto_seccion = $row_rs_productos['producto_seccion'];
				$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
				$producto_precio_dolar_comparado = $row_rs_productos['producto_precio_dolar_comparado'];

				$foto_portada = $row_rs_productos['foto_portada'];

				$precio = formato_moneda($producto_precio_dolar, 'dolar');
				$precio_comparado = formato_moneda($producto_precio_dolar_comparado, 'dolar');
				$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

				$recorte_foto_miniatura = $array_fotos[$foto_portada];
				?>
				<div class="panelproductosolo cd-add-to-cart" data-price="<?php echo $producto_precio_dolar; ?>">
					<a href="#" onmouseover="mostrar_producto(<?php echo $producto_i; ?>)" onmouseout="ocultar_producto(<?php echo $producto_i; ?>)" >
						<div class="producto_nombre" id="producto_nombre_<?php echo $producto_i; ?>">
							<?php echo $producto_titulo; ?>
						</div>

						<div class="producto_solo_img" style="background:url('<?php echo $ruta_imagenes.$recorte_foto_miniatura; ?>') no-repeat center center; background-size: 100% 100%;">
							<?php if($producto_esquinero) { ?>
							<img src="<?php echo $Servidor_url;?>img/esquineros/20.png" class="producto_esquinero" alt="">
							<?php } ?>
						</div>

						<div class="producto_contenedor_precio">
							<p class="producto_precio"><?php echo $precio;?></p>
							<p class="producto_precio_anterior"><?php echo $precio_comparado;?></p>
						</div>
					</a>
				</div>
				<?php $producto_i++; } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<?php 		
	$panelpromo++;
	$_SESSION['panelpromo'] = $panelpromo; 
	$_SESSION['producto_i'] = $producto_i; ?>