<?php 
session_start();

$producto_i = $_SESSION['producto_i'];
$panelpromo = $_SESSION['panelpromo'];

?>
<div class="encabezado_productos fondo<?php echo $panel_fondo; ?>" style="padding-right: 0px !important; margin-right: 0px !important">
	<h1><?php echo $panel_nombre; ?></h1>
	<?php if ($cuenta_regresiva) { ?>
	<h2 class="ul_cuenta_regresiva">
		<ul class="countdown<?php echo $panelpromo; ?>">
			<li>Ends in:</li>
			<li> <span class="days">00</span> days,
			</li>
			<li class="seperator"></li>
			<li> <span class="hours">00</span>h
			</li>
			<li class="seperator">:</li>
			<li> <span class="minutes">00</span>m
			</li>
			<li class="seperator">:</li>
			<li> <span class="seconds">00</span>s
			</li>
		</ul>
	</h2>
	<?php } ?>
	<a href="<?php echo $Servidor_url; ?>sec1231<?php echo $panel_seccion; ?>122312/">
		<h3>View More</h3>
	</a>
	<div class="clear"></div>
	<div class="nuevo_contenedor_productos">
		<div class="panelproductos">
			<ul class="cd-gallery cd-gallery<?php echo $panelpromo; ?>">
				<section class="regular<?php echo $panelpromo; ?> slider">

					<?php 
					do { 
						$id_producto = $row_rs_productos['id_producto'];
						$producto_titulo = $row_rs_productos['producto_titulo'];
						$producto_esquinero = $row_rs_productos['producto_esquinero'];
						$producto_categoria = $row_rs_productos['producto_categoria'];
						$producto_seccion = $row_rs_productos['producto_seccion'];
						$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
						$producto_precio_dolar_comparado = $row_rs_productos['producto_precio_dolar_comparado'];

						$foto_portada = $row_rs_productos['foto_portada'];

						$precio = formato_moneda($producto_precio_dolar, 'dolar');
						$precio_comparado = formato_moneda($producto_precio_dolar_comparado, 'dolar');
						$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

						$recorte_foto_miniatura = $array_fotos[$foto_portada];

						//consultar en la base de datos
						$query_rs_fotos_producto = "SELECT recorte_foto_miniatura, color, id_foto FROM fotos_publicaciones WHERE id_publicacion = $id_producto ORDER BY orden ASC";
						$rs_fotos_producto = mysql_query($query_rs_fotos_producto)or die(mysql_error());
						$row_rs_fotos_producto = mysql_fetch_assoc($rs_fotos_producto);
						$totalrow_rs_fotos_producto = mysql_num_rows($rs_fotos_producto);
						
						$i=0;

						unset($array_colores);
						unset($array_fotos_colores);

						$array_colores = '';
						$array_fotos_colores = '';

						$limite_colores = 3;
						do {
							$recorte_foto_miniatura = $row_rs_fotos_producto['recorte_foto_miniatura'];
							$color = $row_rs_fotos_producto['color'];
							$id_foto = $row_rs_fotos_producto['id_foto'];

							$array_fotos_producto[$id_foto] = $recorte_foto_miniatura;

							if($color) {
								$array_colores[$i] = $color;
								$array_fotos_colores[$i] = $recorte_foto_miniatura;
							}
							
							if($limite_colores==$i) {
								break;
							}
							$i++;
						} while ($row_rs_fotos_producto = mysql_fetch_assoc($rs_fotos_producto));

						?>
						<li class="contenedor_producto_slider">
							<div class="cd-single-item cd-single-item<?php echo $panelpromo; ?>">
								<a href="#0">
									<ul class="cd-slider-wrapper cd-slider-wrapper<?php echo $panelpromo; ?>">
										<?php $i=0; foreach ($array_fotos_colores as $foto) { 
											$selected = null;
											if($i==0) {
												$selected='class="selected"';
											}
											?>
											<li <?php echo $selected; ?>><img src="<?php echo $ruta_imagenes.$foto; ?>" alt="Preview image"></li>
											<?php $i++;} ?>

										</ul>
										<div class="span_precio"><?php echo $precio; ?><span><?php echo $precio_comparado; ?></span></div>
									</a>
									<div class="seleccion_color_talle cd-customization cd-customization<?php echo $panelpromo; ?>">
										<div class="color selected-2 seleccion_color_talle_selected" data-type="select">
											<ul>
												<?php $i=0;foreach ($array_colores as $color) {
													$selected = null;
													if($i==0) {
														$selected='active';
													}
													?>
													<li class="color-<?php echo $color; ?> <?php echo $selected; ?>">color-<?php echo $color; ?></li>
													<?php $i++;} ?>
												</ul>
											</div>

											<div class="size seleccion_color_talle_selected" data-type="select">
												<ul>
													<li class="small active">Small</li>
													<li class="medium">Medium</li>
													<li class="large">Large</li>
												</ul>
											</div>

											<button class="add-to-cart">
												<em><i class="fa fa-cart-plus"></i></em>
												<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
													<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
												</svg>
											</button>
								</div> <!-- .cd-customization 
							-->

							<h3 class="producto_titulo"><?php echo $producto_titulo; ?></h3>

							<span class="panel_producto_shipping"><i class="fa fa-truck"></i> Free Shipping!</span>

							<button class="cd-customization-trigger">Customize</button>
						</div> <!-- .cd-single-item -->
					</li>
					<?php $producto_i++; } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>
				</section>
			</ul> <!-- cd-gallery -->
		</div>
	</div>
	<script>
		var productCustomization<?php echo $panelpromo; ?> = $('.cd-customization<?php echo $panelpromo; ?>'),
		cart = $('.cd-cart'),
		animating = false;

		initCustomization(productCustomization<?php echo $panelpromo; ?>);

		$('body').on('click', function(event){
		//if user clicks outside the .cd-gallery list items - remove the .hover class and close the open ul.size/ul.color list elements
		if( $(event.target).is('body') || $(event.target).is('.cd-gallery<?php echo $panelpromo; ?>') ) {
			deactivateCustomization();
		}
	});

		function initCustomization(items) {
			items.each(function(){
				var actual = $(this),
				selectOptions = actual.find('[data-type="select"]'),
				addToCartBtn = actual.find('.add-to-cart'),
				touchSettings = actual.next('.cd-customization-trigger');

			//detect click on ul.size/ul.color list elements 
			selectOptions.on('click', function(event) { 
				var selected = $(this);
				//open/close options list
				selected.toggleClass('is-open');
				resetCustomization(selected);
				
				if($(event.target).is('li')) {
					// update selected option
					var activeItem = $(event.target),
					index = activeItem.index() + 1;
					
					activeItem.addClass('active').siblings().removeClass('active');
					selected.removeClass('selected-1 selected-2 selected-3').addClass('selected-'+index);
					// if color has been changed, update the visible product image 
					selected.hasClass('color') && updateSlider<?php echo $panelpromo; ?>(selected, index-1);
				}
			});

			//detect click on the add-to-cart button
			addToCartBtn.on('click', function() {	
				if(!animating) {
					//animate if not already animating
					animating =  true;
					resetCustomization(addToCartBtn);

					addToCartBtn.addClass('is-added').find('path').eq(0).animate({
						//draw the check icon
						'stroke-dashoffset':0
					}, 300, function(){
						setTimeout(function(){
							updateCart();
							addToCartBtn.removeClass('is-added').find('em').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
								//wait for the end of the transition to reset the check icon
								addToCartBtn.find('path').eq(0).css('stroke-dashoffset', '19.79');
								animating =  false;
							});

							if( $('.no-csstransitions').length > 0 ) {
								// check if browser doesn't support css transitions
								addToCartBtn.find('path').eq(0).css('stroke-dashoffset', '19.79');
								animating =  false;
							}
						}, 600);
					});	
				}
			});

			//detect click on the settings icon - touch devices only
			touchSettings.on('click', function(event){
				event.preventDefault();
				resetCustomization(addToCartBtn);
			});
		});
		}

		function updateSlider<?php echo $panelpromo; ?>(actual, index) {
			var slider = actual.parent('.cd-customization<?php echo $panelpromo; ?>').prev('a').children('.cd-slider-wrapper<?php echo $panelpromo; ?>'),
			slides = slider.children('li');

			slides.eq(index).removeClass('move-left').addClass('selected').prevAll().removeClass('selected').addClass('move-left').end().nextAll().removeClass('selected move-left');
		}

		function resetCustomization(selectOptions) {
		//close ul.clor/ul.size if they were left open and user is not interacting with them anymore
		//remove the .hover class from items if user is interacting with a different one
		selectOptions.siblings('[data-type="select"]').removeClass('is-open').end().parents('.cd-single-item<?php echo $panelpromo; ?>').addClass('hover').parent('li').siblings('li').find('.cd-single-item<?php echo $panelpromo; ?>').removeClass('hover').end().find('[data-type="select"]').removeClass('is-open');
	}

	function deactivateCustomization() {
		productCustomization.parent('.cd-single-item<?php echo $panelpromo; ?>').removeClass('hover').end().find('[data-type="select"]').removeClass('is-open');
	}

	function updateCart() {
		//show counter if this is the first item added to the cart
		( !cart.hasClass('items-added') ) && cart.addClass('items-added'); 

		var cartItems = cart.find('span'),
		text = parseInt(cartItems.text()) + 1;
		cartItems.text(text);
	}

	$(".regular<?php echo $panelpromo; ?>").slick({
		dots: false,
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
});
</script>
<?php 		
$panelpromo++;
$_SESSION['panelpromo'] = $panelpromo; 
$_SESSION['producto_i'] = $producto_i; ?> 