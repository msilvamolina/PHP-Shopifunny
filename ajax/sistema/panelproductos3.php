<?php 
session_start();

$producto_i = $_SESSION['producto_i'];
$panelpromo = $_SESSION['panelpromo'];

?>
<div class="encabezado_productos fondo<?php echo $panel_fondo; ?>" style="padding-right: 0px !important; margin-right: 0px !important">
	<h1><?php echo $panel_nombre; ?></h1>
	<?php if ($cuenta_regresiva) { ?>
	<h2 class="ul_cuenta_regresiva">
		<ul class="countdown<?php echo $panelpromo; ?>">
			<li>Ends in:</li>
			<li> <span class="days">00</span> days,
			</li>
			<li class="seperator"></li>
			<li> <span class="hours">00</span>h
			</li>
			<li class="seperator">:</li>
			<li> <span class="minutes">00</span>m
			</li>
			<li class="seperator">:</li>
			<li> <span class="seconds">00</span>s
			</li>
		</ul>
	</h2>
	<script class="source" type="text/javascript">
		$('.countdown<?php echo $panelpromo; ?>').downCount({
			date: '<?php echo $cuenta_regresiva_fecha; ?>',
			offset: +10
		}, function () {
			//alert('WOOT WOOT, done!');
		});

	</script> 
	<?php } ?>
	<a href="<?php echo $Servidor_url; ?>sec1231<?php echo $panel_seccion; ?>122312/">
		<h3>View More</h3>
	</a>
	<div class="clear"></div>
	<div class="nuevo_contenedor_productos">
		<div class="panelproductos">
			<ul class="cd-gallery">

				<section class="regular<?php echo $panelpromo; ?> slider">

					<?php 
					do { 
						$id_producto = $row_rs_productos['id_producto'];
						$producto_titulo = $row_rs_productos['producto_titulo'];
						$producto_esquinero = $row_rs_productos['producto_esquinero'];
						$producto_categoria = $row_rs_productos['producto_categoria'];
						$producto_seccion = $row_rs_productos['producto_seccion'];
						$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
						$producto_precio_dolar_comparado = $row_rs_productos['producto_precio_dolar_comparado'];

						$foto_portada = $row_rs_productos['foto_portada'];

						$precio = formato_moneda($producto_precio_dolar, 'dolar');
						$precio_comparado = formato_moneda($producto_precio_dolar_comparado, 'dolar');
						$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

						$recorte_foto_miniatura = $array_fotos[$foto_portada];
						?>
						<div class="contenedor_producto_slider">
							<li>
								<div class="cd-single-item">
									<a href="#0">
										<ul class="cd-slider-wrapper">
											<li><img src="img/thumb-11.jpg" alt="Preview image"></li>
											<li class="selected"><img src="img/thumb-22.jpg" alt="Preview image"></li>
											<li><img src="img/thumb-33.jpg" alt="Preview image"></li>
										</ul>
									</a>

									<div class="cd-customization">
										<div class="color selected-2" data-type="select">
											<ul>
												<li class="color-1">color-1</li>
												<li class="color-2 active">color-2</li>
												<li class="color-3">color-3</li>
											</ul>
										</div>

										<div class="size" data-type="select">
											<ul>
												<li class="small active">Small</li>
												<li class="medium">Medium</li>
												<li class="large">Large</li>
											</ul>
										</div>

										<button class="add-to-cart">
											<em>Add to Cart</em>
											<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
												<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
											</svg>
										</button>
									</div> <!-- .cd-customization -->

									<button class="cd-customization-trigger">Customize</button>
								</div> <!-- .cd-single-item -->

								<div class="cd-item-info">
									<b><a href="#0">Product Name</a></b>
									<em>$9.99</em>
								</div> <!-- cd-item-info -->
							</li>
						</div>
						<?php $producto_i++; } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>
					</section>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<script class="source" type="text/javascript">
		$(".regular<?php echo $panelpromo; ?>").slick({
			dots: false,
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 5,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
});
</script>
<?php 		
$panelpromo++;
$_SESSION['panelpromo'] = $panelpromo; 
$_SESSION['producto_i'] = $producto_i; ?> 