<?php 
session_start();

$producto_i = $_SESSION['producto_i'];
$panelpromo = $_SESSION['panelpromo'];

$fecha_actual = date('Y-m-d H:i:s');

if(strtotime($fecha_actual)>strtotime($cuenta_regresiva_fecha)) {
	$cuenta_regresiva = null;
}
?>
<div class="encabezado_productos fondo<?php echo $panel_fondo; ?> panel_diseno<?php echo $panel_diseno; ?>" style="padding-right: 0px !important; margin-right: 0px !important">

	<div class="panel_titulo"><?php echo $panel_titulo; ?>
		<div class="boton_more"><a href="#">
			<span class="panel_producto_more_icon">
				<i class="fa fa-ellipsis-v"></i>
			</span>
		</a></div>
	</div>
	<?php if ($cuenta_regresiva) { ?>
	<h2 class="ul_cuenta_regresiva">
		<ul id="countdown<?php echo $panelpromo; ?>">
			<li>Ends in:</li>
			<li> <span id="days<?php echo $panelpromo; ?>">00</span> days,
			</li>
			<li class="seperator"></li>
			<li> <span id="hours<?php echo $panelpromo; ?>">00</span>h
			</li>
			<li class="seperator">:</li>
			<li> <span id="minutes<?php echo $panelpromo; ?>">00</span>m
			</li>
			<li class="seperator">:</li>
			<li> <span id="seconds<?php echo $panelpromo; ?>">00</span>s
			</li>
		</ul>
	</h2>
	<script class="source" type="text/javascript">

		$('#countdown<?php echo $panelpromo; ?>').countdown('<?php echo $cuenta_regresiva_fecha; ?>', function(event) {
			$('#days<?php echo $panelpromo; ?>').html(event.strftime('%D'));
			$('#hours<?php echo $panelpromo; ?>').html(event.strftime('%H'));
			$('#minutes<?php echo $panelpromo; ?>').html(event.strftime('%M'));
			$('#seconds<?php echo $panelpromo; ?>').html(event.strftime('%S'));
		});
	</script> 
	<?php } ?>
	<div class="boton_more_txt">
		<a href="<?php echo $Servidor_url; ?>sec1231<?php echo $panel_seccion; ?>122312/">View More</a>
	</div>
	<div class="clear"></div>

	<div class="nuevo_contenedor_productos">
		<div class="panelproductos">
			<section class="regular<?php echo $panelpromo; ?> slider">
				<?php 
				do { 
					$id_producto = $row_rs_productos['id_producto'];
					$producto_titulo = $row_rs_productos['producto_titulo'];
					$producto_esquinero = $row_rs_productos['producto_esquinero'];
					$producto_categoria = $row_rs_productos['producto_categoria'];
					$producto_seccion = $row_rs_productos['producto_seccion'];
					$producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
					$producto_precio_dolar_comparado = $row_rs_productos['producto_precio_dolar_comparado'];

					$producto_url = $row_rs_productos['producto_url'];

					$foto_portada = $row_rs_productos['foto_portada'];
					$producto_precio_shipping_dolar = $row_rs_productos['producto_precio_shipping_dolar'];


					$precio_shipping = 'Free';

					if($producto_precio_shipping_dolar) {
						$precio_shipping = formato_moneda($producto_precio_shipping_dolar, '');
					}


					$precio = formato_moneda($producto_precio_dolar, 'dolar');
					$precio_comparado = formato_moneda($producto_precio_dolar_comparado, '');
					$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

					$recorte_foto_miniatura = $array_fotos[$foto_portada];
					
					$i=0;

					unset($array_colores);
					unset($array_fotos_colores);

					$array_colores = '';
					$array_fotos_colores = '';

					$limite_colores = 3;

					$link_producto = $Servidor_url.'p/'.$producto_url.'/';

					if(!$separar_productos) {
						$productos_agregados[] = $id_producto;
					}

					$array_recorte_foto_miniatura[$id_producto] = $recorte_foto_miniatura;
					$array_producto_titulo[$id_producto] = $producto_titulo;
					$array_link_producto[$id_producto] = $link_producto;
					$array_precio[$id_producto] = $precio;
					$array_precio_comparado[$id_producto] = $precio_comparado;
					$array_precio_shipping[$id_producto] = $precio_shipping;

				} while($row_rs_productos = mysql_fetch_assoc($rs_productos)); 


				if(!$productos_agregados) {
					$productos_agregados = $separar_productos;
				}	



				foreach ($productos_agregados as $id_producto) {

					$recorte_foto_miniatura = $array_recorte_foto_miniatura[$id_producto];
					$producto_titulo = $array_producto_titulo[$id_producto];
					$link_producto = $array_link_producto[$id_producto];
					$precio =  $array_precio[$id_producto];
					$precio_comparado =  $array_precio_comparado[$id_producto];
					$precio_shipping = $array_precio_shipping[$id_producto];
					?>

					<div class="contenedor_producto_slider fondo<?php echo $diseno_producto_fondo; ?>">
						<div class="contenedor_imagen">
							<div class="contenedor_botones">
								<a onclick="add_to_cart(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
									<div class="boton_add_to_cart" id="add_to_cart_producto_<?php echo $producto_i; ?>">
										<i class="fa fa-cart-plus"></i>
									</div>
								</a>
								<a onclick="add_to_wish_list(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
									<div class="boton_add_to_wish_list" id="add_to_wish_list_producto_<?php echo $producto_i; ?>">
										<i class="fa fa-heart-o"></i>
									</div>
								</a>
							</div>
							<a href="<?php echo $link_producto; ?>">
								<img class="producto_imagen" src="<?php echo $ruta_imagenes.$recorte_foto_miniatura; ?>" alt="<?php echo $producto_titulo; ?>">
							</a>
						</div>
						<a href="<?php echo $link_producto; ?>">

							<div class="span_precio"><?php echo $precio; ?><span><?php echo $precio_comparado; ?></span></div>
							<h3 class="producto_titulo"><?php echo $producto_titulo; ?></h3>

							<div class="clear"></div>
							<div class="div_contenedora_boton">
								<span class="panel_producto_shipping"><i class="fa fa-truck"></i> <?php echo $precio_shipping; ?> Shipping!</span>
							</div>
						</a>

					</div> <!-- .cd-single-item -->
					<?php $producto_i++; } ?>
				</section>
			</div>
		</div>
		<script>

			$(".regular<?php echo $panelpromo; ?>").slick({
				dots: false,
				infinite: true,
				slidesToShow: 5,
				slidesToScroll: 5,
				responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},
				{
					breakpoint: 920,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 680,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				}
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
});
</script>
<?php 		
$panelpromo++;
$_SESSION['panelpromo'] = $panelpromo; 
$_SESSION['producto_i'] = $producto_i; ?> 