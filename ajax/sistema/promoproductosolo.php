<?php 
$producto_titulo = $array_productos[$id_producto];

$producto_precio_dolar = $array_productos_precio[$id_producto];

$foto_portada = $array_productos_foto[$id_producto];

$precio = formato_moneda($producto_precio_dolar, 'dolar');
$precio_comparado = formato_moneda($producto_precio_dolar_comparado, 'dolar');
$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

$recorte_foto_miniatura = $array_fotos[$foto_portada];
$i=0;

unset($array_colores);
unset($array_fotos_colores);

$array_colores = '';
$array_fotos_colores = '';

$limite_colores = 3;

?>
<div class="contenedor_producto_slider fondo<?php echo $diseno_producto_fondo; ?>">

	<div class="contenedor_imagen">
		<div class="contenedor_botones">
			<a onclick="add_to_cart(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
				<div class="boton_add_to_cart" id="add_to_cart_producto_<?php echo $producto_i; ?>">
					<i class="fa fa-cart-plus"></i>
				</div>
			</a>
			<a onclick="add_to_wish_list(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
				<div class="boton_add_to_wish_list" id="add_to_wish_list_producto_<?php echo $producto_i; ?>">
					<i class="fa fa-heart-o"></i>
				</div>
			</a>
		</div>
		<img class="producto_imagen" src="<?php echo $ruta_imagenes.$recorte_foto_miniatura; ?>" alt="<?php echo $producto_titulo; ?>">
	</div>
	<div class="span_precio"><?php echo $precio; ?></div>
	<div class="clear"></div>
</div> <!-- .cd-single-item -->

<?php $producto_i++; ?>