<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

$nombre = trim($_GET['nombre']);

if(!$nombre) {
	redireccionar_404();
}

conectar2('shopifun', "admin");
//consultar en la base de datos
$query_rs_producto = "SELECT id_producto, producto_url, producto_titulo, foto_portada, producto_modelo, producto_talles, producto_stock, producto_precio_shipping_dolar, producto_colores, producto_precio_dolar  FROM productos WHERE producto_url = '$nombre' OR id_producto = '$nombre' LIMIT 1";
$rs_producto = mysql_query($query_rs_producto)or die(mysql_error());
$row_rs_producto = mysql_fetch_assoc($rs_producto);
$totalrow_rs_producto = mysql_num_rows($rs_producto);

if(!$totalrow_rs_producto) {
	redireccionar_404();
}

$url_actual = $Servidor_url2.$_SERVER['REQUEST_URI'];

$producto = $row_rs_producto['id_producto'];
$producto_url = $row_rs_producto['producto_url'];
$producto_titulo = $row_rs_producto['producto_titulo'];
$foto_portada = $row_rs_producto['foto_portada'];
$producto_modelo = $row_rs_producto['producto_modelo'];
$producto_talles = $row_rs_producto['producto_talles'];
$producto_stock = $row_rs_producto['producto_stock'];
$producto_colores = $row_rs_producto['producto_colores'];

$producto_precio_dolar = $row_rs_producto['producto_precio_dolar'];
$producto_precio_shipping_dolar = $row_rs_producto['producto_precio_shipping_dolar'];


$producto_precio_total = $producto_precio_dolar + $producto_precio_shipping_dolar;
$url_ideal = $Servidor_url.'p/'.$producto_url.'/';

if($url_actual!=$url_ideal) {
	header('location: '.$url_ideal);
	exit; 
}

if($producto_colores) {
	$lista_colores = explode(',', $producto_colores);
	$lista_colores = array_unique($lista_colores);
}

if($producto_modelo) {
	$lista_modelos = explode(',', $producto_modelo);

	//consultar en la base de datos
	$query_rs_modelos = "SELECT id_modelo, modelo_nombre FROM modelos ";
	$rs_modelos = mysql_query($query_rs_modelos)or die(mysql_error());
	$row_rs_modelos = mysql_fetch_assoc($rs_modelos);
	$totalrow_rs_modelos = mysql_num_rows($rs_modelos);

	do {
		$id_modelo = $row_rs_modelos['id_modelo'];
		$modelo_nombre = $row_rs_modelos['modelo_nombre'];

		$modelos_nombres[$id_modelo] = $modelo_nombre;
	} while($row_rs_modelos = mysql_fetch_assoc($rs_modelos));
}

if($producto_talles) {
	$lista_talles = explode(',', $producto_talles);

//consultar en la base de datos
	$query_rs_talles = "SELECT id_talle, talle FROM talles";
	$rs_talles = mysql_query($query_rs_talles)or die(mysql_error());
	$row_rs_talles = mysql_fetch_assoc($rs_talles);
	$totalrow_rs_talles = mysql_num_rows($rs_talles);

	do {
		$id_talle = $row_rs_talles['id_talle'];
		$talle = $row_rs_talles['talle'];

		$talles_nombres[$id_talle] = $talle;
	} while($row_rs_talles = mysql_fetch_assoc($rs_talles));
}



//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, cuerpo, color, modelo, recorte_foto_nombre, recorte_foto_miniatura, nombre_foto, fecha_carga FROM fotos_publicaciones WHERE id_publicacion = $producto ORDER BY orden ASC";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];
	$recorte_foto_miniatura = $row_rs_imagenes['recorte_foto_miniatura'];
	$cuerpo =  $row_rs_imagenes['cuerpo'];

	$nombre_foto = $row_rs_imagenes['nombre_foto'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
	$array_imagenes_miniatura[$id_foto] = $recorte_foto_miniatura;

	$array_foto[$id_foto] =  $row_rs_imagenes['nombre_foto'];
	$array_foto_cuerpo[$id_foto] =  $row_rs_imagenes['cuerpo'];

	$color = $row_rs_imagenes['color'];
	$modelo = $row_rs_imagenes['modelo'];

	if($color>0) {
		$array_foto_color[$id_foto] = $color;
		$array_fotos_mostrar_color[$id_foto] = $recorte_foto_nombre;
		$array_foto_tiene_color[$color] = $id_foto;
	} else {
		if($cuerpo==0) {
			if($modelo==0) {
				$array_fotos_mostrar[$id_foto] = $recorte_foto_nombre;
			}
		}
	}

	$array_fecha_carga[$id_foto] = $row_rs_imagenes['fecha_carga'];
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));

if(!$array_fotos_mostrar) {
	if($array_fotos_mostrar_color) {
		$array_fotos_mostrar = $array_fotos_mostrar_color;
	}
}
$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/';

$ruta_imagenes_recortes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';
$imagen_mostrar = $ruta_imagenes.'recortes/'.$array_recorte_foto_miniatura[$foto_portada];


//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, cuerpo_tipo, contenido FROM productos_cuerpo WHERE id_producto = $producto ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

do {
	$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
	$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
	$contenido = $row_rs_cuerpo['contenido'];
	$orden = $row_rs_cuerpo['orden'];

	$array_cuerpo[$id_cuerpo] = $contenido;
	$array_cuerpo_tipo[$id_cuerpo] = $cuerpo_tipo;
	$array_cuerpo_orden[$id_cuerpo] = $orden;

	if($cuerpo_tipo=="imagen") {
		$imagen_cuerpo[$contenido] = 1;
	}
} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));

//si llego hasta aca, es porque está todo bien
//consultar en la base de datos
$query_rs_colores = "SELECT id_color, color, color_codigo  FROM colores ";
$rs_colores = mysql_query($query_rs_colores)or die(mysql_error());
$row_rs_colores = mysql_fetch_assoc($rs_colores);
$totalrow_rs_colores = mysql_num_rows($rs_colores);

do {
	$id_color = $row_rs_colores['id_color'];
	$color = $row_rs_colores['color'];
	$color_codigo = $row_rs_colores['color_codigo'];

	$colores_nombres[$id_color] = $color;
	$colores_codigos[$id_color] = $color_codigo;

} while ($row_rs_colores = mysql_fetch_assoc($rs_colores));

desconectar();

$cuenta_regresiva_fecha = '2018-01-01 00:00:00';
$titulo_pagina = "Shopifunny   &raquo; ".$producto_titulo;

$producto_stock = 9;
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php
	//Permisos
	$agregar_slick = 1;
	$agregar_xZoom = 1;
	$agregar_fancybox = 1;?>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="<?php echo $Servidor_url;?>js/sticky-kit.js"></script>
<script src="<?php echo $Servidor_url;?>js/imagenSticky.js"></script>
</head>
<style>
body {
	margin: 0;
	font-family: sans-serif;
	font-size: 16px; }

	h1 {
		font-size: 30px;
		margin: 10px; }

		.content {
			overflow: hidden; }
			.content.right .sidebar {
				float: right;
				margin: 10px;
				margin-left: 0; }
				.content.right .main {
					margin: 10px;
					margin-right: 220px; }
					.content.double .main {
						margin-left: 434px; }
						.content .sidebar {
							width: 200px;
							height: 66px;
							margin: 10px;
							margin-right: 0;
							border: 1px solid red;
							float: left;
							overflow: hidden;
							font-family: sans-serif; }
							.content .sidebar.alt {
								height: 133px; }
								.content .sidebar.tall {
									height: 400px; }
									.content .sidebar.medium {
										height: 300px; }
										.content .sidebar.flat {
											border: 0;
											height: auto; }
											.content .inner {
												border: 1px solid red;
												height: 66px;
												margin: 10px 0; }
												.content .inner.static {
													margin-top: 0;
													border: 1px solid blue; }
													.content .item {
														display: inline-block;
														vertical-align: top;
														width: 120px;
														border: 1px solid blue;
														font-size: 16px;
														margin: 10px;
														overflow: hidden; }
														.content .item.sticky {
															border: 1px solid red;
															height: 100px; }
															.content .inline_columns {
																font-size: 0; }
																.content .main {
																	margin: 10px;
																	margin-left: 222px;
																	border: 1px solid blue;
																	height: auto;
																	overflow: hidden; }
																	.content .main.short {
																		height: auto; }
																		.content .main.tall {
																			height: auto; }

																			.footer {
																				margin: 10px;
																				text-align: center;
																				font-size: 13px;
																				border-top: 1px dashed #dadada;
																				color: #666;
																				padding-top: 10px;
																				min-height: 133px; }

																				.sub {
																					color: #999; }

																					@media all and (max-width: 500px) {
																						.content .sidebar {
																							width: 100px; }
																							.content .item {
																								width: 60px; }
																								.content .main {
																									margin-left: 122px; }
																									.content.double .main {
																										margin-left: 234px; }
																										.content.right .main {
																											margin-right: 120px; } }
																										</style>
																									</head>
																									<body >

																										<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
																										<main class="cd-main-content">
																											<?php include('paginas_include/productos/producto-main.php'); ?>
																										</main>
																										<?php include('paginas_include/estructura/pie.php') ; ?>

																										<script src="<?php echo $ruta_js2; ?>js/foundation.min.js"></script>
																										<script src="<?php echo $ruta_js2; ?>js/setup.js"></script>
																										<script src="<?php echo $Servidor_url;?>js/jquery-1.11.2.min.js"></script> 
																										<script src="<?php echo $Servidor_url;?>js/waypoints/lib/jquery.waypoints.min.js"></script>
																										<script src="<?php echo $Servidor_url;?>js/waypoints/lib/shortcuts/sticky.min.js"></script>

																										<script>
											/*
											var sticky = new Waypoint.Sticky({
												element: $('#botones_sticky')[0]
											});
											*/

										</script>
										<?php include('paginas_include/estructura/javascript-pie.php');?>	
										<script type="text/javascript">		
											function activar_foto(color, original, imagen) {
												$("#xzoom-fancy").attr("src","<?php echo $ruta_imagenes_recortes; ?>"+imagen);
												$("#xzoom-fancy").attr("xoriginal","<?php echo $ruta_imagenes; ?>"+original);

												$('.color_imagen').removeClass('color_imagen_activada');
												$('#color_imagen_id_'+color).addClass('color_imagen_activada');
												$('#producto_color_elegido').val(color);
											}

											function activar_foto2(color) {
												$('.color_imagen').removeClass('color_imagen_activada');
												$('#color_imagen_id_'+color).addClass('color_imagen_activada');
												$('#producto_color_elegido').val(color);
											}

											function activar_modelo(modelo) {
												$('.mostrar_modelos').removeClass('modelo_activado');
												$('#producto_modelo'+modelo).addClass('modelo_activado');
												$('#producto_modelo_elegido').val(modelo);
											}

											function activar_talle(talle) {
												$('.mostrar_talles').removeClass('modelo_activado');
												$('#producto_talle'+talle).addClass('modelo_activado');
												$('#producto_talle_elegido').val(talle);
											}

											function add_to_cart_sticky(producto) {
												$('#cart_loader').show();
					//alert(producto);
					$('#add_to_cart_producto_sticky').html('<i class="fa fa-check fa-stack-1x fa-inverse"></i>');
					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-cart.php?producto="+producto,
						success: function (resultado) {
							$('#add_to_cart_producto_sticky').html('<i class="fa fa-cart-plus fa-stack-1x fa-inverse"></i>');

							var analizar_resultado = resultado.split("*****");

							$('#cart_count').html(analizar_resultado[0]);
							$('#contenedor_cart').html(analizar_resultado[1]);
							$('#cart_loader').hide();
						}
					});	
				}
				function currency(value, decimals, separators) {
					decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
					separators = separators || ['.', "'", ','];
					var number = (parseFloat(value) || 0).toFixed(decimals);
					if (number.length <= (4 + decimals))
						return number.replace('.', separators[separators.length - 1]);
					var parts = number.split(/[-.]/);
					value = parts[parts.length > 1 ? parts.length - 2 : 0];
					var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
						separators[separators.length - 1] + parts[parts.length - 1] : '');
					var start = value.length - 6;
					var idx = 0;
					while (start > -3) {
						result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
						+ separators[idx] + result;
						idx = (++idx) % 2;
						start -= 3;
					}

					var retorno =  (parts.length == 3 ? '-' : '') + result;

					return retorno;
				}

				function aumentar_cantidad(operacion) {
					var cantidad = $("#producto_cantidad").val();
					var stock = <?php echo $producto_stock; ?>;
					var precio_total = <?php echo $producto_precio_total; ?>;
					if(operacion == 0) {
						if(cantidad > 1) {
							cantidad = parseInt(cantidad) - 1;
						}
					}
					if(operacion == 1) {
						if(cantidad < stock) {
							cantidad = parseInt(cantidad) + 1;
						}
					}

					var precio_con_cantidad = parseInt(cantidad) * precio_total;

					var precio_mostrar = currency(precio_con_cantidad, 2, ',');

					$("#total_price_contenedor").html('US $'+precio_mostrar);
					$("#producto_cantidad").val(cantidad);
					$("#cantidad_txt").html(cantidad);
				}

				function add_to_wish_list_sticky(producto) {
					//alert(producto);
					$('#btn_add_to_wish_list_icon').html('<i class="fa fa-heart"></i> Add to Wish List!');
					var redireccion = "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-wish-list.php?producto="+producto;

					var color = $('#producto_color_elegido').val();
					var talle = $('#producto_talle_elegido').val();
					var modelo = $('#producto_modelo_elegido').val();
					var cantidad = $('#producto_cantidad').val();

					redireccion += '&cantidad=' + cantidad + '&color=' + color + '&talle=' + talle + '&modelo=' + modelo;

					$.ajax({
						url: redireccion,
						success: function (resultado) {
							$('#btn_add_to_wish_list_icon').html('<i class="fa fa-heart"></i> into Wish List!');
						}
					});	
				}

				function buynow() {
					var redireccion = '<?php echo $Servidor_url; ?>buynow/';

					var color = $('#producto_color_elegido').val();
					var talle = $('#producto_talle_elegido').val();
					var modelo = $('#producto_modelo_elegido').val();
					var cantidad = $('#producto_cantidad').val();

					redireccion += cantidad + '/' + color + '/' + talle + '/' + modelo + '/';
					redireccion += '<?php echo $producto_url; ?>/';

					window.location.href = redireccion;

				}
				$('.countdown_producto').downCount({
					date: '<?php echo $cuenta_regresiva_fecha; ?>',
					offset: +10
				}, function () {
			//alert('WOOT WOOT, done!');
		});

	</script>
</body>
</html>