	<!-- fancy start -->
	<div id="contenedor_xZoom">
		<section id="fancy">
			<div class="xzoom-container">
				<img class="xzoom4" id="xzoom-fancy" src="<?php echo $ruta_imagenes_recortes.$array_imagenes[$foto_portada]; ?>" xoriginal="<?php echo $ruta_imagenes.$array_foto[$foto_portada]; ?>" />
				<?php if($array_fotos_mostrar) { ?>
				<div class="xzoom-thumbs">

					<?php $i=1; foreach ($array_fotos_mostrar as $id_foto => $imagen) {
						$original = $array_foto[$id_foto];
						$miniatura = $array_imagenes_miniatura[$id_foto];
						?>

						<a href="<?php echo $ruta_imagenes.$original; ?>"><img class="xzoom-gallery4" id="foto<?php echo $id_foto; ?>" width="76.7" src="<?php echo $ruta_imagenes_recortes.$miniatura; ?>"  xpreview="<?php echo $ruta_imagenes_recortes.$imagen; ?>" title="<?php echo $producto_titulo; ?>"></a>

						<?php 
						if($i==6) {
							break;
						}
						$i++;} ?>

					</div>
					<?php } ?>
				</div>     

			</section> 
		</div>  
		<!-- fancy end -->
