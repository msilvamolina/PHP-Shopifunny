<div class="contenido_producto">
	<h3 class="producto_titulo"><?php echo $producto_titulo; ?></h3>
	<div class="rating_bar_contenedor">
		<div class="estrellas">
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-half-o"></i>
			<i class="fa fa-star-o"></i> 
			<p><?php echo $votes; ?> votes | <?php echo $sold; ?> orders</p></div>
		</div>

		<div class="people_watching_now"><i class="fa fa-fire"></i> <?php echo $watching; ?> people watching now!</div>

		<?php if($array_fotos_mostrar) { ?>
		<section class="regularFoto slider panel_diseno3 slick-no-margin">
			<?php $i=1; foreach ($array_fotos_mostrar as $id_foto => $imagen) { ?>
			<div class="imagen_contenedor_slider">
				<img src="<?php echo $ruta_imagenes_recortes.$imagen; ?>" alt="">
			</div>
			<?php } ?>
		</section>
		<?php } ?>
		<div class="contenedor_tabla_productos">


			<div class="row">
				<div class="col-md-3 boostrap_no_width_right">Price:</div>
				<div class="col-md-9 boostrap_no_width_left"><span class="producto_price tachado"><?php echo formato_moneda($producto_precio_dolar_comparado, ''); ?> / piece</span></div>
			</div>
			<div class="row">
				<div class="col-md-3 boostrap_no_width_right">Discount Price:</div>
				<div class="col-md-9 boostrap_no_width_left producto_discount_price"><span class="precio_grande"><?php echo formato_moneda($producto_precio_dolar, 'dolar'); ?></span> / piece
					<span class="porcentaje_descuento"><span class="porcentaje_descuento_numero">Save <?php echo $porcentaje_diferencia; ?>%</span>
					
				</span>
			</div>
		</div>
		<input type="hidden" id="producto_cantidad" value="1" >
		<div class="row row_espaciada">
			<div class="col-md-3 boostrap_no_width_right">Shipping: </div>
			<div class="col-md-9 boostrap_no_width_left shipping_txt">
				<?php if($shipping_shipping) { ?>

				<span class="shipping">
					<?php echo formato_moneda($shipping_shipping, 'dolar'); ?>
				</span> / piece - worldwide shipping

				<?php } else { ?>

				<span class="free_shipping">
					<i class="fa fa-truck"></i> Free Shipping!
				</span>
				<?php  } ?>

			</div>
		</div>

		<input type="hidden" id="producto_talle_elegido" value="0" >
		<div class="row row_espaciada">
			<div class="col-md-3 boostrap_no_width_right">Quantity: </div>
			<div class="col-md-9 boostrap_no_width_left">
				<div class="cantidad_contenedor">
					<a class="cantidad_restar" onclick="aumentar_cantidad(0)"> - </a>
					<span id="cantidad_txt">1</span>
					<a class="cantidad_sumar" onclick="aumentar_cantidad(1)"> + </a>
				</div>
			</div>
		</div>
		<div class="row total_price">
			<div class="col-md-3 boostrap_no_width_right">Total Price: </div>
			<div class="col-md-9 boostrap_no_width_left"><span id="total_price_contenedor"><?php echo formato_moneda($producto_precio_total, 'dolar'); ?></span></div>
		</div>
		<?php if($lista_colores) { ?>
		<div class="row">
			<div class="col-md-3 boostrap_no_width_right">Color: </div>
			<div class="col-md-9 boostrap_no_width_left">
				<?php $i=0; $producto_color_elegido = 0;
				foreach ($lista_colores as $color) { 
					if($color) {
						$activada = null;

						if($i==0) {
							$activada = "color_imagen_activada";
							$producto_color_elegido = $color;
						}

						if($array_foto_tiene_color[$color]) {
							$id_foto = $array_foto_tiene_color[$color];
							$miniatura = $array_imagenes_miniatura[$id_foto];
							$original = $array_foto[$id_foto];
							$imagen = $array_imagenes[$id_foto];
							?>
							<a class="color_imagen <?php echo $activada; ?>" id="color_imagen_id_<?php echo $color; ?>" onclick="activar_foto(<?php echo $color; ?>, '<?php echo $original; ?>',  '<?php echo $imagen; ?>')">
								<img width="55" alt="<?php echo $colores_nombres[$color]; ?>" title="<?php echo $colores_nombres[$color]; ?>" class="border_color<?php echo $color; ?>" title="<?php echo $producto_titulo; ?>" src="<?php echo $ruta_imagenes_recortes.$miniatura; ?>" >
							</a>
							<?php } else { ?>
							<a class="color_imagen <?php echo $activada; ?>" id="color_imagen_id_<?php echo $color; ?>" onclick="activar_foto2(<?php echo $color; ?>)">
								<img width="55" alt="<?php echo $colores_nombres[$color]; ?>" title="<?php echo $colores_nombres[$color]; ?>" class="border_color<?php echo $color; ?>" title="<?php echo $producto_titulo; ?>" src="<?php echo $Servidor_url; ?>img/sistema/imagen_color.php?color=<?php echo $colores_codigos[$color]; ?>" alt="">
							</a>
							<?php } ?>
							<?php $i++;} }?>
						</div>
					</div>
					<?php } ?>
					<input type="hidden" id="producto_color_elegido" value="<?php echo $producto_color_elegido; ?>" >
					<?php if($lista_modelos) { ?>
					<div class="row">
						<div class="col-md-3">Model: </div>
						<div class="col-md-9">
							<ul class="lista_modelos">
								<?php foreach ($lista_modelos as $id_modelo) {
									$modelo_nombre = $modelos_nombres[$id_modelo]; ?>
									<a class="mostrar_modelos" id="producto_modelo<?php echo $id_modelo; ?>" onclick="activar_modelo(<?php echo $id_modelo; ?>)"><li><?php echo $modelo_nombre; ?></li></a>
									<?php } ?>
								</ul>
							</div>
						</div>
						<?php } ?>
						<input type="hidden" id="producto_modelo_elegido" value="0" >
						<?php if($lista_talles) { ?>
						<div class="row">
							<div class="col-md-3">Size: </div>
							<div class="col-md-9">
								<ul class="lista_modelos">
									<?php foreach ($lista_talles as $id_talle) {
										$talle_nombre = $talles_nombres[$id_talle]; ?>
										<a class="mostrar_talles" id="producto_talle<?php echo $id_talle; ?>" onclick="activar_talle(<?php echo $id_talle; ?>)"><li><?php echo $talle_nombre; ?></li></a>
										<?php } ?>
									</ul>
								</div>
							</div>
							<?php } ?>

							<div class="row">

								<div class="col-md-3">Buy: </div>
								<div class="col-md-9">

									<div id="botones_sticky">
										<div class="sticky_contenido">
											<h3 class="titulo_sticky"><?php echo $producto_titulo; ?></h3>
											<div class="botones_comprar_cart">
												<a onclick="buynow()" class="vc_btn_largo vc_btn_verde vc_btn_3d boton_cart_separacion">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-credit-card-alt fa-stack-1x fa-inverse"></i>
													</span>
													<b>
														<span class="btn_txt_largo">Buy now!</span>
														<span class="btn_txt_chico">Buy!</span>
													</b>

												</a>
												<a  class="vc_btn_largo vc_btn_naranja vc_btn_3d " onclick="add_to_cart_sticky(<?php echo $producto; ?>)">

													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<div id="add_to_cart_producto_sticky">
															<i class="fa fa-cart-plus fa-stack-1x fa-inverse"></i>
														</div>
													</span>
													<b>
														<span class="btn_txt_largo">Add to cart</span>
														<span class="btn_txt_chico">Add</span>
													</b>
												</a>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="cotenedor_general_barra_sold">
										<div class="contenedor_harry">HURRY! ONLY <span>12</span> LEFT IN STOCK</div>

										<div id="progressBar" class="default" ><div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar"></div></div>
										
										<div id="countdown_producto2" class="contenedor_dias">
											<span id="days_producto">00</span>
											<span id="hours_producto">00</span>
											<span id="minutes_producto">00</span>
											<span id="seconds_producto">00</span>
										</div>
										<div class="contenedor_dias2">
											<span class="">Days</span>
											<span class="">Hours</span>
											<span class="">Mins</span>
											<span class="">Secs</span>
											<div class="clear"></div>
										</div>
									</div>

									<p id="btn_add_to_wish_list_icon" style="color:red">
										<a onclick="add_to_wish_list_sticky(<?php echo $producto; ?>)">
											<span id="btn_add_to_wish_list_icon"><i class="fa fa-heart-o"></i> Add to Wish List! (<?php echo $wish_list_adds; ?> adds)</span></a></p>
											<a href="<?php echo $Servidor_url; ?>n/buyer-protection/" target="blank">
												<img style="margin-top: -10px; margin-bottom: 10px;" src="<?php echo $Servidor_url; ?>img/secure-checkout_large.png" width="100%" alt="">
											</a>
										</div>
									</div>

								</div>

							</div>
							<div class="clear"> </div>