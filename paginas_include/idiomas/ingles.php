<?php
//Acá van a estar todas las variables de idiomas

//checkout
$TXT_chechout = 'Checkout';
$TXT_no_hay_productos = 'YOUR CART IS EMPTY';
$TXT_customer_information = 'Customer information';
$TXT_email = 'E-mail';
$TXT_keep_me_out_to_date = 'Keep me up to date on news and exclusive offers';
$TXT_shipping_address = 'Shipping address';
$TXT_first_name = 'First Name';
$TXT_last_name = 'Last Name';
$TXT_address = 'Address';
$TXT_address2 = 'Apt, suite, etc. (optional)';
$TXT_please_provide_valid_address = 'Please provide a valid address.';
$TXT_city = 'City';
$TXT_country = 'Country';
$TXT_please_provide_valid_country = 'Please provide a valid country.';
$TXT_please_provide_valid_state = 'Please provide a valid';
$TXT_zip = 'Postal Code';
$TXT_please_provide_valid_zip = 'Please provide a valid Postal Code.';
$TXT_save_this_information = 'Save this information for next time';
$TXT_continue_to_payment = 'Continue to PayPal';
$TXT_state = "State";

$TXT_please_provide_valid_email = 'Please provide a valid E-mail.';

$TXT_ingresa_un_cupon_valido = 'Ingresá un cupón válido';

$TXT_please_provide_valid_first_name = 'Please provide a valid first name.';
$TXT_please_provide_valid_last_name = 'Please provide a valid last name.';
$TXT_please_provide_valid_city = 'Please provide a valid city.';
$TXT_please_provide_valid_phone = 'Please provide a valid phone name.';

$TXT_please_provide_color = 'Please select color';
$TXT_please_provide_model = 'Please select a model';

$TXT_please_provide_size = 'Please select a size';

?>