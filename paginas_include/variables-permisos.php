<?php
session_start();

if(!$_SESSION['codigo_compra']){

	if($_COOKIE['codigo_compra']) {
		$_SESSION['codigo_compra'] = $_COOKIE['codigo_compra'];
	} else {
		$fecha_actual = date('Y-m-d H:i:s');
		$codigo_compra = sha1($fecha_actual.$ip_visitante.rand());
		$_SESSION['codigo_compra'] = $codigo_compra;
		setcookie("codigo_compra",$codigo_compra,time() + (86400 *  365), '/');  
	}
}
?>