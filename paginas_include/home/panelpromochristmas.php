<?php $panelpromo++; ?>	
<div class="contenedor_promocion">
	<div class="promocion_columna0">
		<img src="<?php echo $Servidor_url; ?>img/panel-promos-christmas2.png" class="titulo_black_friday" />
	</div>
	<div class="promocion_columna2">
		<div class="promocion_fila1">
			<div class="promocion_producto4">
				<img src="<?php echo $Servidor_url; ?>img/promo.jpg" />
			</div>
			<div class="promocion_producto3">	
				<?php include('promoproductosolo.php'); ?>
			</div>
		</div>
		<div class="promocion_fila2">
			<div class="promocion_producto1">
				<?php include('promoproductosolo.php'); ?>
			</div>
			<div class="promocion_producto2">
				<?php include('promoproductosolo.php'); ?>
			</div>
			<div class="promocion_producto3">	
				<?php include('promoproductosolo.php'); ?>
			</div>
		</div>
	</div>
	<div class="promocion_columna">
		<!-- masterslider -->
		<div class="master-slider ms-skin-default" id="masterslider<?php echo $panelpromo; ?>">
			<div class="ms-slide slide-1">
				<img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/blank.gif" data-src="<?php echo $Servidor_url; ?>img/carrousel/free-watches.jpg" alt="lorem ipsum dolor sit"/>  
			</div>
			<div class="ms-slide slide-2">
				<img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/blank.gif" data-src="<?php echo $Servidor_url; ?>img/carrousel/free-watches.jpg" alt="lorem ipsum dolor sit"/>  
			</div>
			<div class="ms-slide slide-3">
				<img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/blank.gif" data-src="<?php echo $Servidor_url; ?>img/carrousel/free-watches.jpg" alt="lorem ipsum dolor sit"/>   
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<script>
	var slider<?php echo $panelpromo; ?> = new MasterSlider();

	slider<?php echo $panelpromo; ?>.control('arrows' ,{insertTo:'#masterslider<?php echo $panelpromo; ?>'});	
	slider<?php echo $panelpromo; ?>.control('bullets');	

	slider<?php echo $panelpromo; ?>.setup('masterslider<?php echo $panelpromo; ?>' , {
		width:434,
		height:550,
		autoplay: true,
		speed:20
	});
</script>