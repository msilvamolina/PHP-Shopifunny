<div class="cd-overlay"></div>

<nav class="cd-nav">
	<ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
		<li class="has-children">
			<a href="https://www.shopifunny.com/">Categories</a>

			<ul class="cd-secondary-nav is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
				<li class="see-all"><a href="https://www.shopifunny.com/">All Clothing</a></li>
				<li class="has-children">
					<a href="https://www.shopifunny.com/">Accessories</a>

					<ul class="is-hidden">
						<li class="go-back"><a href="#0">Categories</a></li>
						<li class="see-all"><a href="https://www.shopifunny.com/">All Accessories</a></li>
						<li class="has-children">
							<a href="#0">Women's Clothing</a>

							<ul class="is-hidden">
								<li class="go-back"><a href="#0">Women's Clothing</a></li>
								<li class="see-all"><a href="https://www.shopifunny.com/">Hot Categories</a></li>
								<li><a href="https://www.shopifunny.com/">Bottoms</a></li>
								<li><a href="https://www.shopifunny.com/">Outwear &amp; Jackets</a></li>
								<li><a href="https://www.shopifunny.com/">Tops &amp; Sets</a></li>
								<li><a href="https://www.shopifunny.com/">Weddings &amp; Events</a></li>

								<li><a href="https://www.shopifunny.com/">Accessories</a></li>


							</ul>
						</li>
						<li class="has-children">
							<a href="#0">Caps &amp; Hats</a>

							<ul class="is-hidden">
								<li class="go-back"><a href="#0">Accessories</a></li>
								<li class="see-all"><a href="https://www.shopifunny.com/">All Caps &amp; Hats</a></li>
								<li><a href="https://www.shopifunny.com/">Beanies</a></li>
								<li><a href="https://www.shopifunny.com/">Caps</a></li>
								<li><a href="https://www.shopifunny.com/">Hats</a></li>
							</ul>
						</li>
						<li><a href="https://www.shopifunny.com/">Glasses</a></li>
						<li><a href="https://www.shopifunny.com/">Gloves</a></li>
						<li><a href="https://www.shopifunny.com/">Jewellery</a></li>
						<li><a href="https://www.shopifunny.com/">Scarves</a></li>
						<li><a href="https://www.shopifunny.com/">Wallets</a></li>
						<li><a href="https://www.shopifunny.com/">Watches</a></li>
					</ul>
				</li>

				<li class="has-children">
					<a href="https://www.shopifunny.com/">Bottoms</a>

					<ul class="is-hidden">
						<li class="go-back"><a href="#0">Clothing</a></li>
						<li class="see-all"><a href="https://www.shopifunny.com/">All Bottoms</a></li>
						<li><a href="https://www.shopifunny.com/">Casual Trousers</a></li>
						<li class="has-children">
							<a href="#0">Jeans</a>

							<ul class="is-hidden">
								<li class="go-back"><a href="#0">Bottoms</a></li>
								<li class="see-all"><a href="https://www.shopifunny.com/">All Jeans</a></li>
								<li><a href="https://www.shopifunny.com/">Ripped</a></li>
								<li><a href="https://www.shopifunny.com/">Skinny</a></li>
								<li><a href="https://www.shopifunny.com/">Slim</a></li>
								<li><a href="https://www.shopifunny.com/">Straight</a></li>
							</ul>
						</li>
						<li><a href="#0">Leggings</a></li>
						<li><a href="#0">Shorts</a></li>
					</ul>
				</li>

				<li class="has-children">
					<a href="https://www.shopifunny.com/">Jackets</a>

					<ul class="is-hidden">
						<li class="go-back"><a href="#0">Clothing</a></li>
						<li class="see-all"><a href="https://www.shopifunny.com/">All Jackets</a></li>
						<li><a href="https://www.shopifunny.com/">Blazers</a></li>
						<li><a href="https://www.shopifunny.com/">Bomber jackets</a></li>
						<li><a href="https://www.shopifunny.com/">Denim Jackets</a></li>
						<li><a href="https://www.shopifunny.com/">Duffle Coats</a></li>
						<li><a href="https://www.shopifunny.com/">Leather Jackets</a></li>
						<li><a href="https://www.shopifunny.com/">Parkas</a></li>
					</ul>
				</li>

				<li class="has-children">
					<a href="https://www.shopifunny.com/">Tops</a>

					<ul class="is-hidden">
						<li class="go-back"><a href="#0">Clothing</a></li>
						<li class="see-all"><a href="https://www.shopifunny.com/">All Tops</a></li>
						<li><a href="https://www.shopifunny.com/">Cardigans</a></li>
						<li><a href="https://www.shopifunny.com/">Coats</a></li>
						<li><a href="https://www.shopifunny.com/">Hoodies &amp; Sweatshirts</a></li>
						<li><a href="https://www.shopifunny.com/">Jumpers</a></li>
						<li><a href="https://www.shopifunny.com/">Polo Shirts</a></li>
						<li><a href="https://www.shopifunny.com/">Shirts</a></li>
						<li class="has-children">
							<a href="#0">T-Shirts</a>

							<ul class="is-hidden">
								<li class="go-back"><a href="#0">Tops</a></li>
								<li class="see-all"><a href="https://www.shopifunny.com/">All T-shirts</a></li>
								<li><a href="https://www.shopifunny.com/">Plain</a></li>
								<li><a href="https://www.shopifunny.com/">Print</a></li>
								<li><a href="https://www.shopifunny.com/">Striped</a></li>
								<li><a href="https://www.shopifunny.com/">Long sleeved</a></li>
							</ul>
						</li>
						<li><a href="https://www.shopifunny.com/">Vests</a></li>
					</ul>
				</li>
			</ul>
		</li>

		<li class="has-children">
			<a href="https://www.shopifunny.com/">Best Selling</a>

			<ul class="cd-nav-gallery is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
				<li class="see-all"><a href="https://www.shopifunny.com/">Browse Gallery</a></li>
				<li>
					<a class="cd-nav-item" href="https://www.shopifunny.com/">
						<img src="img/img.jpg" alt="Product Image">
						<h3>Product #1</h3>
					</a>
				</li>

				<li>
					<a class="cd-nav-item" href="https://www.shopifunny.com/">
						<img src="img/img.jpg" alt="Product Image">
						<h3>Product #2</h3>
					</a>
				</li>

				<li>
					<a class="cd-nav-item" href="https://www.shopifunny.com/">
						<img src="img/img.jpg" alt="Product Image">
						<h3>Product #3</h3>
					</a>
				</li>

				<li>
					<a class="cd-nav-item" href="https://www.shopifunny.com/">
						<img src="img/img.jpg" alt="Product Image">
						<h3>Product #4</h3>
					</a>
				</li>
			</ul>
		</li>

		

		<li><a href="https://www.shopifunny.com/"><i class="fa fa-flash"></i> Flash Deals</a></li>
		<li><a href="https://www.shopifunny.com/"><i class="fa fa-lock"></i> Buyer Protection</a></li>


	</ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
	<form>
		<input type="search" placeholder="Search...">
	</form>
</div>