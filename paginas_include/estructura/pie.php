 <footer class="pie_general">
 	<div class="contenedor_pie">
 		<div class="panel_suscripcion">
 			<div class="panel_opcion_flotante_largo">
 				<div class="contenedor_suscripcion">
 					<center>
 						<h3>Subscription</h3>

 						<form class="">
 							<div class="form-group">
 								<input type="email" class="form-control input_suscribe" id="suscripcion_email" aria-describedby="emailHelp" placeholder="Enter email">
 							</div>
 							<button type="button" onclick="mandar_suscripcion()" id="boton_suscripcion" class="btn btn-primary boton_suscribe">Suscribe!</button>
 						</form>
 						<div class="clear"></div>
 						<small id="emailHelp" class="form-text text-muted">
 							<span id="suscribe_email_error" style="color: red !important; display:none">Please provide a valid E-mail.</span>
 							<span id="suscribe_email_done" style="color: green !important; display:none">Done!</span>
 							<span id="suscribe_email_txt" >Register now to get updates on promotions and coupons.</span>

 						</small>
 					</center>
 				</div>

 				<center style="margin-top: 15px">
 					<div id="contenedor_redes_sociales">
 						<h3>Stay Connected</h3>

 						<div class="ssk-group">
 							<a href="" class="ssk ssk-facebook"></a>
 							<a href="" class="ssk ssk-twitter"></a>
 							<a href="" class="ssk ssk-instagram"></a>
 							<a href="" class="ssk ssk-google-plus"></a>
 							<a href="" class="ssk ssk-pinterest"></a>
 						</div>
 					</div>
 				</center>
 			</div>
 			
 			<div class="panel_opciones">
 				<div class="panel_opcion_flotante">
 					<img src="<?php echo $Servidor_url?>img/dollar.png" alt="">
 					<h3>Great Value</h3>
 					<p>We offer competitive prices on all our products range.</p>
 				</div>
 				<div class="panel_opcion_flotante">
 					<img src="<?php echo $Servidor_url?>img/trucks.png" alt="">
 					<h3>WorldWide Delivery</h3>
 					<p>We offer free shipping to over 200 countries & regions.</p>
 				</div>
 				<div class="panel_opcion_flotante">
 					<img src="<?php echo $Servidor_url?>img/credit-card.png" alt="">
 					<h3>Safe Payment</h3>
 					<p>Pay with the world’s most popular and secure payment methods.</p>
 				</div>
 				<div class="panel_opcion_flotante">
 					<img src="<?php echo $Servidor_url?>img/online-shop.png" alt="">
 					<h3>Shop with Confidence</h3>
 					<p>Our Buyer Protection covers your purchase from click to delivery.</p>
 				</div>
 				<div class="clear"></div>
 			</div>


 			<div class="clear"></div>
 		</div>
 	</div>
 	
 	<div class="ultima_barra_abajo">
 		<center>
 			<ul>
 				<li><a href="#">Intellectual Property Protection -</a></li>
 				<li><a href="#">Privacy Policy -</a></li>
 				<li><a href="#">Terms of Use  -</a></li>
 				<li><a href="#">Law Enforcement Compliance Guide © 2016-<?php echo date('Y'); ?> ShopiFunny.com. All rights reserved.</a></li>
 			</ul>
 		</center>
 	</div>
 </footer>

 <?php if(!$sin_cart) { ?>
 <div id="cd-shadow-layer"></div>

 <div id="cd-cart">
 	<div id="cart_padding"></div>

 	<div class="contenedor_loader" id="cart_loader">
 		<center>
 			<img src="<?php echo $ruta_img_loader; ?>" class="elemento_loader" alt="">
 		</center>
 	</div>
 	<div id="contenedor_cart"></div>

 </div> <!-- cd-cart -->
 <?php } ?>
 <?php include('login-modal.php'); ?>
