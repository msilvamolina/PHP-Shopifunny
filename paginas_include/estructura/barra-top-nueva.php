<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8QJ4ZP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--  Comenzamos con el login -->
<header id="encabezado_top" class="header_grande">
	<div id="logo">
		<a href="<?php echo $Servidor_url; ?>">
			<img src="<?php echo $Servidor_url; ?>img/nuevo-nuevo-logo.png" alt="Shopifunny" title="Shopifunny"></a>
		</div>

		<div id="cd-hamburger-menu"><a class="cd-img-replace" href="#0">Menu</a></div>
		<div id="cd-cart-trigger"><span id="cart_count">0</span><a class="cd-img-replace" href="#0">Cart</a></div>
	</header>

	<nav id="main-nav" style="z-index:998" class="nav_grande main-nav">
		<ul>
			<li class="opcion_color_verde ">
				<a href="#categories" class="open-popup-link">
					<i class="fa fa-bars"></i> <span class="nombre_opcion">Categories</span>
				</a>
			</li>

			<li class="opcion_color_rojo nav_li_opcion"><a href="<?php echo $Servidor_url;?>wish-list/" class="nav_li_opcion"><i class="fa fa-heart"></i> <span class="nombre_opcion">Wish List</span></a></li>
			
			<!--
			<li class="opcion_color_azul nav_li_opcion"><a href="#0" id="boton_ingresa" class="cd-signin nav_li_opcion"><i class="fa fa-address-card"></i> <span class="nombre_opcion">My Account</span></a></li>
			-->

			
			<li class="opcion_color_azul nav_li_opcion"><a href="<?php echo $Servidor_url; ?>n/about/" class="nav_li_opcion"><i class="fa fa-info-circle"></i> <span class="nombre_opcion">About us</span></a></li>
			

			<li class="opcion_color_naranja nav_li_opcion"><a href="<?php echo $Servidor_url;?>help/" class="nav_li_opcion"><i class="fa fa-life-ring"></i> <span class="nombre_opcion">Help!</span></a></li>

			<li class="opcion_color_rojo_bloque nav_li_opcion">
				<a href="#search" class="open-popup-link">
					<i class="fa fa-search"></i> <span class="nombre_opcion">Search</span>
				</a>
			</li>
		</ul>
	</nav>

	<!-- Pantallas popups -->
	<div id="search" class="white-popup mfp-hide">
		<form action="javascript:mandar_busqueda()">
			<h3>Search</h3>
			<input type="text" id="input_buscar" placeholder="I'm shopping for..." required  autofocus >
			<br><br>
			<center>
				<button type="submit" class="btn btn-lg btn-success"><i class="fa fa-search fa-fw"></i> Search!</button>
			</center>
		</form>
	</div>
	<div id="currency" class="white-popup mfp-hide">
		<h3>Currency</h3>

	</div>
	<?php 
	conectar2('shopifun', "admin");
	//consultar en la base de datos
	$query_rs_categorias = "SELECT id_categoria, categoria_nombre, categoria_url FROM categorias ORDER BY categoria_nombre ASC ";
	$rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
	$row_rs_categorias = mysql_fetch_assoc($rs_categorias);
	$totalrow_rs_categorias = mysql_num_rows($rs_categorias);
	
	desconectar();

	?>
	<div id="categories" class="white-popup mfp-hide">
		<h3>Categories</h3>
		<ul>
			<?php do { 
				$id_categoria = $row_rs_categorias['id_categoria'];
				$categoria_nombre = $row_rs_categorias['categoria_nombre'];
				$categoria_url = $row_rs_categorias['categoria_url'];
				?>
				<li><a href="<?php echo $Servidor_url; ?>c/<?php echo $categoria_url; ?>/"><?php echo $categoria_nombre; ?></a></li>

				<?php } while ($row_rs_categorias = mysql_fetch_assoc($rs_categorias)); ?>
			</ul>
		</div>