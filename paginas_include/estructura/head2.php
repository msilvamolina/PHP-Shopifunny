<?php $version_css = 103; ?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $Servidor_url;?>apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Servidor_url;?>apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Servidor_url;?>apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Servidor_url;?>apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $Servidor_url;?>apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $Servidor_url;?>apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $Servidor_url;?>apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $Servidor_url;?>apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style2.css"> <!-- Resource style -->

<meta name="google-site-verification" content="99WI4MOmgmtSbJhKqzIObQlPu63S8fflt8A3zC03Z-w" />


<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style4.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->
<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style-barra.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->

<script src="<?php echo $Servidor_url;?>js/modernizr.js"></script> <!-- Modernizr -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />
<script src="<?php echo $Servidor_url; ?>recursos/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/bootstrap/css/bootstrap.min.css" />

<link href="<?php echo $Servidor_url; ?>css/estilohome.css?v=<?php echo $version_css; ?>" rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="<?php echo $Servidor_url; ?>js/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $Servidor_url; ?>js/slick/slick-theme.css?v=<?php echo $version_css; ?>">

<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/fondos.css" />
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/colores.css" />
<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/diseno-paneles.css?v=<?php echo $version_css; ?>" />

<title><?php echo $titulo_pagina; ?></title>


<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/arreglo-responsive.css?v=<?php echo $version_css; ?>" />