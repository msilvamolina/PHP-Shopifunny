<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
	if( !window.jQuery ) document.write('<script src="<?php echo $Servidor_url;?>js/jquery-3.0.0.min.js"><\/script>');
</script>
<script src="<?php echo $Servidor_url;?>js/main4.js"></script> <!-- Resource jQuery -->

<script src="<?php echo $Servidor_url;?>js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo $Servidor_url;?>js/main.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url;?>js/main2.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url;?>js/barratop.js"></script> <!-- Resource jQuery -->
<script src="<?php echo $Servidor_url;?>js/itemcart.js"></script> <!-- Resource jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script src="<?php echo $Servidor_url; ?>js/login.js"></script>


<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo $Servidor_url; ?>js/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>js/jquery.countdown.min.js"></script> 
<script type="text/javascript" src="<?php echo $Servidor_url; ?>js/popup.js"></script> 



<script type="text/javascript">		

	$(document).ready(function() {
		$('.open-popup-link').magnificPopup({
			type:'inline',
		  midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		  alignTop: true,
		  preloader: false,
		  removalDelay: 300,
		  focus: '#input_buscar',
		  mainClass: 'my-mfp-zoom-in'
		});
		add_to_cart(0,0); 
	});


	function add_to_cart(producto, i) {
		$('#cart_loader').show();
					//alert(producto);
					$('#add_to_cart_producto_'+i).html('<i class="fa fa-check"></i>');
					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-cart.php?producto="+producto,
						success: function (resultado) {
							$('#add_to_cart_producto_'+i).html('<i class="fa fa-cart-plus"></i>');

							var analizar_resultado = resultado.split("*****");

							$('#cart_count').html(analizar_resultado[0]);
							$('#contenedor_cart').html(analizar_resultado[1]);
							$('#cart_loader').hide();
						}
					});	
				}
				function add_to_wish_list(producto, i) {
					//alert(producto);
					$('#add_to_wish_list_producto_'+i).html('<i class="fa fa-heart"></i>');
					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/sistema/add-to-wish-list.php?producto="+producto,
						success: function (resultado) {
						}
					});	
				}
				jQuery(document).scroll(function(){
					if(jQuery(this).scrollTop() > 52){
						document.getElementById("encabezado_top").className = "header_chico navbar navbar-inverse navbar-fixed-top";
						document.getElementById("main-nav").className = "nav_chica";
						$( "#cart_padding" ).hide('slow');
					}else{
						document.getElementById("encabezado_top").className = "header_grande navbar navbar-inverse navbar-fixed-top";
						document.getElementById("main-nav").className = "nav_grande";
						$( "#cart_padding" ).show('slow');
					}
				});


				function cerrar_popup() {
					$('.cd-popup').removeClass('is-visible');
				}

				function borrar_item_cart(id_compra) {
					$('#popup_borrar_item').addClass('is-visible');
					$('#btn_confirmar_borrar_item').html('<a onclick="confirmar_borrar_item_cart('+id_compra+')" >Yes</a>');
				}

				function confirmar_borrar_item_cart(id_compra) {
					$('#item_compra'+id_compra).hide();
					$('.cd-popup').removeClass('is-visible');

					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/sistema/remove-item-cart-derecha.php?compra="+id_compra,
						success: function (resultado) {
							var analizar_resultado = resultado.split("*****");

							$('#cart_count').html(analizar_resultado[0]);
							$('#contenedor_cart').html(analizar_resultado[1]);
							$('#cart_loader').hide();
						}
					});	
				}

				function reset_cart() {
					$('#popup_borrar_item2').addClass('is-visible');
					$('#btn_confirmar_borrar_item').html('<a onclick="confirmar_borrar_item_cart()" >Yes</a>');
				}

				function confirm_reset_cart() {
					$('.cd-popup').removeClass('is-visible');

					$.ajax({
						url: "<?php echo $Servidor_url; ?>/ajax/sistema/reset-cart.php",
						success: function (resultado) {
							var analizar_resultado = resultado.split("*****");

							$('#cart_count').html(analizar_resultado[0]);
							$('#contenedor_cart').html(analizar_resultado[1]);
							$('#cart_loader').hide();
						}
					});	
				}


				function mandar_suscripcion() {
					var input_valor = $('#suscripcion_email').val();
					$('#suscripcion_email').removeClass('is-invalid');
					$('#suscripcion_email').removeClass('is-valid');
					$('#suscribe_email_done').hide();
					$('#suscribe_email_error').hide();
					$('#suscribe_email_txt').show();
					if(input_valor) {
						$('#boton_suscripcion').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 18px"></i>');
						document.getElementById("boton_suscripcion").disabled = true;
						var sendInfo = {
							email: input_valor,
						};
						$.ajax({
							type: "POST",
							url: "<?php echo $Servidor_url;?>suscribe/",
							success: function (resultado) {
								$('#boton_suscripcion').html('Suscribe!');
								document.getElementById("boton_suscripcion").disabled = false;
								var inspeccionar = resultado.split('OK*****');

								if(inspeccionar[1]) {
									$('#suscripcion_email').addClass('is-valid');
									$('#suscribe_email_done').show();
									$('#suscribe_email_txt').hide();
									$('#boton_suscripcion').html(':D');
								} else {
									$('#suscripcion_email').addClass('is-invalid');
									$('#suscribe_email_error').show();
									$('#suscribe_email_txt').hide();
								}
								
							},
							data: sendInfo
						});
					}
				}
			</script>
