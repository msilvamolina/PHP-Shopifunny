	<?php $version_css = 165;

	$ruta_js = $Servidor_url.'js/xZoom/';
	$ruta_js2 = $Servidor_url.'js/xZoom/example/';

	//Permisos
	//$agregar_masterslider = 0;
	//$agregar_slick = 0;
	//$agregar_xZoom = 0;
	//$agregar_fancybox = 0;

	?>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $Servidor_url;?>apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Servidor_url;?>apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Servidor_url;?>apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Servidor_url;?>apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $Servidor_url;?>apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $Servidor_url;?>apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $Servidor_url;?>apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $Servidor_url;?>apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<script src="<?php echo $ruta_js2; ?>js/vendor/modernizr.js"></script>
	<script src="<?php echo $ruta_js2; ?>js/vendor/jquery.js"></script>

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style2.css"> <!-- Resource style -->

	<?php if($agregar_xZoom) { ?>
	<!-- xzoom plugin here -->
	<script type="text/javascript" src="<?php echo $ruta_js; ?>dist/xzoom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $ruta_js2; ?>css/xzoom2.css?v=<?php echo $version_css; ?>" media="all" /> 
	<!-- hammer plugin here -->
	<script type="text/javascript" src="<?php echo $ruta_js2; ?>hammer.js/1.0.5/jquery.hammer.min.js"></script>
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<?php } ?>
	<?php if($agregar_fancybox) { ?>
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $ruta_js2; ?>fancybox/source/jquery.fancybox.css" />
	<script type="text/javascript" src="<?php echo $ruta_js2; ?>fancybox/source/jquery.fancybox.js"></script>
	<?php } ?>

	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $ruta_js2; ?>magnific-popup/css/magnific-popup.css" />
	<script type="text/javascript" src="<?php echo $ruta_js2; ?>magnific-popup/js/magnific-popup.js"></script> 
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style4.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style-barra2.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />

	<?php if($bootstrap_viejo) { ?>
	<script src="<?php echo $Servidor_url; ?>recursos/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/bootstrap/css/bootstrap.min.css" />
	<?php } else { ?>
	<script src="<?php echo $Servidor_url; ?>recursos/bootstrap-4.0.0-beta.2/dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/bootstrap-4.0.0-beta.2/dist/css/bootstrap.css" />
	<?php } ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/botones.css?v=<?php echo $version_css; ?>"> <!-- Resource style -->

	<link href="<?php echo $Servidor_url; ?>css/estilohome.css?v=<?php echo $version_css; ?>" rel='stylesheet' type='text/css'>

	<?php if($agregar_slick) { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $Servidor_url; ?>js/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $Servidor_url; ?>js/slick/slick-theme.css?v=<?php echo $version_css; ?>">
	<?php } ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/fondos.css?v=<?php echo $version_css; ?>" />
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/colores.css" />
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/sistema/diseno-paneles.css?v=<?php echo $version_css; ?>" />

	<title><?php echo $titulo_pagina; ?></title>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/arreglo-responsive.css?v=<?php echo $version_css; ?>" />  


	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/login.css?v=<?php echo $version_css; ?>"> <!-- Gem style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/form.css?v=<?php echo $version_css; ?>"> <!-- Gem style -->
	<?php if($agregar_masterslider) { ?>
	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>

	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>
	<?php } ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/popup.css?v=<?php echo $version_css; ?>" />  

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>recursos/social-share-kit/dist/css/social-share-kit.css?v=1.0.14">
	
	<style>
	.nav_grande ul {
		z-index: 999;
	}
</style>
<!-- Global site tag (gtag.js) - Google Analytics 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111416209-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-111416209-1');
</script>-->