	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="cd-switcher">
				<li><a href="#0">Sign in</a></li>
				<li><a href="#0">New account</a></li>
			</ul>
			<div id="cd-login"> <!-- log in form -->
				<form class="cd-form" action="javascript:mandar_login()">
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" value="<?php echo $usuario_email; ?>" id="signin-email" type="email" placeholder="E-mail">
						<span class="cd-error-message mensaje_login">Error!</span>
					</p>
					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Password">
						<a href="#0" class="hide-password">Show</a>
						<span class="cd-error-message mensaje_login">Error!</span>
					</p>
					<div>
						<ul class="cd-form-list">
							<li>
								<input type="checkbox" checked id="cd-checkbox-1" value="1">
								<label for="cd-checkbox-1">Remember me</label>
							</li>
						</ul>
					</div>
					<p class="fieldset">
						<input class="full-width" type="submit" id="btn_login" value="Ingresar">
					</p>
					<br><br>
				</form>
				
				<p class="cd-form-bottom-message"><a href="#0" style="padding-top: 20px">Forgot your password?</a></p>
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->
			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form" action="<?php echo $Servidor_url; ?>nuevo/php/registro-usuario-db.php" method="post" onsubmit="return controlar_registro()">
					<p class="fieldset">
						<label class="image-replace cd-username " for="signup-nombre">First Name</label>
						<input class="half-width has-padding has-border" required id="signup-nombre" name="signup-nombre" type="text" placeholder="First Name">
						<label class="image-replace cd-username " for="signup-apellido">Last Name</label>

						<input class="half-width has-padding has-border" required id="signup-apellido" name="signup-apellido" type="text" placeholder="Last Name">
						<span class="cd-error-message">Error!</span>
					</p>
					<div>
						<ul class="cd-form-list">
							<li>
								<input type="radio" name="signup-sexo" value="Man"  id="signup-sexo" checked>
								<label for="signup-sexo">Man</label>
							</li>

							<li>
								<input type="radio" name="signup-sexo" value="Women" id="signup-sexo"  >
								<label for="signup-sexo">Women</label>
							</li>

						</ul>
					</div>
					<input type="hidden" name="chequear_email_dato" id="chequear_email_dato" value="1">
					<p class="fieldset">
						<label class="image-replace cd-email" for="signup-email">E-mail</label>
						<input class="full-width has-padding has-border" value="<?php echo $usuario_email; ?>" onkeypress="checkear_email()" onblur="checkear_email()" onfocus="checkear_email()"  required id="signup-email" name="signup-email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Esta dirección de e-mail ya se encuentra registrada, o no es una dirección válida.</span>
					</p>
					<p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Password</label>
						<input class="full-width has-padding has-border" required id="signup-password" name="signup-password" type="Password"  placeholder="Password">
						<a href="#0" class="hide-password">Show</a>
						<span class="cd-error-message" id="registro_contrasena_error">La contraseña tiene que tener al menos 6 caracteres</span>
					</p>
					<p class="fieldset">
						<input type="checkbox" id="accept-terms">
						<label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" id="signup-boton" value="Create account">
					</p>
					<br><br>
				</form>
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-signup -->
			<div id="cd-reset-password"> <!-- reset password form -->
				<p class="cd-form-message">¿Olvidaste tu contraseña? Ingresá tu dirección de e-mail y te enviaremos las instrucciones para restablecerla</p>
				<form class="cd-form" id="restablecer_contrase_form" action="javascript:restablecer_contrasena()" onsubmit="controlar_restablecer_contrasena()">
					<p class="fieldset">

						<label class="image-replace cd-email" for="reset-email">E-mail</label>
						<input class="full-width has-padding has-border" value="<?php echo $usuario_email; ?>" required id="reset-email" type="email" placeholder="E-mail">
						<span class="cd-error-message" id="restablecer_contrasena_error">Esta dirección de e-mail no está registrada en nuesto sitio, o no es una dirección válida</span>
					</p>
					<p class="fieldset">
						<input class="full-width has-padding" type="submit" id="boton_restablecer_contrasena" value="Restablecer Contraseña">
					</p>
					<br>
				</form>
				<div id="restablecer_contrase_ok" >
					<p>Te enviamos a tu casilla de e-mail las pasos para que cambiés tu contraseña por otra</p>
				</div>
				<p class="cd-form-bottom-message"><a href="#0">Volver a Ingresar</a></p>
			</div> <!-- cd-reset-password -->
			<a href="#0" class="cd-close-form">Cerrar</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->