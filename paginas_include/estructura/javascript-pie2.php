<script>
	function mandar_busqueda() {
		var busqueda = document.getElementById("input_buscar").value;
		if(busqueda) {
			window.location.href = "<?php echo $Servidor_url; ?>search?q="+busqueda;
		}
	}
</script>
<script src="<?php echo $Servidor_url; ?>recursos/Magnific-Popup/dist/jquery.magnific-popup.js"></script>	

<script type="text/javascript">
	
	function controlar_registro() {
		checkear_email();

		var nombre = document.getElementById("signup-nombre").value;	
		var apellido = document.getElementById("signup-apellido").value;	
		var email = document.getElementById("signup-email").value;	
		var chequear_email_dato = document.getElementById("chequear_email_dato").value;	
		var contrasena = document.getElementById("signup-password").value;	
		var nacimiento_dia = document.getElementById("fecha_dia").value;	
		var nacimiento_mes = document.getElementById("fecha_mes").value;	
		var nacimiento_ano = document.getElementById("fecha_ano").value;	

		var error = null;
		$('#fecha_dia').removeClass('bordes_rojos');
		$('#fecha_mes').removeClass('bordes_rojos');
		$('#fecha_ano').removeClass('bordes_rojos');
		$('#signup-email').removeClass('bordes_rojos');
		$('#fecha_nacimiento').removeClass('is-visible');

		if(!nombre) {
			error=1;
		}
		if(!apellido) {
			error=1;
		}
		if(!email) {
			error=1;
		}
		if(!contrasena) {
			error=1;
		}
		if(contrasena.length < 6) {
			$('#registro_contrasena_error').toggleClass('is-visible');
			error=1;
		}
		if(nacimiento_dia==0) {
			$('#fecha_dia').addClass('bordes_rojos');
			$('#fecha_nacimiento').toggleClass('is-visible');
			error=1;
		}
		if(nacimiento_mes==0) {
			$('#fecha_mes').addClass('bordes_rojos');
			$('#fecha_nacimiento').toggleClass('is-visible');		
			error=1;		
		}	
		if(nacimiento_ano==0) {
			$('#fecha_ano').addClass('bordes_rojos');
			$('#fecha_nacimiento').toggleClass('is-visible');

			error=1;		
		}	
		if(chequear_email_dato==1) {
			$('#signup-email').addClass('bordes_rojos');
			$('#signup-email').toggleClass('has-error').next('span').toggleClass('is-visible');
			error=1;		
		}		
		if(error==1) {
			return false;		
		} else {
			document.getElementById("signup-boton").disabled = true;
			$('#signup-boton').addClass('boton_trabajando');		
			return true;
		}
	}
	
	function controlar_restablecer_contrasena() {

		var email = document.getElementById("reset-email").value;	

		var error = null;

		if(!email) {
			return false;
		} else {
			return true;
		}

	}
	function restablecer_contrasena() {
		$('#reset-email').removeClass('bordes_rojos');	
		var email = document.getElementById("reset-email").value;	
		document.getElementById("boton_restablecer_contrasena").disabled = true;
		$('#boton_restablecer_contrasena').addClass('boton_trabajando');	
		$.ajax({
			url: "<?php echo $Servidor_url; ?>nuevo/php/restablecer-contrasena-db.php?email="+email,
			success: function (resultado) {
				var resultado = resultado.trim();
				if(resultado=='ok') {
					$('#restablecer_contrase_form').hide();
					$('#restablecer_contrase_ok').show('slow');				
				}else{
					document.getElementById("boton_restablecer_contrasena").disabled = false;
					$('#boton_restablecer_contrasena').removeClass('boton_trabajando');	
					$('#restablecer_contrasena_error').toggleClass('is-visible');
					$('#reset-email').addClass('bordes_rojos');
				}
			}
		});
	}

	function checkear_email() {
		var email = document.getElementById("signup-email").value;	
		$('#signup-email').removeClass('bordes_rojos');
		$('#signup-email').removeClass('bordes_verdes');
		$.ajax({
			url: "<?php echo $Servidor_url; ?>nuevo/php/comprobar-email.php?email="+email,
			success: function (resultado) {
				var resultado = resultado.trim();
				document.getElementById("chequear_email_dato").value = resultado;	
				if(resultado==1) {
					$('#signup-email').addClass('bordes_rojos');
				} else {
					$('#signup-email').addClass('bordes_verdes');
				}
			}
		});
	}

	function mandar_login() {
		var email = document.getElementById("signin-email").value;	
		var contrasena = document.getElementById("signin-password").value;	
		var recordarse = document.getElementById("cd-checkbox-1").checked;	

		document.getElementById("btn_login").disabled = true;
		$('#btn_login').addClass('boton_trabajando');	
		$.ajax({
			url: "<?php echo $Servidor_url; ?>nuevo/php/loguear-usuario-db.php?email="+email+"&contrasena="+contrasena+"&recordarse="+recordarse,
			success: function (resultado) {
				resultado = resultado.trim();
				if(resultado=='error') {
					$('.mensaje_login').toggleClass('is-visible');	
					document.getElementById("btn_login").disabled = false;
					$('#btn_login').removeClass('boton_trabajando');
				}else if(resultado=='ok'){
					window.location.href="<?php echo $Servidor_url;?>aplicacion/";
				}
			}
		});
	}

</script>
