		<div class="cd-popup" id="popup_borrar_item" role="alert">
			<div class="cd-popup-container">
				<p>Are you sure to delete this item?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_borrar_item"><a onclick="">Yes</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div>
		<div class="cd-popup" id="popup_borrar_item2" role="alert">
			<div class="cd-popup-container">
				<p>Are you sure to remove all items?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_borrar_item"><a onclick="confirm_reset_cart()">Yes</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div>