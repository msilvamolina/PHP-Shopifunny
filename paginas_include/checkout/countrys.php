<option value="0" selected="selected" data-name="0">Choose your country</option>

<option value="US" data-name="United States">United States</option>
<option disabled="disabled" data-name="---">---</option>
<option value="AF" data-name="Afghanistan">Afghanistan</option>
<option value="AX" data-name="Aland Islands">Åland Islands</option>
<option value="AL" data-name="Albania">Albania</option>
<option value="DZ" data-name="Algeria">Algeria</option>
<option value="AD" data-name="Andorra">Andorra</option>
<option value="AO" data-name="Angola">Angola</option>
<option value="AI" data-name="Anguilla">Anguilla</option>
<option value="AG" data-name="Antigua And Barbuda">Antigua &amp; Barbuda</option>
<option value="AR" data-name="Argentina">Argentina</option>
<option value="AM" data-name="Armenia">Armenia</option>
<option value="AW" data-name="Aruba">Aruba</option>
<option value="AU" data-name="Australia">Australia</option>
<option value="AT" data-name="Austria">Austria</option>
<option value="AZ" data-name="Azerbaijan">Azerbaijan</option>
<option value="BS" data-name="Bahamas">Bahamas</option>
<option value="BH" data-name="Bahrain">Bahrain</option>
<option value="BD" data-name="Bangladesh">Bangladesh</option>
<option value="BB" data-name="Barbados">Barbados</option>
<option value="BY" data-name="Belarus">Belarus</option>
<option value="BE" data-name="Belgium">Belgium</option>
<option value="BZ" data-name="Belize">Belize</option>
<option value="BJ" data-name="Benin">Benin</option>
<option value="BM" data-name="Bermuda">Bermuda</option>
<option value="BT" data-name="Bhutan">Bhutan</option>
<option value="BO" data-name="Bolivia">Bolivia</option>
<option value="BA" data-name="Bosnia And Herzegovina">Bosnia &amp; Herzegovina</option>
<option value="BW" data-name="Botswana">Botswana</option>
<option value="BV" data-name="Bouvet Island">Bouvet Island</option>
<option value="BR" data-name="Brazil">Brazil</option>
<option value="IO" data-name="British Indian Ocean Territory">British Indian Ocean Territory</option>
<option value="VG" data-name="Virgin Islands, British">British Virgin Islands</option>
<option value="BN" data-name="Brunei">Brunei</option>
<option value="BG" data-name="Bulgaria">Bulgaria</option>
<option value="BF" data-name="Burkina Faso">Burkina Faso</option>
<option value="BI" data-name="Burundi">Burundi</option>
<option value="KH" data-name="Cambodia">Cambodia</option>
<option value="CM" data-name="Republic of Cameroon">Cameroon</option>
<option value="CA" data-name="Canada">Canada</option>
<option value="CV" data-name="Cape Verde">Cape Verde</option>
<option value="KY" data-name="Cayman Islands">Cayman Islands</option>
<option value="CF" data-name="Central African Republic">Central African Republic</option>
<option value="TD" data-name="Chad">Chad</option>
<option value="CL" data-name="Chile">Chile</option>
<option value="CN" data-name="China">China</option>
<option value="CX" data-name="Christmas Island">Christmas Island</option>
<option value="CC" data-name="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option value="CO" data-name="Colombia">Colombia</option>
<option value="KM" data-name="Comoros">Comoros</option>
<option value="CG" data-name="Congo">Congo - Brazzaville</option>
<option value="CD" data-name="Congo, The Democratic Republic Of The">Congo - Kinshasa</option>
<option value="CK" data-name="Cook Islands">Cook Islands</option>
<option value="CR" data-name="Costa Rica">Costa Rica</option>
<option value="HR" data-name="Croatia">Croatia</option>
<option value="CU" data-name="Cuba">Cuba</option>
<option value="CW" data-name="Curaçao">Curaçao</option>
<option value="CY" data-name="Cyprus">Cyprus</option>
<option value="CZ" data-name="Czech Republic">Czech Republic</option>
<option value="CI" data-name="Côte d'Ivoire">Côte d’Ivoire</option>
<option value="DK" data-name="Denmark">Denmark</option>
<option value="DJ" data-name="Djibouti">Djibouti</option>
<option value="DM" data-name="Dominica">Dominica</option>
<option value="DO" data-name="Dominican Republic">Dominican Republic</option>
<option value="EC" data-name="Ecuador">Ecuador</option>
<option value="EG" data-name="Egypt">Egypt</option>
<option value="SV" data-name="El Salvador">El Salvador</option>
<option value="GQ" data-name="Equatorial Guinea">Equatorial Guinea</option>
<option value="ER" data-name="Eritrea">Eritrea</option>
<option value="EE" data-name="Estonia">Estonia</option>
<option value="ET" data-name="Ethiopia">Ethiopia</option>
<option value="FK" data-name="Falkland Islands (Malvinas)">Falkland Islands</option>
<option value="FO" data-name="Faroe Islands">Faroe Islands</option>
<option value="FJ" data-name="Fiji">Fiji</option>
<option value="FI" data-name="Finland">Finland</option>
<option value="FR" data-name="France">France</option>
<option value="GF" data-name="French Guiana">French Guiana</option>
<option value="PF" data-name="French Polynesia">French Polynesia</option>
<option value="TF" data-name="French Southern Territories">French Southern Territories</option>
<option value="GA" data-name="Gabon">Gabon</option>
<option value="GM" data-name="Gambia">Gambia</option>
<option value="GE" data-name="Georgia">Georgia</option>
<option value="DE" data-name="Germany">Germany</option>
<option value="GH" data-name="Ghana">Ghana</option>
<option value="GI" data-name="Gibraltar">Gibraltar</option>
<option value="GR" data-name="Greece">Greece</option>
<option value="GL" data-name="Greenland">Greenland</option>
<option value="GD" data-name="Grenada">Grenada</option>
<option value="GP" data-name="Guadeloupe">Guadeloupe</option>
<option value="GT" data-name="Guatemala">Guatemala</option>
<option value="GG" data-name="Guernsey">Guernsey</option>
<option value="GN" data-name="Guinea">Guinea</option>
<option value="GW" data-name="Guinea Bissau">Guinea-Bissau</option>
<option value="GY" data-name="Guyana">Guyana</option>
<option value="HT" data-name="Haiti">Haiti</option>
<option value="HM" data-name="Heard Island And Mcdonald Islands">Heard &amp; McDonald Islands</option>
<option value="HN" data-name="Honduras">Honduras</option>
<option value="HK" data-name="Hong Kong">Hong Kong SAR China</option>
<option value="HU" data-name="Hungary">Hungary</option>
<option value="IS" data-name="Iceland">Iceland</option>
<option value="IN" data-name="India">India</option>
<option value="ID" data-name="Indonesia">Indonesia</option>
<option value="IR" data-name="Iran, Islamic Republic Of">Iran</option>
<option value="IQ" data-name="Iraq">Iraq</option>
<option value="IE" data-name="Ireland">Ireland</option>
<option value="IM" data-name="Isle Of Man">Isle of Man</option>
<option value="IL" data-name="Israel">Israel</option>
<option value="IT" data-name="Italy">Italy</option>
<option value="JM" data-name="Jamaica">Jamaica</option>
<option value="JP" data-name="Japan">Japan</option>
<option value="JE" data-name="Jersey">Jersey</option>
<option value="JO" data-name="Jordan">Jordan</option>
<option value="KZ" data-name="Kazakhstan">Kazakhstan</option>
<option value="KE" data-name="Kenya">Kenya</option>
<option value="KI" data-name="Kiribati">Kiribati</option>
<option value="XK" data-name="Kosovo">Kosovo</option>
<option value="KW" data-name="Kuwait">Kuwait</option>
<option value="KG" data-name="Kyrgyzstan">Kyrgyzstan</option>
<option value="LA" data-name="Lao People's Democratic Republic">Laos</option>
<option value="LV" data-name="Latvia">Latvia</option>
<option value="LB" data-name="Lebanon">Lebanon</option>
<option value="LS" data-name="Lesotho">Lesotho</option>
<option value="LR" data-name="Liberia">Liberia</option>
<option value="LY" data-name="Libyan Arab Jamahiriya">Libya</option>
<option value="LI" data-name="Liechtenstein">Liechtenstein</option>
<option value="LT" data-name="Lithuania">Lithuania</option>
<option value="LU" data-name="Luxembourg">Luxembourg</option>
<option value="MO" data-name="Macao">Macau SAR China</option>
<option value="MK" data-name="Macedonia, Republic Of">Macedonia</option>
<option value="MG" data-name="Madagascar">Madagascar</option>
<option value="MW" data-name="Malawi">Malawi</option>
<option value="MY" data-name="Malaysia">Malaysia</option>
<option value="MV" data-name="Maldives">Maldives</option>
<option value="ML" data-name="Mali">Mali</option>
<option value="MT" data-name="Malta">Malta</option>
<option value="MQ" data-name="Martinique">Martinique</option>
<option value="MR" data-name="Mauritania">Mauritania</option>
<option value="MU" data-name="Mauritius">Mauritius</option>
<option value="YT" data-name="Mayotte">Mayotte</option>
<option value="MX" data-name="Mexico">Mexico</option>
<option value="MD" data-name="Moldova, Republic of">Moldova</option>
<option value="MC" data-name="Monaco">Monaco</option>
<option value="MN" data-name="Mongolia">Mongolia</option>
<option value="ME" data-name="Montenegro">Montenegro</option>
<option value="MS" data-name="Montserrat">Montserrat</option>
<option value="MA" data-name="Morocco">Morocco</option>
<option value="MZ" data-name="Mozambique">Mozambique</option>
<option value="MM" data-name="Myanmar">Myanmar (Burma)</option>
<option value="NA" data-name="Namibia">Namibia</option>
<option value="NR" data-name="Nauru">Nauru</option>
<option value="NP" data-name="Nepal">Nepal</option>
<option value="NL" data-name="Netherlands">Netherlands</option>
<option value="AN" data-name="Netherlands Antilles">Netherlands Antilles</option>
<option value="NC" data-name="New Caledonia">New Caledonia</option>
<option value="NZ" data-name="New Zealand">New Zealand</option>
<option value="NI" data-name="Nicaragua">Nicaragua</option>
<option value="NE" data-name="Niger">Niger</option>
<option value="NG" data-name="Nigeria">Nigeria</option>
<option value="NU" data-name="Niue">Niue</option>
<option value="NF" data-name="Norfolk Island">Norfolk Island</option>
<option value="KP" data-name="Korea, Democratic People's Republic Of">North Korea</option>
<option value="NO" data-name="Norway">Norway</option>
<option value="OM" data-name="Oman">Oman</option>
<option value="PK" data-name="Pakistan">Pakistan</option>
<option value="PS" data-name="Palestinian Territory, Occupied">Palestinian Territories</option>
<option value="PA" data-name="Panama">Panama</option>
<option value="PG" data-name="Papua New Guinea">Papua New Guinea</option>
<option value="PY" data-name="Paraguay">Paraguay</option>
<option value="PE" data-name="Peru">Peru</option>
<option value="PH" data-name="Philippines">Philippines</option>
<option value="PN" data-name="Pitcairn">Pitcairn Islands</option>
<option value="PL" data-name="Poland">Poland</option>
<option value="PT" data-name="Portugal">Portugal</option>
<option value="QA" data-name="Qatar">Qatar</option>
<option value="RE" data-name="Reunion">Réunion</option>
<option value="RO" data-name="Romania">Romania</option>
<option value="RU" data-name="Russia">Russia</option>
<option value="RW" data-name="Rwanda">Rwanda</option>
<option value="SX" data-name="Sint Maarten">Saint Martin</option>
<option value="WS" data-name="Samoa">Samoa</option>
<option value="SM" data-name="San Marino">San Marino</option>
<option value="ST" data-name="Sao Tome And Principe">São Tomé &amp; Príncipe</option>
<option value="SA" data-name="Saudi Arabia">Saudi Arabia</option>
<option value="SN" data-name="Senegal">Senegal</option>
<option value="RS" data-name="Serbia">Serbia</option>
<option value="SC" data-name="Seychelles">Seychelles</option>
<option value="SL" data-name="Sierra Leone">Sierra Leone</option>
<option value="SG" data-name="Singapore">Singapore</option>
<option value="SK" data-name="Slovakia">Slovakia</option>
<option value="SI" data-name="Slovenia">Slovenia</option>
<option value="SB" data-name="Solomon Islands">Solomon Islands</option>
<option value="SO" data-name="Somalia">Somalia</option>
<option value="ZA" data-name="South Africa">South Africa</option>
<option value="GS" data-name="South Georgia And The South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
<option value="KR" data-name="South Korea">South Korea</option>
<option value="SS" data-name="South Sudan">South Sudan</option>
<option value="ES" data-name="Spain">Spain</option>
<option value="LK" data-name="Sri Lanka">Sri Lanka</option>
<option value="BL" data-name="Saint Barthélemy">St. Barthélemy</option>
<option value="SH" data-name="Saint Helena">St. Helena</option>
<option value="KN" data-name="Saint Kitts And Nevis">St. Kitts &amp; Nevis</option>
<option value="LC" data-name="Saint Lucia">St. Lucia</option>
<option value="MF" data-name="Saint Martin">St. Martin</option>
<option value="PM" data-name="Saint Pierre And Miquelon">St. Pierre &amp; Miquelon</option>
<option value="VC" data-name="St. Vincent">St. Vincent &amp; Grenadines</option>
<option value="SD" data-name="Sudan">Sudan</option>
<option value="SR" data-name="Suriname">Suriname</option>
<option value="SJ" data-name="Svalbard And Jan Mayen">Svalbard &amp; Jan Mayen</option>
<option value="SZ" data-name="Swaziland">Swaziland</option>
<option value="SE" data-name="Sweden">Sweden</option>
<option value="CH" data-name="Switzerland">Switzerland</option>
<option value="SY" data-name="Syria">Syria</option>
<option value="TW" data-name="Taiwan">Taiwan</option>
<option value="TJ" data-name="Tajikistan">Tajikistan</option>
<option value="TZ" data-name="Tanzania, United Republic Of">Tanzania</option>
<option value="TH" data-name="Thailand">Thailand</option>
<option value="TL" data-name="Timor Leste">Timor-Leste</option>
<option value="TG" data-name="Togo">Togo</option>
<option value="TK" data-name="Tokelau">Tokelau</option>
<option value="TO" data-name="Tonga">Tonga</option>
<option value="TT" data-name="Trinidad and Tobago">Trinidad &amp; Tobago</option>
<option value="TN" data-name="Tunisia">Tunisia</option>
<option value="TR" data-name="Turkey">Turkey</option>
<option value="TM" data-name="Turkmenistan">Turkmenistan</option>
<option value="TC" data-name="Turks and Caicos Islands">Turks &amp; Caicos Islands</option>
<option value="TV" data-name="Tuvalu">Tuvalu</option>
<option value="UM" data-name="United States Minor Outlying Islands">U.S. Outlying Islands</option>
<option value="UG" data-name="Uganda">Uganda</option>
<option value="UA" data-name="Ukraine">Ukraine</option>
<option value="AE" data-name="United Arab Emirates">United Arab Emirates</option>
<option value="GB" data-name="United Kingdom">United Kingdom</option>
<option value="US" data-name="United States">United States</option>
<option value="UY" data-name="Uruguay">Uruguay</option>
<option value="UZ" data-name="Uzbekistan">Uzbekistan</option>
<option value="VU" data-name="Vanuatu">Vanuatu</option>
<option value="VA" data-name="Holy See (Vatican City State)">Vatican City</option>
<option value="VE" data-name="Venezuela">Venezuela</option>
<option value="VN" data-name="Vietnam">Vietnam</option>
<option value="WF" data-name="Wallis And Futuna">Wallis &amp; Futuna</option>
<option value="EH" data-name="Western Sahara">Western Sahara</option>
<option value="YE" data-name="Yemen">Yemen</option>
<option value="ZM" data-name="Zambia">Zambia</option>
<option value="ZW" data-name="Zimbabwe">Zimbabwe</option>