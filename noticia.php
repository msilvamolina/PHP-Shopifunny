<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

$nombre = trim($_GET['nombre']);

if(!$nombre) {
	redireccionar_404();
}

conectar2('shopifun', "admin");
//consultar en la base de datos
$query_rs_producto = "SELECT noticia_titulo, noticia_fondo, id_noticia, noticia_url FROM noticias WHERE noticia_url = '$nombre' OR id_noticia = '$nombre' LIMIT 1";
$rs_producto = mysql_query($query_rs_producto)or die(mysql_error());
$row_rs_producto = mysql_fetch_assoc($rs_producto);
$totalrow_rs_producto = mysql_num_rows($rs_producto);

if(!$totalrow_rs_producto) {
	redireccionar_404();
}

$url_actual = $Servidor_url2.$_SERVER['REQUEST_URI'];

$noticia = $row_rs_producto['id_noticia'];
$noticia_url = $row_rs_producto['noticia_url'];
$noticia_titulo = $row_rs_producto['noticia_titulo'];
$noticia_fondo = $row_rs_producto['noticia_fondo'];

$url_ideal = $Servidor_url.'n/'.$noticia_url.'/';

if($url_actual!=$url_ideal) {
	header('location: '.$url_ideal);
	exit; 
}

//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, cuerpo_tipo, contenido FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);


desconectar();
$titulo_pagina = "Shopifunny   &raquo; ".$noticia_titulo;

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php
	//Permisos
	$agregar_slick = 1;

	include('paginas_include/estructura/head.php'); ?>
	
	<?php include('paginas_include/estructura/google-tag-manager.php'); ?>

	<style>
	.contenedor_noticia {
		margin-bottom: 20px;
		padding: 30px;
	}
	.titulo_pagina {
		font-weight: bold;
		font-size: 32px;
	}

	.contenedor_elementos {
		width: 100%;
		max-width: 600px;
		padding: 40px;
		padding-top: 15px;
		margin: 0 auto;
		background: #fff;
		-webkit-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);	
	}
</style>
</head>
<body >
	
	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
	<main class="cd-main-content">
		<div class="contenedor_noticia fondo<?php echo $noticia_fondo; ?>">

			<div class="contenedor_elementos">
				<?php do {
					$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
					$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
					$contenido = $row_rs_cuerpo['contenido'];
					$orden = $row_rs_cuerpo['orden'];

					if($cuerpo_tipo == 'imagen') { 
						$imagen = $array_foto[$contenido];
						?>
						<img src="<?php echo $ruta_imagenes_recortes.$imagen; ?>" class="contenido_imagen" alt="">
						<?php } elseif($tipo='texto') {
							$contenido = arreglar_datos_db_al_reves($contenido);
							?>
							<div class="contenido_texto"><?php echo $contenido;  ?></div>
							<?php } ?>

							<?php } while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo)); ?>
						</div>
					</div>
				</main>
				<?php include('paginas_include/estructura/pie.php') ; ?>

				<?php include('paginas_include/estructura/javascript-pie.php');?>

				<?php include('paginas_include/estructura/javascript-pie2.php');?>	

			</body>
			</html>