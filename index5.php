<?php include('paginas_include/variables-generales.php');



$panelpromo = 0;
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $Servidor_url;?>apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Servidor_url;?>apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Servidor_url;?>apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Servidor_url;?>apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $Servidor_url;?>apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $Servidor_url;?>apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $Servidor_url;?>apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $Servidor_url;?>apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo $Servidor_url;?>favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style2.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/style3.css?v=8"> <!-- Resource style -->

	<script src="<?php echo $Servidor_url;?>js/modernizr.js"></script> <!-- Modernizr -->

	<title>Shopifunny - Shopping is funny with us!</title>

	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>


	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>

	<link href="<?php echo $Servidor_url; ?>css/estilohome.css?v=5" rel='stylesheet' type='text/css'>

	<link href="<?php echo $Servidor_url; ?>css/productos-grilla.css?v=1" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/font-awesome/css/font-awesome.min.css" />


	<style type="text/css">
	
	body {
		background-color: #eaeaea;


	} 
	.cd-main-content {
		background: transparent !important ;

	} 

	.ecabezado_principal {
	}

	.oferta_main {
		height: 423px;
		background: #e31b1e url('https://www.shopifunny.com/img/fondoweb.jpg') no-repeat center center;
	}

	.oferta_main img {
		height: 423px;
		width: 365px;
	}

	.cd-logo {
		position: absolute;
		top: 0px;
		left: 0px;
	}
	.cd-logo img {
		display: block;
		width: 216px;
	}
	.cd-main-content {
		margin-top: 0px;
	}
	

	.banner_principal {
		width: 100%;
		height: 80px;
		/* fallback */
		background-color: #3F51B5;
		background-repeat: repeat-y;
		/* Safari 4-5, Chrome 1-9 */
		background: -webkit-gradient(linear, left top, right top, from(#1976D2), to(#3F51B5));
		/* Safari 5.1, Chrome 10+ */
		background: -webkit-linear-gradient(top, #1976D2, #3F51B5);
		/* Firefox 3.6+ */
		background: -moz-linear-gradient(top, #1976D2, #3F51B5);
		/* IE 10 */
		background: -ms-linear-gradient(top, #1976D2, #3F51B5);
		/* Opera 11.10+ */
		background: -o-linear-gradient(top, #1976D2, #3F51B5);

	}

	.nuevo_header {
		background: #fff;
		height: 100px;
		width: 100%;

	}

	.logo_nuevo {
		width: 220px;
		float: left;

	}

	.barra_header {
		background: #f6f6f6;
		height: 35px;
		border-bottom: 1px solid #e4e4e4;
		width: 100%;
	}

	.form_busqueda {
		float: left;
	}
</style>
</head>
<body  onload="countDown();">
	
	<div class="banner_principal"></div>
	<header class="cd-main-header animate-search">
		<div class="cd-logo"><a href="#0"><img src="<?php echo $Servidor_url; ?>img/logo_nuevo.png" alt="Logo" class="logo"></a></div>

		<nav class="cd-main-nav-wrapper">
			<a href="#search" class="cd-search-trigger cd-text-replace">Buscar</a>
			
			<ul class="cd-main-nav">
				<li><a href="#0">Servicios</a></li>
				<li><a href="#0">Negocios</a></li>
				<li><a href="#0">Blog</a></li>
				<li><a href="#0">Contacto</a></li>
			</ul> <!-- .cd-main-nav -->
		</nav> <!-- .cd-main-nav-wrapper -->

		<a href="#0" class="cd-nav-trigger cd-text-replace">Menu<span></span></a>
	</header>
	

	<main class="cd-main-content">
		<div class="ecabezado_principal">
			<?php include('paginas_include/home/masterslider.php'); ?>
			
			<div class="oferta_main">
				<img src="<?php echo $Servidor_url; ?>img/oferta.jpg" alt="">
			</div>
		</div>

		<img src="<?php echo $Servidor_url; ?>img/banner.jpg" class="banner" alt="">

		<div id="contador"></div>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelproductos.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

		<?php include('paginas_include/home/panelpromo.php'); ?>

	</main>


	<div id="search" class="cd-main-search">
		<form>
			<input type="search" placeholder="Buscar...">

			<div class="cd-select">
				<span>in</span>
				<select name="select-category">
					<option value="all-categories">all Categories</option>
					<option value="category1">Category 1</option>
					<option value="category2">Category 2</option>
					<option value="category3">Category 3</option>
				</select>
				<span class="selected-value">all Categories</span>
			</div>
		</form>

		<div class="cd-search-suggestions">
			<div class="news">
				<h3>Recommendations for you</h3>
				<ul>
					<li>
						<a class="image-wrapper" href="#0"><img src="<?php echo $Servidor_url; ?>img/placeholder.png" alt="News image"></a>
						<h4><a class="cd-nowrap" href="#0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h4>
						<time datetime="2016-01-12">Feb 03, 2016</time>
					</li>

					<li>
						<a class="image-wrapper" href="#0"><img src="<?php echo $Servidor_url; ?>img/placeholder.png" alt="News image"></a>
						<h4><a class="cd-nowrap" href="#0">Incidunt voluptatem adipisci voluptates fugit beatae culpa eum, distinctio, assumenda est ad</a></h4>
						<time datetime="2016-01-12">Jan 28, 2016</time>
					</li>

					<li>
						<a class="image-wrapper" href="#0"><img src="<?php echo $Servidor_url; ?>img/placeholder.png" alt="News image"></a>
						<h4><a class="cd-nowrap" href="#0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, esse.</a></h4>
						<time datetime="2016-01-12">Jan 12, 2016</time>
					</li>
				</ul>
			</div> <!-- .news -->

			<div class="quick-links">
				<h3>Quick Links</h3>
				<ul>
					<li><a href="#0">Find a store</a></li>
					<li><a href="#0">Accessories</a></li>
					<li><a href="#0">Warranty info</a></li>
					<li><a href="#0">Support</a></li>
					<li><a href="#0">Contact us</a></li>
				</ul>
			</div> <!-- .quick-links -->
		</div> <!-- .cd-search-suggestions -->

		<a href="#0" class="close cd-text-replace">Close Form</a>
	</div> <!-- .cd-main-search -->
	<script src="<?php echo $Servidor_url;?>js/jquery-2.1.1.js"></script>
	<script src="<?php echo $Servidor_url;?>js/jquery.mobile.custom.min.js"></script>
	<script src="<?php echo $Servidor_url;?>js/main3.js"></script> <!-- Resource jQuery -->

	<script type="text/javascript">		

		var slider = new MasterSlider();

		slider.control('arrows' ,{insertTo:'#masterslider'});	
		slider.control('bullets');	

		slider.setup('masterslider' , {
			width:1280,
			height:600,
			autoplay: true,
			speed:20
		});


		function mostrar_producto(producto) {
			$('#producto_nombre_'+producto).show(); 
		}
		function ocultar_producto(producto) {
			$('#producto_nombre_'+producto).hide(); 
		}
	</script>
</body>
</html>