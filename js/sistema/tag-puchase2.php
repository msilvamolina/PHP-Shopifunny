<?php 

$codigo_compra = trim($get_codigo_compra);


conectar2('shopifun', "compras");

//consultar en la base de datos
$query_rs_compra_segundo_paso = "SELECT compras_segundo_paso.compra_email, compras_segundo_paso.fecha_creacion, direcciones.direccion_first_name, direcciones.direccion_last_name, direcciones.direccion_address, direcciones.direccion_address_2, direcciones.direccion_city, direcciones.direccion_country, direcciones.direccion_state, direcciones.direccion_zip, direcciones.direccion_phone  FROM compras_segundo_paso, direcciones WHERE compras_segundo_paso.compra_codigo = '$codigo_compra' AND compras_segundo_paso.compra_direccion = direcciones.id_direccion ORDER BY compras_segundo_paso.id_compra DESC LIMIT 1";
$rs_compra_segundo_paso = mysql_query($query_rs_compra_segundo_paso)or die(mysql_error());
$row_rs_compra_segundo_paso = mysql_fetch_assoc($rs_compra_segundo_paso);
$totalrow_rs_compra_segundo_paso = mysql_num_rows($rs_compra_segundo_paso);


//consultar en la base de datos
$query_rs_compras = "SELECT id_compra, id_producto, cantidad FROM compras_primer_paso WHERE codigo_compra = '$codigo_compra' ORDER BY id_compra DESC";
$rs_compras = mysql_query($query_rs_compras)or die(mysql_error());
$row_rs_compras = mysql_fetch_assoc($rs_compras);
$totalrow_rs_compras = mysql_num_rows($rs_compras);


$lista_producto = NULL;
do {
  $id_compra = $row_rs_compras['id_compra'];
  $id_producto = $row_rs_compras['id_producto'];
  $cantidad = $row_rs_compras['cantidad'];

  if(!$lista_producto) {
    $lista_producto = 'WHERE id_producto = '.$id_producto;
  } else {
    $lista_producto .= ' OR id_producto = '.$id_producto;
  }

  $compra_lista[$id_compra] = $id_producto;
  $compra_lista_cantidad[$id_compra] = $cantidad;
} while ($row_rs_compras = mysql_fetch_assoc($rs_compras));

$fecha_actual = date('Y-m-d H:i:s');
  //consultar en la base de datos
$query_rs_consultar_codigo_descuento = "SELECT id_cupon, cupon_codigo,cupon_descuento FROM cupones_descuento WHERE cupon_codigo_compra = '$codigo_compra'";
$rs_consultar_codigo_descuento = mysql_query($query_rs_consultar_codigo_descuento)or die(mysql_error());
$row_rs_consultar_codigo_descuento = mysql_fetch_assoc($rs_consultar_codigo_descuento);
$totalrow_rs_consultar_codigo_descuento = mysql_num_rows($rs_consultar_codigo_descuento);

$precio_descuento_porcentaje = 0;

if($totalrow_rs_consultar_codigo_descuento) {
  $cupon_codigo = $row_rs_consultar_codigo_descuento['cupon_codigo'];
  $id_cupon_descuento = $row_rs_consultar_codigo_descuento['id_cupon'];
  $precio_descuento_porcentaje = $row_rs_consultar_codigo_descuento['cupon_descuento'];
}
desconectar();

if($lista_producto) {
  conectar2('shopifun', "admin");
//consultar en la base de datos
  $query_rs_productos = "SELECT id_producto, producto_categoria, producto_titulo, producto_precio_dolar, producto_precio_shipping_dolar  FROM productos $WHERE";
  $rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
  $row_rs_productos = mysql_fetch_assoc($rs_productos);
  $totalrow_rs_productos = mysql_num_rows($rs_productos);

//consultar en la base de datos
  $query_rs_categorias = "SELECT id_categoria, categoria_nombre FROM categorias ";
  $rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
  $row_rs_categorias = mysql_fetch_assoc($rs_categorias);
  $totalrow_rs_categorias = mysql_num_rows($rs_categorias);

  do {
    $id_categoria = $row_rs_categorias['id_categoria'];
    $categoria_nombre = $row_rs_categorias['categoria_nombre'];

    $array_categoria[$id_categoria] = $categoria_nombre;
  } while (  $row_rs_categorias = mysql_fetch_assoc($rs_categorias));
  desconectar();
}

do {
  $id_producto = $row_rs_productos['id_producto'];
  $producto_titulo = $row_rs_productos['producto_titulo'];
  $producto_precio_dolar = $row_rs_productos['producto_precio_dolar'];
  $producto_precio_shipping_dolar = $row_rs_productos['producto_precio_shipping_dolar'];
  $producto_categoria = $row_rs_productos['producto_categoria'];

  $array_producto[$id_producto] = $producto_titulo;
  $array_producto_precio[$id_producto] = $producto_precio_dolar;
  $array_producto_shipping[$id_producto] = $producto_precio_shipping_dolar;
  $array_producto_categoria[$id_producto] = $producto_categoria;

} while ($row_rs_productos = mysql_fetch_assoc($rs_productos));

$precio_total = 0;
$shipping = 0;

$descuento = 0;

foreach ($compra_lista as $id_compra => $id_producto) {
  $producto_precio_dolar = $array_producto_precio[$id_producto];
  $producto_shipping = $array_producto_shipping[$id_producto];

  $precio_total = $precio_total + ($producto_precio_dolar * $cantidad);
  $shipping = $shipping + ($producto_shipping * $cantidad); 
}

if($precio_descuento_porcentaje) {
  $precio_final = $precio_total + $shipping;
  $descuento = ($precio_descuento_porcentaje * $precio_final) / 100;
}
if($descuento != 0) {
  $precio_total = $precio_total - $descuento;
}

$precio_total = round($precio_total, 2);
?>
<script>
// Send transaction data with a pageview if available
// when the page loads. Otherwise, use an event when the transaction
// data becomes available.
dataLayer.push({
  'ecommerce': {
    'purchase': {
      'actionField': {
            'id': '<?php echo $codigo_compra; ?>',                         // Transaction ID. Required for purchases and refunds.
            'affiliation': 'Shopifunny',
            'revenue': '<?php echo $precio_total + $shipping; ?>',                     // Total transaction value (incl. tax and shipping)
            'shipping': '<?php echo $shipping; ?>',
            'coupon': '<?php echo $cupon_codigo; ?>'
          },
          'products': [{           
            <?php 
            $i=1;
            $total_productos = count($compra_lista);
            foreach ($compra_lista as $id_compra => $id_producto) {
              $producto_titulo = $array_producto[$id_producto];
              $producto_precio_dolar = $array_producto_precio[$id_producto];
              $producto_shipping = $array_producto_shipping[$id_producto];
              $producto_categoria = $array_producto_categoria[$id_producto];

              $cantidad = $compra_lista_cantidad[$id_compra]; 

              $categoria = $array_categoria[$producto_categoria];

              $llave_coma = '},';

              if($i==$total_productos) {
                $llave_coma = '}';
              }?>

              'name': '<?php echo $producto_titulo; ?>',
              'id': '<?php echo $id_producto; ?>',
              'price': '<?php echo $producto_precio_dolar; ?>',
              'category': '<?php echo $categoria; ?>',
              'quantity': <?php echo $cantidad; ?>
              <?php echo $llave_coma; ?> 
              <?php $i++;} ?>
              ]
            }
          },
          event: 'gtm4wp.orderCompleted'
        });
      </script>