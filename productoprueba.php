<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');
?>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="<?php echo $Servidor_url;?>js/sticky-kit.js"></script>
<script src="<?php echo $Servidor_url;?>js/imagenSticky.js"></script>

<style>
body {
	margin: 0;
	font-family: sans-serif;
	font-size: 16px; }

	h1 {
		font-size: 30px;
		margin: 10px; }

		.content {
			overflow: hidden; }
			.content.right .sidebar {
				float: right;
				margin: 10px;
				margin-left: 0; }
				.content.right .main {
					margin: 10px;
					margin-right: 220px; }
					.content.double .main {
						margin-left: 434px; }
						.content .sidebar {
							width: 200px;
							height: 66px;
							margin: 10px;
							margin-right: 0;
							border: 1px solid red;
							float: left;
							overflow: hidden;
							font-family: sans-serif; }
							.content .sidebar.alt {
								height: 133px; }
								.content .sidebar.tall {
									height: 400px; }
									.content .sidebar.medium {
										height: 300px; }
										.content .sidebar.flat {
											border: 0;
											height: auto; }
											.content .inner {
												border: 1px solid red;
												height: 66px;
												margin: 10px 0; }
												.content .inner.static {
													margin-top: 0;
													border: 1px solid blue; }
													.content .item {
														display: inline-block;
														vertical-align: top;
														width: 120px;
														border: 1px solid blue;
														font-size: 16px;
														margin: 10px;
														overflow: hidden; }
														.content .item.sticky {
															border: 1px solid red;
															height: 100px; }
															.content .inline_columns {
																font-size: 0; }
																.content .main {
																	margin: 10px;
																	margin-left: 222px;
																	border: 1px solid blue;
																	height: auto;
																	overflow: hidden; }
																	.content .main.short {
																		height: auto; }
																		.content .main.tall {
																			height: auto; }

																			.footer {
																				margin: 10px;
																				text-align: center;
																				font-size: 13px;
																				border-top: 1px dashed #dadada;
																				color: #666;
																				padding-top: 10px;
																				min-height: 133px; }

																				.sub {
																					color: #999; }

																					@media all and (max-width: 500px) {
																						.content .sidebar {
																							width: 100px; }
																							.content .item {
																								width: 60px; }
																								.content .main {
																									margin-left: 122px; }
																									.content.double .main {
																										margin-left: 234px; }
																										.content.right .main {
																											margin-right: 120px; } }
																										</style>
																									</head>
																									<body >

																										<h1>My Site</h1>
																										<div class="content" data-sticky_parent>
																											<div class="sidebar" data-sticky_column>
																												This is a sticky column
																											</div>

																											<div class="main" data-sticky_column>
																												This is the main column
																												<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
				<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																																<p class="sub">
																													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus id
																													leo et aliquam. Proin consectetur ligula vel neque cursus laoreet. Nullam
																													dignissim, augue at consectetur pellentesque, metus ipsum interdum
																													sapien, quis ornare quam enim vel ipsum.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																											</div>
																										</div>
																										<div class="footer">
																											My very tall footer!

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>

																												<p class="sub">
																													In congue nunc vitae magna
																													tempor ultrices. Cras ultricies posuere elit. Nullam ultrices purus ante,
																													at mattis leo placerat ac. Nunc faucibus ligula nec lorem sodales
																													venenatis. Curabitur nec est condimentum, blandit tellus nec, semper
																													arcu. Nullam in porta ipsum, non consectetur mi. Sed pharetra sapien
																													nisl. Aliquam ac lectus sed elit vehicula scelerisque ut vel sem. Ut ut
																													semper nisl.
																												</span>

																												<p class="sub">
																													Curabitur rhoncus, arcu at placerat volutpat, felis elit sollicitudin ante, sed
																													tempus justo nibh sed massa. Integer vestibulum non ante ornare eleifend. In
																													vel mollis dolor.
																												</p>
																										
																										</div>


																									</body>
																									</html>