<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

include('paginas_include/home/home.php');

$get_codigo_compra= trim($_GET['compra']);

$error_paypal= trim($_GET['error_paypal']);

$paypalapproved= trim($_GET['paypalapproved']);

$paypalcancelled = trim($_GET['paypalcancelled']);

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta property="og:url" content="<?php echo $Servidor_url;?>">
	<meta name="description" content="Shopping is funny with us!">
	<meta name="og:title" content="Shopifunny"/>
	<meta name="twitter:title" content="Shopifunny" />
	<meta itemprop="name" content="Shopifunny" />
	<meta name="og:site_name" content="shopifunny.com"/>
	<meta name="og:description" content="Shopping is funny with us!"/>
	<meta itemprop="description" content="Shopping is funny with us!"/>
	<meta name="author" content="Shopifunny Team :D" />
	<meta name="rating" content="General" />
	<meta name="distribution" content="global" />
	<meta name="robots" content="index,follow,noodp" />
	<meta name="twitter:card" content="photo" />
	<meta name="twitter:site" content="@shopifunny" />
	<meta name="twitter:creator" content="@shopifunny" />
	<meta name="twitter:domain" content="shopifunny.com" />    
	<link rel="image_src" type="image/png" href="<?php echo $Servidor_url;?>img/og_shopi.png">
	<meta property="og:image" content="<?php echo $Servidor_url;?>img/og_shopi.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="500" />
	<meta property="og:image:height" content="500" />
	<?php
	//Permisos
	$agregar_masterslider = 1;
	$agregar_slick = 1;

	include('paginas_include/estructura/head.php'); ?>
	
	<?php if ($paypalapproved AND $get_codigo_compra) {
		include('js/sistema/tag-puchase2.php'); 
	} ?>
	<?php include('paginas_include/estructura/google-tag-manager.php'); ?>
	<style>
	.elemento_panel {
		background: #fff;
		padding: 20px;
		margin-bottom: 15px;
		text-align: center;
	}

	.elemento_panel p {
		width: 100%;
		max-width: 600px;
		margin: 0 auto;
	}

	.elemento_panel b {
		width: 100%;
		max-width: 600px;
		margin: 0 auto;
	}

	.element_panel_delete {
		position: absolute;
		right: 0;
		margin-right: 20px;
		margin-top: -10px;
	}

	.element_panel_delete img{
		width: 30px;
	}

	.element_panel_delete a{
		cursor: pointer;
	}
</style>
</head>
<body >
	
	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
	<main class="cd-main-content">
		<?php include('paginas_include/estructura/main.php') ;?>
		<?php if($error_paypal) { 
			include('paginas_include/home/paypalerror.php');
		} ?>
		<?php include('paginas_include/estructura/main.php') ;?>
		<?php if($paypalapproved) { 
			include('paginas_include/home/paypalapproved.php');
		} ?>
		<?php if($paypalcancelled) { 
			include('paginas_include/home/paypalcancelled.php');
		} ?>
		<?php if($home_banner_top) { ?>
		<div class="banner_principal fondo<?php echo $banner_top_banner_fondo; ?>" >
			<a href="<?php echo $banner_lateral_banner_link; ?>" target="<?php echo $banner_lateral_banner_target; ?>">
				<img src="<?php echo $ruta_imagenes_banner.$banner_top_banner_imagen; ?>" class="banner_top" alt="<?php echo $banner_top_banner_nombre; ?>" title="<?php echo $banner_top_banner_nombre; ?>">
			</a>
		</div>
		<?php } ?>
		<div class="ecabezado_principal">
			<!-- template -->
			<div class="ms-fullscreen-template " id="slider1-wrapper">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider">
					<?php $i=1; do { 
						$nombre_foto = $row_rs_home_slider['nombre_foto'];
						$recorte_foto_nombre = $row_rs_home_slider['recorte_foto_nombre'];
						$link = $row_rs_home_slider['link'];
						$target = $row_rs_home_slider['target'];

						$foto = $nombre_foto;

						if($recorte_foto_nombre) {
							$foto = 'recortes/'.$recorte_foto_nombre;
						}

						$data_link = null;
						if($link) {
							$data_link = 'data-href="'.$link.'"';
						}
						?>
						<div <?php echo $data_link; ?> class="ms-slide slide-<?php echo $i; ?>">
							<img style="cursor:pointer" src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/blank.gif" data-src="<?php echo $ruta_imagenes_home_slider.$foto; ?>"/> 
						</div>

						<?php $i++; } while ($row_rs_home_slider = mysql_fetch_assoc($rs_home_slider)); ?>
					</div>
				</div>
				<!-- end of masterslider -->
				<!-- end of template -->			
				<?php if($home_banner_lateral) { ?>
				<div class="oferta_main fondo<?php echo $banner_lateral_banner_fondo; ?>">
					<a href="<?php echo $banner_lateral_banner_link; ?>" target="<?php echo $banner_lateral_banner_target; ?>">
						<img src="<?php echo $ruta_imagenes_banner.$banner_lateral_banner_imagen; ?>" alt="<?php echo $banner_lateral_banner_nombre; ?>" title="<?php echo $banner_lateral_banner_nombre; ?>">
					</a>
				</div>
				<?php } ?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<?php if($totalrow_rs_home_cuerpo) { 
				foreach ($array_cuerpo as $id_cuerpo => $contenido) {
					$cuerpo_tipo = $array_cuerpo_tipo[$id_cuerpo]; ?>
					<div class="elemento_contenedor elemento_<?php echo $cuerpo_tipo; ?>" id="elemento<?php echo $id_cuerpo; ?>">
						<div class="contenedor_loader">
							<img src="<?php echo $ruta_img_loader; ?>" class="elemento_loader" alt="">
						</div>
					</div>
					<?php } } ?>
					<br>
				</main>
				<?php include('paginas_include/estructura/pie.php') ; ?>

				<?php include('paginas_include/estructura/javascript-pie.php');?>
				
				<?php if($paypalapproved) { ?>
				<script src="<?php echo $Servidor_url; ?>js/sistema/tag-puchase.js?c=<?php echo $get_codigo_compra; ?>"></script>
				<?php } ?>
				<script type="text/javascript">		
					<?php foreach ($array_cuerpo as $id_cuerpo => $value) { ?>
						cargar_elemento(<?php echo $id_cuerpo; ?>);
						<?php } ?>

						var slider = new MasterSlider();

						slider.control('arrows' ,{insertTo:'#masterslider'});	
						slider.control('bullets', {autohide:false});	

						slider.setup('masterslider' , {
							width:1280,
							height:600,
							autoplay: true,
							loop: true,
							speed:10
						});
						function mostrar_producto(producto) {
							$('#producto_nombre_'+producto).show(); 
						}
						function ocultar_producto(producto) {
							$('#producto_nombre_'+producto).hide(); 
						}
						function cargar_elemento(elemento) {
							$.ajax({
								url: "<?php echo $Servidor_url; ?>/ajax/sistema/cargar-elemento.php?elemento="+elemento,
								success: function (resultado) {
									$("#elemento"+elemento).html(resultado);
								}
							});	
						}

						function cerrar_mensaje() {
							$('#mensaje_top').hide('slow');
						}

						$('div[data-href]').on("click", function() {
							document.location = $(this).data('href');
						});
					</script>
					<?php include('paginas_include/estructura/javascript-pie2.php');?>	

				</body>
				</html>