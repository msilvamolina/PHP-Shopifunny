<?php 
include('../paginas_include/variables-generales.php');
include('../paginas_include/variables-permisos.php');

$mandar_error = 0;

session_start();

$get_codigo = trim($_GET['codigo']);
$get_codigo1 = trim($_GET['codigo1']);
$get_codigo2 = trim($_GET['codigo2']);
$get_codigo3 = trim($_GET['codigo3']);

$post_codigo = trim($_POST['codigo_seguridad']);
$post_codigo1 = trim($_POST['codigo_seguridad_1']);
$post_codigo2 = trim($_POST['codigo_seguridad_2']);
$post_codigo3 = trim($_POST['codigo_seguridad_3']);

$session_codigo = trim($_SESSION['codigo_seguridad']);
$session_codigo1 = trim($_SESSION['codigo_seguridad_1']);
$session_codigo2 = trim($_SESSION['codigo_seguridad_2']);
$session_codigo3 = trim($_SESSION['codigo_seguridad_3']);


$redireccion_error = $Servidor_url.'error404';

if(($get_codigo!=$post_codigo) || ($get_codigo1!=$post_codigo1)
	|| ($get_codigo2!=$post_codigo2)
	|| ($get_codigo3!=$post_codigo3)) {
	header('Location: '.$redireccion_error);exit;
} else {
	if(($get_codigo!=$session_codigo) || ($get_codigo1!=$session_codigo1)
		|| ($get_codigo2!=$session_codigo2)
		|| ($get_codigo3!=$session_codigo3)) {
		header('Location: '.$redireccion_error);
	exit;
}
}

$precio_total_shipping = trim($_POST['precio_total_shipping']);
$precio_total = trim($_POST['precio_total']);
$precio_final = trim($_POST['precio_final']);
$email = trim($_POST['email']);
$fist_name = trim($_POST['fist_name']);
$last_name = trim($_POST['last_name']);
$address = trim($_POST['address']);

$address2 = trim($_POST['address2']);
$city = trim($_POST['city']);
$country = trim($_POST['country']);
$country_code = trim($_POST['country']);

$state = trim($_POST['state'][$country]);
$zip = trim($_POST['zip']);
$phone = trim($_POST['phone']);

$precio_descuento = trim($_POST['precio_descuento']);

$codigo_compra = trim($_POST['codigo_compra']);
$id_direccion = trim($_POST['id_direccion']);

$id_usuario = 0;
$save_information = 0;
$keep_me_out =0;

$redireccion_error = $Servidor_url.'checkout/error';

$buynow = trim($_POST['buynow']);

if($buynow) {
	$redireccion_error = $Servidor_url.'checkout/buy/'.$codigo_compra.'/error';
}

if($_POST['keep_me_out']) {
	$keep_me_out = 1;
}

if($_POST['save_information']) {
	$save_information = 1;
}
conectar2('shopifun', "compras");
foreach ($_POST['cantidad'] as $id_compra => $cantidad) {
	$modelo = 0;
	$color = 0;
	$talle = 0;

	if($_POST['modelos'][$id_compra]!=-1) {
		$modelo = $_POST['modelos'][$id_compra];
	} else {
		$modelo = 0;
		$mandar_error = 1;
	}
	if($_POST['colores'][$id_compra]!=-1) {
		$color = $_POST['colores'][$id_compra];
	} else {
		$color = 0;
		$mandar_error = 1;
	}
	if($_POST['talles'][$id_compra]!=-1) {
		$talle = $_POST['talles'][$id_compra];
	} else {
		$talle = 0;
		$mandar_error = 1;
	}

	mysql_query("UPDATE compras_primer_paso SET id_modelo='$modelo', id_color='$color', id_talle='$talle', cantidad='$cantidad'WHERE id_compra='$id_compra'");
}

//hasta aca está perfecto, para saber si lo dejamos pasar, primero verificamos que haya ingresados todos los datos que necesitamos

//consultar en la base de datos
$query_rs_inspeccionar_pais = "SELECT * FROM paises WHERE pais_codigo = '$country' ";
$rs_inspeccionar_pais = mysql_query($query_rs_inspeccionar_pais)or die(mysql_error());
$row_rs_inspeccionar_pais = mysql_fetch_assoc($rs_inspeccionar_pais);
$totalrow_rs_inspeccionar_pais = mysql_num_rows($rs_inspeccionar_pais);

if(!$totalrow_rs_inspeccionar_pais) {
	$mandar_error = 1;
}

$pais_state = $row_rs_inspeccionar_pais['pais_state'];
$pais_provincia = $row_rs_inspeccionar_pais['pais_provincia'];
$pais_state_territory = $row_rs_inspeccionar_pais['pais_state_territory'];
$pais_region = $row_rs_inspeccionar_pais['pais_region'];
$pais_country = $row_rs_inspeccionar_pais['pais_country'];
$pais_prefecture = $row_rs_inspeccionar_pais['pais_prefecture'];
$pais_zip = $row_rs_inspeccionar_pais['pais_zip'];
$pais_pin = $row_rs_inspeccionar_pais['pais_pin'];

$necesita_estado = 0;
$necesita_postal = 0;

if($pais_state || $pais_provincia || $pais_state_territory || $pais_region || $pais_country || $pais_prefecture ) {
	$necesita_estado = 1;
}

if($pais_zip || $pais_pin) {
	$necesita_postal = 1;
}

if($necesita_estado) {
	if(!$state) {
		$mandar_error = 1;
	}
} else {
	$state = 0;
}

if($necesita_postal) {
	if(!$zip) {
		$mandar_error = 1;
	}
} else {
	$zip = 0;
}

if(!$id_direccion) {
	$query = "INSERT INTO direcciones (direccion_email) VALUES ('$email')";
	$result = mysql_query($query);
	$id_direccion = mysql_insert_id();
}

mysql_query("UPDATE direcciones SET 
	direccion_email='$email', 
	direccion_keep_me_up='$keep_me_out', 
	direccion_first_name='$fist_name', 
	direccion_last_name='$last_name', 
	direccion_address='$address', 
	direccion_address_2='$address2', 
	direccion_city='$city', 
	direccion_country='$country', 
	direccion_state='$state',
	direccion_zip='$zip', 
	direccion_phone='$phone', 
	direccion_save_information='$save_information', 
	ip_visitante='$ip_visitante'
	WHERE id_direccion='$id_direccion'");


if($save_information) {
	setcookie("id_direccion",$id_direccion,time() + (86400 *  365), '/');
}

$_SESSION['id_direccion'] = $id_direccion;


if(!$email || !$fist_name || !$last_name || !$address || !$city || !$country || !$phone) {
	$mandar_error = 1;
}

if(!$mandar_error) {
	$query = "INSERT INTO compras_segundo_paso (id_usuario, compra_email, compra_direccion, compra_descuento, compra_total, compra_shipping, compra_total_final, compra_codigo, ip_visitante) 
	VALUES ('$id_usuario', '$email', '$id_direccion', '$precio_total', '$precio_total_shipping', '$precio_descuento', '$precio_final', '$codigo_compra', '$ip_visitante')";
	$result = mysql_query($query);
	$id_compra_segundo_paso = mysql_insert_id();

	desconectar();

	$redireccion = $Servidor_url.'checkout/paypal/'.$codigo_compra.'/'.$session_codigo.'/'.$session_codigo1.'/'.$session_codigo2.'/'.$session_codigo3.'/';
} else {
	$redireccion = $redireccion_error;
}


header('Location: '.$redireccion);
exit;
?>