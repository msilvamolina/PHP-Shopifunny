<?php include('paginas_include/variables-generales.php');


$titulo_pagina = 'Shopifunny - Checkout';

$productos_a_comprar = '19,18,16';


$WHERE = null;


$array_productos = explode(',', $productos_a_comprar);

foreach ($array_productos as $valor) {
	if(!$WHERE) {
		$WHERE = 'WHERE id_producto = '.$valor;
	} else {
		$WHERE .= ' OR id_producto = '.$valor;
	}
}




conectar2('shopifun', "admin");

//consultar en la base de datos
$query_rs_productos = "SELECT * FROM productos $WHERE  ORDER BY id_producto DESC";
$rs_productos = mysql_query($query_rs_productos)or die(mysql_error());
$row_rs_productos = mysql_fetch_assoc($rs_productos);
$totalrow_rs_productos = mysql_num_rows($rs_productos);


//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, id_publicacion, recorte_foto_miniatura FROM fotos_publicaciones";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/productos/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));
desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('paginas_include/estructura/head.php'); ?>

</head>
<body >
	<?php include('paginas_include/estructura/barra-top.php') ; ?>
	<main class="cd-main-content">
		<h2>Checkout</h2>
		<div class="cd-form floating-labels" style="max-width:1000px; margin: 0 auto;">
			<?php if($totalrow_rs_productos) { ?>
			<table class="table table-striped">
				<tbody>
					<?php 
					$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

					do { 
						$id_producto = $row_rs_productos['id_producto'];
						$promocion_titulo = $row_rs_productos['producto_titulo'];
						$producto_publicado = $row_rs_productos['producto_publicado'];
						$nombre_foto = $row_rs_productos['nombre_foto'];
						$fecha_publicacion = $row_rs_productos['fecha_publicacion'];

						$foto_portada = $row_rs_productos['foto_portada'];

						$publicada = '<p class="rojo">No está publicado</p>';
						if($producto_publicado) {
							$publicada = '<p class="verde">'.nombre_fecha($fecha_publicacion).'</p>';
						}

						$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
						$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

						if($foto_portada) {
							$imagen = $array_fotos[$foto_portada];
						}

						$super_class = null;
						if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
							$super_class = 'categorias_con_subgrupos';
						}
						?>
						<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/productos/04-ficha-producto.php?producto=<?php echo $id_producto; ?>">
							<td><img src="<?php echo $imagen; ?>"  width="100"></td>
							<td><?php echo $promocion_titulo; ?></td>
							<td width="200"><?php echo $publicada; ?></td>
						</tr>		
						<?php } while($row_rs_productos = mysql_fetch_assoc($rs_productos)); ?>	          	
					</tbody>
				</table>		     
				<?php } else { ?>
				<p>No hay más notas</p>
				<?php }?>           
			</div>

			<br><br><br><br><br><br><br><br>

		</main>

		<?php include('paginas_include/estructura/javascript-pie.php');?>
	</script>
</body>
</html>