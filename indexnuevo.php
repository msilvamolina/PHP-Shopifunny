<?php include('paginas_include/variables-generales.php');

include('paginas_include/home/home.php');


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('paginas_include/estructura/head.php'); ?>
	<!-- Base MasterSlider style sheet -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/masterslider.css" />

	<!-- Master Slider Skin -->
	<link href="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>

	<!-- jQuery -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/jquery.easing.min.js"></script>

	<!-- Master Slider -->
	<script src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/masterslider.min.js"></script>
	<link href="<?php echo $Servidor_url; ?>css/productos-grilla.css?v=2" rel='stylesheet' type='text/css'>

	<link href='http://fonts.googleapis.com/css?family=Work+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/itemcart.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>js/modernizr2.js"></script> <!-- Modernizr -->


	<style>
	.elemento_contenedor {
		margin-top: 20px;
		background: #EEEEEE;
	}
	.elemento_banner .contenedor_loader{
		padding: 20px; 
	}

	.elemento_panel .contenedor_loader{
		padding: 80px; 
	}

	.ul_cuenta_regresiva li{
		display: inline-block;
	}
	
	.contenedor_producto_slider {
		background: #E0E0E0 !important;
		padding: 10px;
		margin-right: 15px;
		border: 3px solid #BDBDBD;
	}


</style>

</head>
<body>
	<?php if($home_banner_top) { ?>
	<div class="banner_principal fondo<?php echo $banner_top_banner_fondo; ?>" >
		<a href="<?php echo $banner_lateral_banner_link; ?>" target="<?php echo $banner_lateral_banner_target; ?>">
			<img src="<?php echo $ruta_imagenes_banner.$banner_top_banner_imagen; ?>" class="banner_top" alt="<?php echo $banner_top_banner_nombre; ?>" title="<?php echo $banner_top_banner_nombre; ?>">
		</a>
	</div>
	<?php } ?>
	<?php include('paginas_include/estructura/barra-top.php') ; ?>

	<a href="#0" class="cd-cart">
		<span>0</span>
	</a>

	<main class="cd-main-content">
		<div class="ecabezado_principal">
			<!-- template -->
			<div class="ms-fullscreen-template" id="slider1-wrapper">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider">
					<?php $i=1; do { 
						$nombre_foto = $row_rs_home_slider['nombre_foto'];
						$recorte_foto_nombre = $row_rs_home_slider['recorte_foto_nombre'];
						$link = $row_rs_home_slider['link'];
						$target = $row_rs_home_slider['target'];

						$foto = $nombre_foto;

						if($recorte_foto_nombre) {
							$foto = 'recortes/'.$recorte_foto_nombre;
						}
						?>
						<div class="ms-slide slide-<?php echo $i; ?>">
							<img src="<?php echo $Servidor_url; ?>recursos/masterslider/slider-templates/classic-sliders/masterslider/style/blank.gif" data-src="<?php echo $ruta_imagenes_home_slider.$foto; ?>"/>  
						</div>
						<?php $i++; } while ($row_rs_home_slider = mysql_fetch_assoc($rs_home_slider)); ?>
					</div>
				</div>
				<!-- end of masterslider -->
				<!-- end of template -->			
				<?php if($home_banner_lateral) { ?>
				<div class="oferta_main fondo<?php echo $banner_lateral_banner_fondo; ?>">
					<a href="<?php echo $banner_lateral_banner_link; ?>" target="<?php echo $banner_lateral_banner_target; ?>">
						<img src="<?php echo $ruta_imagenes_banner.$banner_lateral_banner_imagen; ?>" alt="<?php echo $banner_lateral_banner_nombre; ?>" title="<?php echo $banner_lateral_banner_nombre; ?>">
					</a>
				</div>
				<?php } ?>
			</div>
			<div class="clear"></div>

			<ul class="cd-gallery">
				<li>
					<div class="cd-single-item">
						<a href="#0">
							<ul class="cd-slider-wrapper">
								<li><img src="<?php echo $Servidor_url;?>img/thumb-11.jpg" alt="Preview image"></li>
								<li class="selected"><img src="<?php echo $Servidor_url;?>img/thumb-22.jpg" alt="Preview image"></li>
								<li><img src="<?php echo $Servidor_url;?>img/thumb-33.jpg" alt="Preview image"></li>
							</ul>
						</a>

						<div class="cd-customization">
							<div class="color selected-2" data-type="select">
								<ul>
									<li class="color-1">color-1</li>
									<li class="color-2 active">color-2</li>
									<li class="color-3">color-3</li>
								</ul>
							</div>

							<div class="size" data-type="select">
								<ul>
									<li class="small active">Small</li>
									<li class="medium">Medium</li>
									<li class="large">Large</li>
								</ul>
							</div>

							<button class="add-to-cart">
								<em>Add to Cart</em>
								<svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
									<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
								</svg>
							</button>
						</div> <!-- .cd-customization -->

						<button class="cd-customization-trigger">Customize</button>
					</div> <!-- .cd-single-item -->
				</li>

			</ul> <!-- cd-gallery -->
			<?php if($totalrow_rs_home_cuerpo) { 
				foreach ($array_cuerpo as $id_cuerpo => $contenido) {
					$cuerpo_tipo = $array_cuerpo_tipo[$id_cuerpo]; ?>
					<div class="elemento_contenedor elemento_<?php echo $cuerpo_tipo; ?>" id="elemento<?php echo $id_cuerpo; ?>">
						<div class="contenedor_loader">
							<img src="<?php echo $ruta_img_loader; ?>" class="elemento_loader" alt="">
						</div>
					</div>
					<?php } } ?>
					<br><br><br><br><br><br><br><br>

				</main>

			<script src="<?php echo $Servidor_url;?>js/jquery-2.1.4.js"></script>
			<script src="<?php echo $Servidor_url;?>js/itemcart.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">		

				<?php foreach ($array_cuerpo as $id_cuerpo => $value) { ?>
					cargar_elemento(<?php echo $id_cuerpo; ?>);
					<?php } ?>

					var slider = new MasterSlider();

					slider.control('arrows' ,{insertTo:'#masterslider'});	
					slider.control('bullets', {autohide:false});	

					slider.setup('masterslider' , {
						width:1280,
						height:600,
						autoplay: true,
						speed:20
					});


					function mostrar_producto(producto) {
						$('#producto_nombre_'+producto).show(); 
					}
					function ocultar_producto(producto) {
						$('#producto_nombre_'+producto).hide(); 
					}


					function cargar_elemento(elemento) {
						$.ajax({
							url: "<?php echo $Servidor_url; ?>/ajax/sistema/cargar-elemento.php?elemento="+elemento,
							success: function (resultado) {
								$("#elemento"+elemento).html(resultado);
							}
						});	
					}
				</script>
			</body>
			</html>