<?php include('paginas_include/variables-generales.php');
include('paginas_include/variables-permisos.php');

$busqueda = trim($_GET['q']);

if(!$busqueda) {
	redireccionar_404();
}

$busqueda = str_replace("'", '', $busqueda);

conectar2('shopifun', "admin");

$titulo_pagina = "Shopifunny   &raquo; ".$busqueda;

session_start();
$pagina = 1;


$_SESSION['noticias_pagina']++;
$comienza = $_GET['comienza'];
$limite = 10;
$arranca = ($limite * $pagina);

$query_rs_negocios = "SELECT productos.id_producto,productos.producto_url, productos.producto_titulo, productos.producto_descripcion, productos.producto_precio_dolar, productos.producto_precio_dolar_comparado, productos.foto_portada, productos.producto_precio_shipping_dolar, fotos_publicaciones.recorte_foto_miniatura, MATCH (productos.producto_titulo, productos.producto_descripcion, productos.producto_tags) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM productos,fotos_publicaciones WHERE fotos_publicaciones.id_foto = productos.foto_portada  AND (MATCH (productos.producto_titulo, productos.producto_descripcion, productos.producto_tags) AGAINST ( '$busqueda' IN BOOLEAN MODE)) AND productos.producto_publicado = 1";
$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
$totalrow_rs_negocios = mysql_num_rows($rs_negocios);


	//consultar en la base de datos
$query_rs_negocios_like = "SELECT productos.id_producto, productos.producto_url, productos.producto_titulo, productos.producto_descripcion, productos.producto_precio_dolar, productos.producto_precio_dolar_comparado, productos.foto_portada, productos.producto_precio_shipping_dolar, fotos_publicaciones.recorte_foto_miniatura FROM productos, fotos_publicaciones WHERE (productos.producto_titulo LIKE '%$busqueda%' OR productos.producto_descripcion LIKE '%$busqueda%' OR productos.producto_tags LIKE '%$busqueda%') AND fotos_publicaciones.id_foto  = productos.foto_portada AND productos.producto_publicado = 1";
$rs_negocios_like = mysql_query($query_rs_negocios_like)or die(mysql_error());
$row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like);
$totalrow_rs_negocios_like = mysql_num_rows($rs_negocios_like);


if($totalrow_rs_negocios) {
	do {
		$id_producto = $row_rs_negocios['id_producto'];
		$producto_titulo = $row_rs_negocios['producto_titulo'];
		$producto_precio_dolar = $row_rs_negocios['producto_precio_dolar'];
		$producto_precio_dolar_comparado = $row_rs_negocios['producto_precio_dolar_comparado'];
		$recorte_foto_miniatura = $row_rs_negocios['recorte_foto_miniatura'];
		$producto_url = $row_rs_negocios['producto_url'];
		$producto_precio_shipping_dolar = $row_rs_negocios['producto_precio_shipping_dolar'];

		$array_productos[$id_producto] = $producto_titulo;
		$array_productos_precio[$id_producto] = $producto_precio_dolar;
		$array_productos_precio_comparado[$id_producto] = $producto_precio_dolar_comparado;
		$array_productos_miniatura[$id_producto] = $recorte_foto_miniatura;
		$array_productos_url[$id_producto] = $producto_url;
		$array_productos_precio_shipping[$id_producto] = $producto_precio_shipping_dolar;

	} while ($row_rs_negocios = mysql_fetch_assoc($rs_negocios));
}

if($totalrow_rs_negocios_like) {
	do {
		$id_producto = $row_rs_negocios_like['id_producto'];
		$producto_titulo = $row_rs_negocios_like['producto_titulo'];
		$producto_precio_dolar = $row_rs_negocios_like['producto_precio_dolar'];
		$producto_precio_dolar_comparado = $row_rs_negocios_like['producto_precio_dolar_comparado'];
		$recorte_foto_miniatura = $row_rs_negocios_like['recorte_foto_miniatura'];
		$producto_url = $row_rs_negocios_like['producto_url'];

		$array_productos[$id_producto] = $producto_titulo;
		$array_productos_precio[$id_producto] = $producto_precio_dolar;
		$array_productos_precio_comparado[$id_producto] = $producto_precio_dolar_comparado;
		$array_productos_miniatura[$id_producto] = $recorte_foto_miniatura;
		$array_productos_url[$id_producto] = $producto_url;

	} while ($row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like));
}
desconectar();

$total_total = count($array_productos);

$panel_diseno = 6;
$panel_fondo = 17;
$fondo = 17;
$diseno_producto_fondo = 17;
$panelpromo = 10;
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php
	//Permisos
	$agregar_slick = 1;

	include('paginas_include/estructura/head.php'); ?>
	<?php include('paginas_include/estructura/google-tag-manager.php'); ?>

	<style>
	.contenedor_noticia {
		margin-bottom: 20px;
		padding: 30px;
	}
	.titulo_pagina {
		font-weight: bold;
		font-size: 32px;
	}

	.contenedor_elementos {
		width: 100%;
	}

	.contenedor_producto_busqueda {
		width: 99%;
		margin: 0.5%;
		float: left;
		background: #fff;
		border: 2px solid #000;
	}
	@media only screen and (min-width: 480px) {
		.contenedor_producto_busqueda {
			width: 49%;
		}
	}

	@media only screen and (min-width: 680px) {
		.contenedor_producto_busqueda {
			width: 32%;
		}
	}

	@media only screen and (min-width: 920px) {
		.contenedor_producto_busqueda {
			width: 24%;
		}
	}

	@media only screen and (min-width: 1200px) {
		.contenedor_producto_busqueda {
			width: 19%;
		}
	}

	.contenedor_producto_busqueda:hover {
		-webkit-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);
		box-shadow: 0px 0px 21px -3px rgba(0,0,0,0.75);	
	}

	.nuevo_contenedor_botones {
		position: absolute;
	}

	.contenedor_elementos2 {
		padding: 20px;
	}
</style>
</head>
<body >

	<?php include('paginas_include/estructura/barra-top-nueva.php'); ?>
	<main class="cd-main-content">

		<?php if($total_total) { 
			asort($array_productos);
			?>			
			<div class="contenedor_elementos2">
				<h2 class="error">Find <?php echo $total_total; ?> products for you</i></h2>
			</div>
			<div class=" panel_diseno<?php echo $panel_diseno; ?>">
				<?php 
				$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/productos/recortes/'; $producto_i=1;
				foreach ($array_productos as $id_producto => $producto_titulo) {

					$producto_precio_dolar = $array_productos_precio[$id_producto];
					$producto_precio_dolar_comparado = $array_productos_precio_comparado[$id_producto];
					$recorte_foto_miniatura = $array_productos_miniatura[$id_producto];

					$producto_url = $array_productos_url[$id_producto];

					$precio = formato_moneda($producto_precio_dolar, 'dolar');
					$precio_comparado = formato_moneda($producto_precio_dolar_comparado, '');

					$link_producto = $Servidor_url.'p/'.$producto_url.'/';

					$producto_precio_shipping_dolar = $array_productos_precio_shipping[$id_producto];

					$precio_shipping = 'Free';

					if($producto_precio_shipping_dolar) {
						$precio_shipping = formato_moneda($producto_precio_shipping_dolar, '');
					}
					?>

					<div class="contenedor_producto_slider contenedor_producto_busqueda fondo<?php echo $diseno_producto_fondo; ?>">
						<div class="contenedor_imagen">
							<div class="nuevo_contenedor_botones">
								<a onclick="add_to_cart(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
									<div class="boton_add_to_cart" id="add_to_cart_producto_<?php echo $producto_i; ?>">
										<i class="fa fa-cart-plus"></i>
									</div>
								</a>
								<a onclick="add_to_wish_list(<?php echo $id_producto; ?>, <?php echo $producto_i; ?>)">
									<div class="boton_add_to_wish_list" id="add_to_wish_list_producto_<?php echo $producto_i; ?>">
										<i class="fa fa-heart-o"></i>
									</div>
								</a>
							</div>
							<a href="<?php echo $link_producto; ?>">
								<img class="producto_imagen" src="<?php echo $ruta_imagenes.$recorte_foto_miniatura; ?>" alt="<?php echo $producto_titulo; ?>">
							</a>
						</div>
						<a href="<?php echo $link_producto; ?>">

							<div class="span_precio"><?php echo $precio; ?><span><?php echo $precio_comparado; ?></span></div>
							<h3 class="producto_titulo"><?php echo $producto_titulo; ?></h3>

							<div class="clear"></div>
							<div class="div_contenedora_boton">
								<span class="panel_producto_shipping">
									<i class="fa fa-truck"></i> <?php echo $precio_shipping; ?> Shipping!</span>
								</div>
							</a>

						</div> <!-- .cd-single-item -->
						<?php $producto_i++; } ?>
						<div class="clear"></div>

					</div>

					<?php } else { ?>
					<div class="contenedor_elementos2">
						<h2 class="error">Sorry, we didn't find any matches for '<i><?php echo $busqueda; ?>'</i></h2>
					</div>
					<?php } ?>
					<div class="clear"></div>
				</div>

			</div>
			<br>
		</main>
		<?php include('paginas_include/estructura/pie.php') ; ?>

		<?php include('paginas_include/estructura/javascript-pie.php');?>

		<?php include('paginas_include/estructura/javascript-pie2.php');?>	

	</body>
	</html>